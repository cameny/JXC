/*
Navicat MySQL Data Transfer

Source Server         : db
Source Server Version : 50626
Source Host           : localhost:3306
Source Database       : jxc

Target Server Type    : MYSQL
Target Server Version : 50626
File Encoding         : 65001

Date: 2016-08-10 10:01:46
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for jxc_bsd_father
-- ----------------------------
DROP TABLE IF EXISTS `jxc_bsd_father`;
CREATE TABLE `jxc_bsd_father` (
  `id` varchar(36) NOT NULL COMMENT 'id',
  `kdsj` datetime DEFAULT NULL COMMENT '开单时间',
  `dh` varchar(50) DEFAULT NULL COMMENT '单号',
  `mdid` varchar(50) DEFAULT NULL COMMENT '出库仓库',
  `cky` varchar(50) DEFAULT NULL COMMENT '出库员',
  `bz` varchar(100) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='报损单';

-- ----------------------------
-- Records of jxc_bsd_father
-- ----------------------------

-- ----------------------------
-- Table structure for jxc_bsd_son
-- ----------------------------
DROP TABLE IF EXISTS `jxc_bsd_son`;
CREATE TABLE `jxc_bsd_son` (
  `id` varchar(36) NOT NULL COMMENT 'id',
  `bh` int(4) DEFAULT NULL COMMENT '编号',
  `zbid` varchar(36) DEFAULT NULL COMMENT '主表id',
  `txm` varchar(50) DEFAULT NULL COMMENT '条形码',
  `gg` varchar(50) DEFAULT NULL COMMENT '规格',
  `dw` varchar(50) DEFAULT NULL COMMENT '单位',
  `sl` int(4) DEFAULT NULL COMMENT '数量',
  `bz` varchar(50) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='报损单子表';

-- ----------------------------
-- Records of jxc_bsd_son
-- ----------------------------

-- ----------------------------
-- Table structure for jxc_cgjh_father
-- ----------------------------
DROP TABLE IF EXISTS `jxc_cgjh_father`;
CREATE TABLE `jxc_cgjh_father` (
  `id` varchar(36) NOT NULL COMMENT 'id',
  `kdsj` datetime DEFAULT NULL COMMENT '开单时间',
  `dh` varchar(50) DEFAULT NULL COMMENT '单号',
  `khmc` varchar(50) NOT NULL DEFAULT '散户' COMMENT '客户名称',
  `lxfs` varchar(50) DEFAULT NULL COMMENT '联系方式',
  `dz` varchar(50) DEFAULT NULL COMMENT '地址',
  `mdid` varchar(50) DEFAULT NULL COMMENT '出库仓库',
  `khr` varchar(50) DEFAULT NULL COMMENT '开单人',
  `cky` varchar(50) DEFAULT NULL COMMENT '出库员',
  `rmbdx` varchar(200) DEFAULT NULL,
  `rmbxx` float DEFAULT NULL,
  `bz` varchar(50) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='采购进货';

-- ----------------------------
-- Records of jxc_cgjh_father
-- ----------------------------

-- ----------------------------
-- Table structure for jxc_cgjh_son
-- ----------------------------
DROP TABLE IF EXISTS `jxc_cgjh_son`;
CREATE TABLE `jxc_cgjh_son` (
  `id` varchar(36) NOT NULL COMMENT 'id',
  `bh` int(4) DEFAULT NULL COMMENT '编号',
  `spmc` varchar(100) DEFAULT NULL,
  `zbid` varchar(36) DEFAULT NULL COMMENT '主表id',
  `txm` varchar(50) DEFAULT NULL COMMENT '条形码',
  `gg` varchar(50) DEFAULT NULL COMMENT '规格',
  `dw` varchar(50) DEFAULT NULL COMMENT '单位',
  `sl` int(4) DEFAULT NULL COMMENT '数量',
  `dj` decimal(18,2) DEFAULT NULL COMMENT '单价',
  `xj` decimal(18,2) DEFAULT NULL COMMENT '小计',
  `bz` varchar(100) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='采购进货子表';

-- ----------------------------
-- Records of jxc_cgjh_son
-- ----------------------------

-- ----------------------------
-- Table structure for jxc_cgth_father
-- ----------------------------
DROP TABLE IF EXISTS `jxc_cgth_father`;
CREATE TABLE `jxc_cgth_father` (
  `id` varchar(36) NOT NULL COMMENT 'id',
  `kdsj` datetime DEFAULT NULL COMMENT '开单时间',
  `dh` varchar(50) DEFAULT NULL COMMENT '单号',
  `khmc` varchar(50) NOT NULL DEFAULT '散户' COMMENT '客户名称',
  `lxfs` varchar(50) DEFAULT NULL COMMENT '联系方式',
  `dz` varchar(50) DEFAULT NULL COMMENT '地址',
  `mdid` varchar(50) DEFAULT NULL COMMENT '出库仓库',
  `khr` varchar(50) DEFAULT NULL COMMENT '开单人',
  `cky` varchar(50) DEFAULT NULL COMMENT '出库员',
  `rmbdx` varchar(100) DEFAULT NULL,
  `rmbxx` float DEFAULT NULL,
  `bz` varchar(50) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='采购退货';

-- ----------------------------
-- Records of jxc_cgth_father
-- ----------------------------

-- ----------------------------
-- Table structure for jxc_cgth_son
-- ----------------------------
DROP TABLE IF EXISTS `jxc_cgth_son`;
CREATE TABLE `jxc_cgth_son` (
  `id` varchar(36) NOT NULL COMMENT 'id',
  `bh` int(4) DEFAULT NULL COMMENT '编号',
  `spmc` varchar(100) DEFAULT NULL,
  `zbid` varchar(36) DEFAULT NULL COMMENT '主表id',
  `txm` varchar(50) DEFAULT NULL COMMENT '条形码',
  `gg` varchar(50) DEFAULT NULL COMMENT '规格',
  `dw` varchar(50) DEFAULT NULL COMMENT '单位',
  `sl` int(4) DEFAULT NULL COMMENT '数量',
  `dj` decimal(18,2) DEFAULT NULL COMMENT '单价',
  `xj` decimal(18,2) DEFAULT NULL COMMENT '小计',
  `bz` varchar(100) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='采购退货子表';

-- ----------------------------
-- Records of jxc_cgth_son
-- ----------------------------

-- ----------------------------
-- Table structure for jxc_khgys
-- ----------------------------
DROP TABLE IF EXISTS `jxc_khgys`;
CREATE TABLE `jxc_khgys` (
  `id` varchar(36) NOT NULL COMMENT 'id',
  `mc` varchar(100) DEFAULT NULL,
  `bh` int(11) NOT NULL DEFAULT '0',
  `lxr` varchar(50) DEFAULT NULL COMMENT '联系人',
  `lxfs` varchar(50) DEFAULT NULL COMMENT '联系方式',
  `dz` varchar(100) DEFAULT NULL COMMENT '地址',
  `sfkh` varchar(2) NOT NULL DEFAULT '0' COMMENT '是否客户',
  `sfgys` varchar(2) NOT NULL DEFAULT '0' COMMENT '是否供应商',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='客户供应商';

-- ----------------------------
-- Records of jxc_khgys
-- ----------------------------

-- ----------------------------
-- Table structure for jxc_md
-- ----------------------------
DROP TABLE IF EXISTS `jxc_md`;
CREATE TABLE `jxc_md` (
  `id` varchar(36) NOT NULL COMMENT 'id',
  `mdmc` varchar(50) NOT NULL COMMENT '门店名称',
  `gly` varchar(20) DEFAULT NULL COMMENT '管理员',
  `dh` varchar(20) DEFAULT NULL COMMENT '电话',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='门店表';

-- ----------------------------
-- Records of jxc_md
-- ----------------------------
INSERT INTO `jxc_md` VALUES ('049af03c-2c4b-4484-b017-dcb8b49755c9', 'dsfsdffff', 'admin', 'dfdffgjjj7777fffjjj');
INSERT INTO `jxc_md` VALUES ('3de2a46d-b176-4d3c-acc6-f70c135155ed', 'fdffff', 'fff', 'dfff');
INSERT INTO `jxc_md` VALUES ('5faaa2ba-ce95-4c13-a1ee-34283565a00f', '', '', '');

-- ----------------------------
-- Table structure for jxc_sp
-- ----------------------------
DROP TABLE IF EXISTS `jxc_sp`;
CREATE TABLE `jxc_sp` (
  `id` varchar(64) NOT NULL COMMENT 'id',
  `spmc` varchar(100) DEFAULT NULL COMMENT '商品名称',
  `xh` varchar(50) DEFAULT NULL COMMENT '型号',
  `gg` varchar(50) DEFAULT NULL COMMENT '规格',
  `dw` varchar(50) DEFAULT NULL COMMENT '单位',
  `sph` varchar(100) DEFAULT NULL COMMENT '商品号',
  `txm` varchar(100) DEFAULT NULL COMMENT '条形码',
  `spbz` varchar(200) DEFAULT NULL COMMENT '商品备注',
  `cscbj` decimal(18,2) DEFAULT NULL COMMENT '初始成本价',
  `bj` decimal(18,2) DEFAULT NULL COMMENT '标价',
  `cskc` int(4) DEFAULT NULL COMMENT '初始库存',
  `kczdz` int(4) DEFAULT NULL COMMENT '库存最大值',
  `kczxz` int(4) DEFAULT NULL COMMENT '库存最小值',
  `zt` varchar(2) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品';

-- ----------------------------
-- Records of jxc_sp
-- ----------------------------
INSERT INTO `jxc_sp` VALUES ('42a609fe-e230-4f6f-abe6-9d1e4422e0da', '商品名称17', '商品型号17', '规格17', '个', '商品号17', '条形码', '备注', '5.00', '3.00', '500', '500', '0', '0');
INSERT INTO `jxc_sp` VALUES ('49abdbd1-397d-4ce0-b44b-23d3224de89f', '商品名称2', '商品型号2', '规格2', '个', '商品号2', '条形码', '备注', '5.00', '3.00', '500', '500', '0', '0');
INSERT INTO `jxc_sp` VALUES ('5646f5b9-83e8-4655-8d99-ae9f318feaee', '商品名称9', '商品型号9', '规格9', '个', '商品号9', '条形码', '备注', '5.00', '3.00', '500', '500', '0', '0');
INSERT INTO `jxc_sp` VALUES ('57229748-6c78-4dd5-b556-e66ab7209cdf', '商品名称7', '商品型号7', '规格7', '个', '商品号7', '条形码', '备注', '5.00', '3.00', '500', '500', '0', '0');
INSERT INTO `jxc_sp` VALUES ('6164b978-0da9-48bc-a98b-b966ffc95703', '商品名称5', '商品型号5', '规格5', '个', '商品号5', '条形码', '备注', '5.00', '3.00', '500', '500', '0', '0');
INSERT INTO `jxc_sp` VALUES ('62ec5ed3-6d90-4139-97e3-f960c5556a76', '商品名称14', '商品型号14', '规格14', '个', '商品号14', '条形码', '备注', '5.00', '3.00', '500', '500', '0', '0');
INSERT INTO `jxc_sp` VALUES ('6a9082a6-3fa9-45d1-aada-f57689f53c1f', '商品名称11', '商品型号11', '规格11', '个', '商品号11', '条形码', '备注', '5.00', '3.00', '500', '500', '0', '0');
INSERT INTO `jxc_sp` VALUES ('97650069-f7a7-4b80-9912-e290f2d7c384', '商品名称13', '商品型号13', '规格13', '个', '商品号13', '条形码', '备注', '5.00', '3.00', '500', '500', '0', '0');
INSERT INTO `jxc_sp` VALUES ('9943152f-837f-4ff0-aad9-e149b37033f9', '商品名称3', '商品型号3', '规格3', '个', '商品号3', '条形码', '备注', '5.00', '3.00', '500', '500', '0', '0');
INSERT INTO `jxc_sp` VALUES ('a2ee74ac-d919-4fa8-b101-2d39e2877bca', '商品名称8', '商品型号8', '规格8', '个', '商品号8', '条形码', '备注', '5.00', '3.00', '500', '500', '0', '0');

-- ----------------------------
-- Table structure for jxc_xsck_father
-- ----------------------------
DROP TABLE IF EXISTS `jxc_xsck_father`;
CREATE TABLE `jxc_xsck_father` (
  `id` varchar(36) NOT NULL COMMENT 'id',
  `kdsj` datetime DEFAULT NULL COMMENT '开单时间',
  `dh` varchar(50) DEFAULT NULL COMMENT '单号',
  `khmc` varchar(50) NOT NULL DEFAULT '散户' COMMENT '客户名称',
  `lxfs` varchar(50) DEFAULT NULL COMMENT '联系方式',
  `dz` varchar(50) DEFAULT NULL COMMENT '地址',
  `mdid` varchar(50) DEFAULT NULL COMMENT '出库仓库',
  `khr` varchar(50) DEFAULT NULL COMMENT '开单人',
  `cky` varchar(50) DEFAULT NULL COMMENT '出库员',
  `rmbdx` varchar(100) DEFAULT NULL,
  `rmbxx` float DEFAULT NULL,
  `bz` varchar(50) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='销售出库';

-- ----------------------------
-- Records of jxc_xsck_father
-- ----------------------------
INSERT INTO `jxc_xsck_father` VALUES ('008da101-b4b7-4189-942d-df5ca5fa549d', '2016-08-08 15:31:08', 'XS16080600046', '散户', null, null, '门店1', '系统管理员', '系统管理员', '贰拾壹元整', '21', '换货46');
INSERT INTO `jxc_xsck_father` VALUES ('02ebdf24-aa93-4186-bae6-95aafb8f8b26', '2016-08-08 15:31:02', 'XS16080600027', '散户', null, null, '门店1', '系统管理员', '系统管理员', '贰拾壹元整', '21', '换货27');
INSERT INTO `jxc_xsck_father` VALUES ('030f083e-61e1-4b53-9ff3-0637e41f0a59', '2016-08-08 15:30:55', 'XS1608060008', '散户', null, null, '门店1', '系统管理员', '系统管理员', '贰拾壹元整', '21', '换货8');
INSERT INTO `jxc_xsck_father` VALUES ('0c87b0b0-8b60-4463-ba20-2e1970c7a9f5', '2016-08-08 15:31:01', 'XS16080600023', '散户', null, null, '门店1', '系统管理员', '系统管理员', '贰拾壹元整', '21', '换货23');
INSERT INTO `jxc_xsck_father` VALUES ('129d8554-1a4c-405a-aee2-d349b0c8cd2f', '2016-08-06 15:27:49', 'XS1608060002', '散户', null, null, '门店1', '系统管理员', '系统管理员', '贰拾壹元整', '21', '换货2');
INSERT INTO `jxc_xsck_father` VALUES ('13b16a57-4e2e-4a4a-97b7-159e1ba5f79f', '2016-08-06 16:48:03', 'XS1608060006', '散户', null, null, '门店1', '系统管理员', '系统管理员', '贰拾壹元整', '21', '换货6');
INSERT INTO `jxc_xsck_father` VALUES ('188629fb-3b6e-4adc-aa60-e3b8516509df', '2016-08-08 15:30:59', 'XS16080600016', '散户', null, null, '门店1', '系统管理员', '系统管理员', '贰拾壹元整', '21', '换货16');
INSERT INTO `jxc_xsck_father` VALUES ('19e3ec4a-697b-47a5-b9b2-c1a9efe97105', '2016-08-08 15:31:00', 'XS16080600019', '散户', null, null, '门店1', '系统管理员', '系统管理员', '贰拾壹元整', '21', '换货19');
INSERT INTO `jxc_xsck_father` VALUES ('1a652fc6-2c71-4eac-8b29-ff9f0d426ea0', '2016-08-08 15:30:56', 'XS1608060009', '散户', null, null, '门店1', '系统管理员', '系统管理员', '贰拾壹元整', '21', '换货9');
INSERT INTO `jxc_xsck_father` VALUES ('1a877460-c9c8-4282-bc2a-2b241425d529', '2016-08-08 15:31:02', 'XS16080600026', '散户', null, null, '门店1', '系统管理员', '系统管理员', '贰拾壹元整', '21', '换货26');
INSERT INTO `jxc_xsck_father` VALUES ('1dcc35f8-5033-4b76-ad77-ed8834e1be3d', '2016-08-08 15:31:08', 'XS16080600047', '散户', null, null, '门店1', '系统管理员', '系统管理员', '贰拾壹元整', '21', '换货47');
INSERT INTO `jxc_xsck_father` VALUES ('283cd4b8-c931-4866-8e45-edeb682d9a6a', '2016-08-08 15:31:03', 'XS16080600032', '散户', null, null, '门店1', '系统管理员', '系统管理员', '贰拾壹元整', '21', '换货32');
INSERT INTO `jxc_xsck_father` VALUES ('2db44162-9a80-47a9-bce1-78b2504c37f1', '2016-08-08 15:30:55', 'XS1608060007', '散户', null, null, '门店1', '系统管理员', '系统管理员', '贰拾壹元整', '21', '换货7');
INSERT INTO `jxc_xsck_father` VALUES ('2ea761a3-bf35-4aac-8d2b-5428e48100ac', '2016-08-08 15:31:03', 'XS16080600031', '散户', null, null, '门店1', '系统管理员', '系统管理员', '贰拾壹元整', '21', '换货31');
INSERT INTO `jxc_xsck_father` VALUES ('309b86bb-e617-4afc-ac7a-021838376a6e', '2016-08-08 15:30:59', 'XS16080600017', '散户', null, null, '门店1', '系统管理员', '系统管理员', '贰拾壹元整', '21', '换货17');
INSERT INTO `jxc_xsck_father` VALUES ('39377fe5-2fd8-43db-8387-14744f89cb30', '2016-08-08 15:31:01', 'XS16080600022', '散户', null, null, '门店1', '系统管理员', '系统管理员', '贰拾壹元整', '21', '换货22');
INSERT INTO `jxc_xsck_father` VALUES ('41e9cf99-27eb-4612-a3e5-0bf5656d3914', '2016-08-08 15:31:08', 'XS16080600048', '散户', null, null, '门店1', '系统管理员', '系统管理员', '贰拾壹元整', '21', '换货48');
INSERT INTO `jxc_xsck_father` VALUES ('4884a692-4023-4c7b-b654-f9a1e1ef1b79', '2016-08-08 15:31:07', 'XS16080600044', '散户', null, null, '门店1', '系统管理员', '系统管理员', '贰拾壹元整', '21', '换货44');
INSERT INTO `jxc_xsck_father` VALUES ('4b8cac2c-cd2a-48f1-944f-4ce1ee63bea2', '2016-08-06 16:45:55', 'XS1608060005', '散户', null, null, '门店1', '系统管理员', '系统管理员', '贰拾壹元整', '21', '换货5');
INSERT INTO `jxc_xsck_father` VALUES ('4be03e73-70f8-4aff-9598-87a14c7f9dee', '2016-08-08 15:30:59', 'XS16080600018', '散户', null, null, '门店1', '系统管理员', '系统管理员', '贰拾壹元整', '21', '换货18');
INSERT INTO `jxc_xsck_father` VALUES ('4c0c9743-506b-4db3-890e-fbdc2634c8b4', '2016-08-08 15:31:05', 'XS16080600038', '散户', null, null, '门店1', '系统管理员', '系统管理员', '贰拾壹元整', '21', '换货38');
INSERT INTO `jxc_xsck_father` VALUES ('4ef33a8c-5907-4186-ac9b-07a4036b9a3f', '2016-08-08 15:31:06', 'XS16080600041', '散户', null, null, '门店1', '系统管理员', '系统管理员', '贰拾壹元整', '21', '换货41');
INSERT INTO `jxc_xsck_father` VALUES ('57299bf1-682c-4635-8ee6-e858883a0cb2', '2016-08-08 15:31:02', 'XS16080600028', '散户', null, null, '门店1', '系统管理员', '系统管理员', '贰拾壹元整', '21', '换货28');
INSERT INTO `jxc_xsck_father` VALUES ('5b2b1955-ab3f-43bc-9ff8-35d825c34307', '2016-08-08 15:30:57', 'XS16080600011', '散户', null, null, '门店1', '系统管理员', '系统管理员', '贰拾壹元整', '21', '换货11');
INSERT INTO `jxc_xsck_father` VALUES ('5d4a8565-b8c7-458a-8973-dc569c874f6f', '2016-08-08 15:31:05', 'XS16080600039', '散户', null, null, '门店1', '系统管理员', '系统管理员', '贰拾壹元整', '21', '换货39');
INSERT INTO `jxc_xsck_father` VALUES ('64225ceb-e950-427f-b99e-dae3a495396c', '2016-08-08 15:30:58', 'XS16080600012', '散户', null, null, '门店1', '系统管理员', '系统管理员', '贰拾壹元整', '21', '换货12');
INSERT INTO `jxc_xsck_father` VALUES ('660d5cca-8868-4983-a374-eabe92044239', '2016-08-08 15:30:58', 'XS16080600014', '散户', null, null, '门店1', '系统管理员', '系统管理员', '贰拾壹元整', '21', '换货14');
INSERT INTO `jxc_xsck_father` VALUES ('6e871712-94c1-469a-9c8b-8f7ade9725ff', '2016-08-08 15:31:07', 'XS16080600045', '散户', null, null, '门店1', '系统管理员', '系统管理员', '贰拾壹元整', '21', '换货45');
INSERT INTO `jxc_xsck_father` VALUES ('7b55260c-2544-42bd-bd1a-1b00e45a597f', '2016-08-08 15:31:00', 'XS16080600021', '散户', null, null, '门店1', '系统管理员', '系统管理员', '贰拾壹元整', '21', '换货21');
INSERT INTO `jxc_xsck_father` VALUES ('7bb0dce1-4056-4283-baaa-fcd9721053b4', '2016-08-08 15:31:09', 'XS16080600051', '散户', null, null, '门店1', '系统管理员', '系统管理员', '贰拾壹元整', '21', '换货51');
INSERT INTO `jxc_xsck_father` VALUES ('7f1766d8-723e-40c9-855f-5d4d40006156', '2016-08-08 15:31:01', 'XS16080600025', '散户', null, null, '门店1', '系统管理员', '系统管理员', '贰拾壹元整', '21', '换货25');
INSERT INTO `jxc_xsck_father` VALUES ('83213bcb-61c2-4f52-8851-4b8774cfa1d5', '2016-08-08 15:31:07', 'XS16080600043', '散户', null, null, '门店1', '系统管理员', '系统管理员', '贰拾壹元整', '21', '换货43');
INSERT INTO `jxc_xsck_father` VALUES ('834853e7-2baa-4569-8676-f9cf345811ed', '2016-08-08 15:31:01', 'XS16080600024', '散户', null, null, '门店1', '系统管理员', '系统管理员', '贰拾壹元整', '21', '换货24');
INSERT INTO `jxc_xsck_father` VALUES ('86bcf065-d961-46c8-8c54-c69176596e21', '2016-08-08 15:31:05', 'XS16080600037', '散户', null, null, '门店1', '系统管理员', '系统管理员', '贰拾壹元整', '21', '换货37');
INSERT INTO `jxc_xsck_father` VALUES ('8e329beb-012e-403e-9530-f1db569205cc', '2016-08-06 15:27:52', 'XS1608060003', '散户', null, null, '门店1', '系统管理员', '系统管理员', '贰拾壹元整', '21', '换货3');
INSERT INTO `jxc_xsck_father` VALUES ('90300c21-51c4-40c3-b382-2df1bd40863b', '2016-08-08 15:31:10', 'XS16080600053', '散户', null, null, '门店1', '系统管理员', '系统管理员', '贰拾壹元整', '21', '换货53');
INSERT INTO `jxc_xsck_father` VALUES ('90971498-2d09-4447-8eb6-b78c45d547c0', '2016-08-08 15:31:00', 'XS16080600020', '散户', null, null, '门店1', '系统管理员', '系统管理员', '贰拾壹元整', '21', '换货20');
INSERT INTO `jxc_xsck_father` VALUES ('960483b8-4bbf-48bd-8a7b-aa0d85e518f5', '2016-08-08 15:31:11', 'XS16080600056', '散户', null, null, '门店1', '系统管理员', '系统管理员', '贰拾壹元整', '21', '换货56');
INSERT INTO `jxc_xsck_father` VALUES ('9f65881f-3867-45fe-aafa-b1dcaa18e05c', '2016-08-08 15:31:04', 'XS16080600034', '散户', null, null, '门店1', '系统管理员', '系统管理员', '贰拾壹元整', '21', '换货34');
INSERT INTO `jxc_xsck_father` VALUES ('a199bb97-1604-4048-a4e7-5b693fe80a11', '2016-08-08 15:30:58', 'XS16080600015', '散户', null, null, '门店1', '系统管理员', '系统管理员', '贰拾壹元整', '21', '换货15');
INSERT INTO `jxc_xsck_father` VALUES ('a2cbe340-20d4-4e33-9e6e-cd93ed5690ea', '2016-08-08 15:31:03', 'XS16080600030', '散户', null, null, '门店1', '系统管理员', '系统管理员', '贰拾壹元整', '21', '换货30');
INSERT INTO `jxc_xsck_father` VALUES ('a3cf8ca6-9468-4c67-8645-5297226103d7', '2016-08-08 15:31:11', 'XS16080600055', '散户', null, null, '门店1', '系统管理员', '系统管理员', '贰拾壹元整', '21', '换货55');
INSERT INTO `jxc_xsck_father` VALUES ('a76ef432-ff83-4f3f-98fc-ab1bc0886319', '2016-08-08 15:31:09', 'XS16080600049', '散户', null, null, '门店1', '系统管理员', '系统管理员', '贰拾壹元整', '21', '换货49');
INSERT INTO `jxc_xsck_father` VALUES ('b183a217-6368-4979-b97e-d99fbaee64c2', '2016-08-08 15:31:03', 'XS16080600029', '散户', null, null, '门店1', '系统管理员', '系统管理员', '贰拾壹元整', '21', '换货29');
INSERT INTO `jxc_xsck_father` VALUES ('b918f431-039b-4891-88fe-1a1eb43a1c2e', '2016-08-08 15:30:58', 'XS16080600013', '散户', null, null, '门店1', '系统管理员', '系统管理员', '贰拾壹元整', '21', '换货13');
INSERT INTO `jxc_xsck_father` VALUES ('b94a72bb-cfad-4ada-a620-57b29849252b', '2016-08-08 15:31:10', 'XS16080600052', '散户', null, null, '门店1', '系统管理员', '系统管理员', '贰拾壹元整', '21', '换货52');
INSERT INTO `jxc_xsck_father` VALUES ('c248ea79-eedf-499b-94e5-869fc5acd64f', '2016-08-08 15:30:57', 'XS16080600010', '散户', null, null, '门店1', '系统管理员', '系统管理员', '贰拾壹元整', '21', '换货10');
INSERT INTO `jxc_xsck_father` VALUES ('c4ac3961-a2eb-44c3-961e-b019a94f6d2e', '2016-08-06 15:27:57', 'XS1608060004', '散户', null, null, '门店1', '系统管理员', '系统管理员', '贰拾壹元整', '21', '换货4');
INSERT INTO `jxc_xsck_father` VALUES ('c4ad2aaa-0971-4231-9872-09f3c01ab519', '2016-08-08 15:31:09', 'XS16080600050', '散户', null, null, '门店1', '系统管理员', '系统管理员', '贰拾壹元整', '21', '换货50');
INSERT INTO `jxc_xsck_father` VALUES ('db019779-6c2f-4d35-9ec9-9211ee825a30', '2016-08-08 15:31:05', 'XS16080600036', '散户', null, null, '门店1', '系统管理员', '系统管理员', '贰拾壹元整', '21', '换货36');
INSERT INTO `jxc_xsck_father` VALUES ('e29e92ec-76ec-4fb6-b6b1-cf7a479c0765', '2016-08-08 15:31:06', 'XS16080600042', '散户', null, null, '门店1', '系统管理员', '系统管理员', '贰拾壹元整', '21', '换货42');
INSERT INTO `jxc_xsck_father` VALUES ('e719683e-79c0-4e3b-94a4-cb320ba23de9', '2016-08-08 15:31:10', 'XS16080600054', '散户', null, null, '门店1', '系统管理员', '系统管理员', '贰拾壹元整', '21', '换货54');
INSERT INTO `jxc_xsck_father` VALUES ('ee71570c-8865-4ac2-ab24-fb70f005d8d4', '2016-08-06 15:28:01', 'XS1608060005', '散户', null, null, '门店1', '系统管理员', '系统管理员', '贰拾壹元整', '21', '换货5');
INSERT INTO `jxc_xsck_father` VALUES ('fda3f675-15c7-4836-a092-775ca98e5c9c', '2016-08-08 15:31:06', 'XS16080600040', '散户', null, null, '门店1', '系统管理员', '系统管理员', '贰拾壹元整', '21', '换货40');
INSERT INTO `jxc_xsck_father` VALUES ('fdb2a87a-146b-43c9-8ad7-681145713afa', '2016-08-08 15:31:04', 'XS16080600033', '散户', null, null, '门店1', '系统管理员', '系统管理员', '贰拾壹元整', '21', '换货33');
INSERT INTO `jxc_xsck_father` VALUES ('fe9eb176-b42b-4f03-bfd0-f66620f7d3b9', '2016-08-08 15:31:04', 'XS16080600035', '散户', null, null, '门店1', '系统管理员', '系统管理员', '贰拾壹元整', '21', '换货35');

-- ----------------------------
-- Table structure for jxc_xsck_son
-- ----------------------------
DROP TABLE IF EXISTS `jxc_xsck_son`;
CREATE TABLE `jxc_xsck_son` (
  `id` varchar(36) NOT NULL COMMENT 'id',
  `bh` int(4) DEFAULT NULL COMMENT '编号',
  `spmc` varchar(100) DEFAULT NULL,
  `zbid` varchar(36) DEFAULT NULL COMMENT '主表id',
  `txm` varchar(50) DEFAULT NULL COMMENT '条形码',
  `gg` varchar(50) DEFAULT NULL COMMENT '规格',
  `dw` varchar(50) DEFAULT NULL COMMENT '单位',
  `sl` int(4) DEFAULT NULL COMMENT '数量',
  `dj` decimal(18,2) DEFAULT NULL COMMENT '单价',
  `xj` decimal(18,2) DEFAULT NULL COMMENT '小计',
  `bz` varchar(100) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='销售出库子表';

-- ----------------------------
-- Records of jxc_xsck_son
-- ----------------------------
INSERT INTO `jxc_xsck_son` VALUES ('00d1693b-5056-4f8e-b257-adc27db8bfd3', '2', null, '9f65881f-3867-45fe-aafa-b1dcaa18e05c', 'txm2', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('01231e52-a2bb-421f-95fb-726e848a7a19', '1', null, '7bb0dce1-4056-4283-baaa-fcd9721053b4', 'txm1', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('0193319b-4483-4618-82c9-8cdb4fdb72d2', '1', null, '64225ceb-e950-427f-b99e-dae3a495396c', 'txm1', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('02b334ba-f156-464a-8d8c-8d7699f5c807', '4', null, '64225ceb-e950-427f-b99e-dae3a495396c', 'txm4', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('030d8303-b9cd-42ea-b4b5-243df6ad4dac', '4', null, '4be03e73-70f8-4aff-9598-87a14c7f9dee', 'txm4', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('06cee7b1-cc58-41a8-b43c-65282fb23332', '3', null, '309b86bb-e617-4afc-ac7a-021838376a6e', 'txm3', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('078078ad-6f0f-4360-950a-bf406958b51c', '1', null, 'a199bb97-1604-4048-a4e7-5b693fe80a11', 'txm1', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('0908e69c-ed5b-4f61-aa17-26c2cad7750c', '3', null, 'b183a217-6368-4979-b97e-d99fbaee64c2', 'txm3', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('0a86acd4-61c2-4907-9f8f-432c3c21aafe', '2', null, '309b86bb-e617-4afc-ac7a-021838376a6e', 'txm2', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('0ae3ca79-b325-4ce7-bf24-593a47a57bad', '4', null, '7bb0dce1-4056-4283-baaa-fcd9721053b4', 'txm4', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('0b4014b1-5505-4543-83cc-54c471d3d7a8', '2', null, '834853e7-2baa-4569-8676-f9cf345811ed', 'txm2', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('0b608881-09ed-467d-acbf-6eca9cb9faa3', '3', null, '02ebdf24-aa93-4186-bae6-95aafb8f8b26', 'txm3', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('0bf4f738-a92b-4970-9f5e-1631bb13a80b', '3', null, 'a3cf8ca6-9468-4c67-8645-5297226103d7', 'txm3', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('0e2ee987-d019-4bac-975f-d7f909df5703', '3', null, '2ea761a3-bf35-4aac-8d2b-5428e48100ac', 'txm3', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('0f54ba09-35d1-4067-889a-9a8212b92ffa', '4', null, 'b918f431-039b-4891-88fe-1a1eb43a1c2e', 'txm4', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('0fbd4107-21fd-404a-911b-10ba4be7da4c', '2', null, '02ebdf24-aa93-4186-bae6-95aafb8f8b26', 'txm2', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('1026c35a-beb1-4729-88b8-585ad787d71f', '1', null, '030f083e-61e1-4b53-9ff3-0637e41f0a59', 'txm1', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('126c71a6-b30d-4fe7-9f7f-958fbd367872', '1', null, '7f1766d8-723e-40c9-855f-5d4d40006156', 'txm1', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('130553a7-f7c7-4aac-9a21-43a1f3ae6595', '1', null, 'b183a217-6368-4979-b97e-d99fbaee64c2', 'txm1', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('13a9652e-afa0-491c-8053-5586558334a3', '1', null, '41e9cf99-27eb-4612-a3e5-0bf5656d3914', 'txm1', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('144e5d53-d0d4-44d2-a626-045436a8fd81', '3', '1', '13b16a57-4e2e-4a4a-97b7-159e1ba5f79f', 'txm3', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('1504514a-00cf-47a0-a75b-964ef811a022', '3', null, 'e719683e-79c0-4e3b-94a4-cb320ba23de9', 'txm3', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('15408118-d57f-442b-a42b-a11b32beee06', '3', null, 'b94a72bb-cfad-4ada-a620-57b29849252b', 'txm3', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('1652ae34-1b2c-4328-9384-9d40a76cbbcc', '1', null, 'e719683e-79c0-4e3b-94a4-cb320ba23de9', 'txm1', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('16c588f8-d9b5-424f-9f14-d8ff8511809a', '4', null, '5b2b1955-ab3f-43bc-9ff8-35d825c34307', 'txm4', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('1794706f-06e3-437c-9a06-1ce78ee2b4df', '4', null, '1a877460-c9c8-4282-bc2a-2b241425d529', 'txm4', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('19b58f43-ea74-46df-a6d5-97b0ed7fb642', '2', '2', '4b8cac2c-cd2a-48f1-944f-4ce1ee63bea2', 'txm2', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('19ed7317-1de4-4f1d-bbdd-595f730c818e', '3', null, '188629fb-3b6e-4adc-aa60-e3b8516509df', 'txm3', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('1a272520-d5c2-4e0b-aca1-d4432df9c9f8', '2', null, '030f083e-61e1-4b53-9ff3-0637e41f0a59', 'txm2', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('1a54e1cc-0a30-48e0-97d9-af07e9346310', '1', null, '283cd4b8-c931-4866-8e45-edeb682d9a6a', 'txm1', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('1a606e2f-4bac-4385-a4e4-e174b717c7fe', '7', '3', '13b16a57-4e2e-4a4a-97b7-159e1ba5f79f', 'txm7', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('1b01d27c-7830-444b-be9c-4464a7b5c1f1', '1', null, 'fda3f675-15c7-4836-a092-775ca98e5c9c', 'txm1', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('1ce706f4-4670-48f8-901b-59e549a2800d', '1', null, '4be03e73-70f8-4aff-9598-87a14c7f9dee', 'txm1', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('1d08ddf8-9aa9-416b-81b5-cd67a6a02591', '4', null, '86bcf065-d961-46c8-8c54-c69176596e21', 'txm4', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('1fb61464-8851-4594-9120-d61e6f6404e2', '2', null, '188629fb-3b6e-4adc-aa60-e3b8516509df', 'txm2', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('1ffaf170-9020-4178-a68d-b7658f409f9c', '2', null, '7bb0dce1-4056-4283-baaa-fcd9721053b4', 'txm2', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('2225df22-485d-4075-b655-1b6a6138c453', '2', null, 'e29e92ec-76ec-4fb6-b6b1-cf7a479c0765', 'txm2', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('26546e7d-7df0-4643-8715-143aec920058', '3', null, '283cd4b8-c931-4866-8e45-edeb682d9a6a', 'txm3', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('2760a564-1425-4f36-aa99-174f1efe3aa1', '2', null, '19e3ec4a-697b-47a5-b9b2-c1a9efe97105', 'txm2', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('28718124-e2c7-4252-a0ec-2e38bf64ce00', '2', null, 'fda3f675-15c7-4836-a092-775ca98e5c9c', 'txm2', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('29910ba9-f7a9-4793-b1aa-830951665a84', '1', null, '19e3ec4a-697b-47a5-b9b2-c1a9efe97105', 'txm1', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('2ae40443-99cf-400e-9c96-693664b767a6', '1', null, 'fdb2a87a-146b-43c9-8ad7-681145713afa', 'txm1', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('2b38ed4c-eb53-4a2e-87eb-ab2efddae5c4', '3', null, '834853e7-2baa-4569-8676-f9cf345811ed', 'txm3', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('2c5c8fe0-4c7a-4711-a6ac-27caa3abc1dd', '4', null, '83213bcb-61c2-4f52-8851-4b8774cfa1d5', 'txm4', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('2d8623a4-8690-424b-a409-30222f6ceaa4', '1', null, '4ef33a8c-5907-4186-ac9b-07a4036b9a3f', 'txm1', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('2ee47d90-fc20-403f-87de-8474eaa25795', '4', null, '960483b8-4bbf-48bd-8a7b-aa0d85e518f5', 'txm4', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('2eed3d87-6e3a-48c5-a221-cf3dfc8e9d13', '3', null, '57299bf1-682c-4635-8ee6-e858883a0cb2', 'txm3', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('3156a4c7-99a7-4736-a9d2-ed590aa876d3', '1', null, '2db44162-9a80-47a9-bce1-78b2504c37f1', 'txm1', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('3206b1cb-0bdd-4954-a1cc-9efc442cfe4a', '3', null, 'a199bb97-1604-4048-a4e7-5b693fe80a11', 'txm3', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('367e05c8-469d-4a76-9413-2f367ed466cf', '3', null, '7bb0dce1-4056-4283-baaa-fcd9721053b4', 'txm3', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('3755bcda-9acd-4440-969a-744ba083abb2', '4', null, '2ea761a3-bf35-4aac-8d2b-5428e48100ac', 'txm4', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('39003c09-6c49-430c-98c9-9865b5c063a7', '3', null, '90300c21-51c4-40c3-b382-2df1bd40863b', 'txm3', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('39ebcd77-577c-4904-b51f-50505cc4a1e6', '2', null, '0c87b0b0-8b60-4463-ba20-2e1970c7a9f5', 'txm2', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('3a2003a3-faa3-47e3-9cfc-4bf19158f0e4', '1', null, '1dcc35f8-5033-4b76-ad77-ed8834e1be3d', 'txm1', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('3a549e23-b1cd-4fdb-8db9-d182cf89938d', '4', null, '188629fb-3b6e-4adc-aa60-e3b8516509df', 'txm4', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('3c134e66-74db-44a6-a49a-d16ce91dcf01', '1', null, '309b86bb-e617-4afc-ac7a-021838376a6e', 'txm1', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('3c3e1977-0a9e-4644-ae52-9bb76a9a8eb7', '3', null, '2db44162-9a80-47a9-bce1-78b2504c37f1', 'txm3', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('3c53da9d-59c5-4e13-9809-10f8d086337b', '4', null, '309b86bb-e617-4afc-ac7a-021838376a6e', 'txm4', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('3cf3f4e4-77fc-4e80-9ab7-711cb9a238f5', '2', null, '6e871712-94c1-469a-9c8b-8f7ade9725ff', 'txm2', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('3e13b9ba-e155-42f2-977c-a6abc456d47d', '3', null, '008da101-b4b7-4189-942d-df5ca5fa549d', 'txm3', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('3ff40bd2-9234-452d-a260-411d2b4ee954', '1', null, '90971498-2d09-4447-8eb6-b78c45d547c0', 'txm1', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('406980d3-47cc-44a9-a0fa-bc3573492e91', '4', null, 'c248ea79-eedf-499b-94e5-869fc5acd64f', 'txm4', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('41306447-1d3d-4763-a7f1-3aeb19e4dec4', '4', null, '030f083e-61e1-4b53-9ff3-0637e41f0a59', 'txm4', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('420ce0ff-11ee-4108-a0c9-768a51996476', '3', null, 'fe9eb176-b42b-4f03-bfd0-f66620f7d3b9', 'txm3', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('42aec103-418a-49c5-bc31-fd673e27e9e4', '4', null, '4ef33a8c-5907-4186-ac9b-07a4036b9a3f', 'txm4', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('461eebbd-5efb-4c18-8282-57ea32f2baef', '4', '3', '13b16a57-4e2e-4a4a-97b7-159e1ba5f79f', 'txm4', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('46a2f651-0f9a-4c83-8692-d0aeb497d02a', '4', null, 'b183a217-6368-4979-b97e-d99fbaee64c2', 'txm4', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('46a72032-26c6-4254-b6de-ece19dcd9e1d', '3', null, '90971498-2d09-4447-8eb6-b78c45d547c0', 'txm3', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('48882168-8fe1-49fc-b054-4a5010b89f13', '3', null, '030f083e-61e1-4b53-9ff3-0637e41f0a59', 'txm3', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('49600822-8826-48cb-9b68-811c14d9d38c', '4', null, '2db44162-9a80-47a9-bce1-78b2504c37f1', 'txm4', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('4970ef6d-586d-4d39-8cde-f6bcd2507c42', '1', null, '660d5cca-8868-4983-a374-eabe92044239', 'txm1', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('4baf0377-ed9e-4392-8bc7-33feb6687d6b', '4', null, 'e719683e-79c0-4e3b-94a4-cb320ba23de9', 'txm4', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('4c72d28c-5005-44d9-877a-e39fb7ee1f41', '2', null, '64225ceb-e950-427f-b99e-dae3a495396c', 'txm2', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('4d80654a-8c33-4d34-8c64-b99619fcfd1b', '3', null, '5d4a8565-b8c7-458a-8973-dc569c874f6f', 'txm3', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('4e42601c-d493-46d8-900c-308eb1644f84', '2', null, '90300c21-51c4-40c3-b382-2df1bd40863b', 'txm2', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('4e9dfac0-3834-4c22-b21c-ad8da5b4e0b0', '2', null, 'c4ad2aaa-0971-4231-9872-09f3c01ab519', 'txm2', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('4f89d4c7-cc24-41df-91fa-a0d4f2439b83', '2', null, 'a199bb97-1604-4048-a4e7-5b693fe80a11', 'txm2', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('52a67639-b73c-4e86-afcc-d141ed435e74', '2', null, 'a2cbe340-20d4-4e33-9e6e-cd93ed5690ea', 'txm2', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('52aeb44d-c16f-4ee0-be66-b479f6bc627d', '1', null, 'fe9eb176-b42b-4f03-bfd0-f66620f7d3b9', 'txm1', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('54eb6a9d-c2be-4ce1-a330-34c9d94ac592', '4', null, '4884a692-4023-4c7b-b654-f9a1e1ef1b79', 'txm4', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('57dc4e77-3433-408f-b397-ea46ee9ddb2b', '3', null, 'db019779-6c2f-4d35-9ec9-9211ee825a30', 'txm3', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('58fc2231-2905-4dcf-8823-570f91093e82', '2', null, 'b918f431-039b-4891-88fe-1a1eb43a1c2e', 'txm2', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('5a46a9b3-4722-4343-9257-d1d5b94b2af5', '4', null, '660d5cca-8868-4983-a374-eabe92044239', 'txm4', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('5c0ed8ec-20ec-4d9d-b387-58f15e03b18e', '4', null, 'a76ef432-ff83-4f3f-98fc-ab1bc0886319', 'txm4', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('5d5214aa-e809-4aad-9526-15938c29aecc', '3', null, 'a2cbe340-20d4-4e33-9e6e-cd93ed5690ea', 'txm3', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('5fe41825-4fcf-4705-b151-25fe3b7fac9c', '1', null, '1a652fc6-2c71-4eac-8b29-ff9f0d426ea0', 'txm1', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('60657576-2363-40a6-a605-5ad3c75b2904', '1', null, '7b55260c-2544-42bd-bd1a-1b00e45a597f', 'txm1', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('6590cf9b-0aed-47d8-8901-6b3744a22693', '4', null, '90300c21-51c4-40c3-b382-2df1bd40863b', 'txm4', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('65b71c08-2227-44dc-9ed7-5462c9c9252c', '2', null, '4be03e73-70f8-4aff-9598-87a14c7f9dee', 'txm2', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('68fbcc4e-e7d8-4743-a6e2-431fa9497a16', '3', null, '83213bcb-61c2-4f52-8851-4b8774cfa1d5', 'txm3', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('6d7324a1-e0f0-4aa5-acb9-f393dce52ecc', '4', null, 'e29e92ec-76ec-4fb6-b6b1-cf7a479c0765', 'txm4', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('6d91b6c4-00ca-4036-80a6-3e9f5c1522b5', '2', null, '1a652fc6-2c71-4eac-8b29-ff9f0d426ea0', 'txm2', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('6e69ab84-4e99-4176-94e0-511917528667', '1', null, 'e29e92ec-76ec-4fb6-b6b1-cf7a479c0765', 'txm1', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('70110b99-0da4-45a0-a433-7257848d432c', '2', null, '4c0c9743-506b-4db3-890e-fbdc2634c8b4', 'txm2', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('7086645f-6eb4-4320-9a92-c88618303b90', '2', '3', '13b16a57-4e2e-4a4a-97b7-159e1ba5f79f', 'txm2', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('726b56fd-0a55-4df2-b404-91151db3747f', '4', null, '02ebdf24-aa93-4186-bae6-95aafb8f8b26', 'txm4', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('75006b26-0021-422e-b552-fb09c75fda29', '3', null, '1dcc35f8-5033-4b76-ad77-ed8834e1be3d', 'txm3', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('7608e9df-b88c-4414-9144-3d85206e8981', '1', null, '960483b8-4bbf-48bd-8a7b-aa0d85e518f5', 'txm1', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('77ade3e1-acfb-400a-bd0a-df70c432be8a', '4', null, '90971498-2d09-4447-8eb6-b78c45d547c0', 'txm4', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('78e9fc3c-c362-4d30-8842-9589c87b92f3', '4', null, 'fda3f675-15c7-4836-a092-775ca98e5c9c', 'txm4', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('7a1923fb-9c87-4fbf-9135-b3d9b834b3f1', '4', null, 'fe9eb176-b42b-4f03-bfd0-f66620f7d3b9', 'txm4', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('7b7baba0-42ad-4a2d-8de0-4ae73984fbf1', '4', null, '4c0c9743-506b-4db3-890e-fbdc2634c8b4', 'txm4', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('7ba7fdee-d69f-450a-ae6a-95a315f78665', '3', null, '1a652fc6-2c71-4eac-8b29-ff9f0d426ea0', 'txm3', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('7d903398-1177-4e36-99e4-91679c749c21', '3', null, 'c4ad2aaa-0971-4231-9872-09f3c01ab519', 'txm3', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('7da68f04-0234-4ca5-9d09-74389b07c0b1', '3', null, '64225ceb-e950-427f-b99e-dae3a495396c', 'txm3', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('7e04922e-bd8a-48cf-9756-952063688648', '1', null, '0c87b0b0-8b60-4463-ba20-2e1970c7a9f5', 'txm1', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('7e61ef0e-42f1-4585-af56-ebc62c7c6845', '1', null, 'db019779-6c2f-4d35-9ec9-9211ee825a30', 'txm1', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('7e6d2b8d-d89b-4e11-a908-3f7370d4d053', '2', null, '41e9cf99-27eb-4612-a3e5-0bf5656d3914', 'txm2', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('7e962bca-bdb8-4fe1-92f2-519c19803263', '4', null, 'fdb2a87a-146b-43c9-8ad7-681145713afa', 'txm4', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('80eaca84-3be4-44a2-99bc-7f528a57782b', '4', null, '5d4a8565-b8c7-458a-8973-dc569c874f6f', 'txm4', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('81158c0a-2379-443b-ba4b-ccccde1e710a', '2', null, '2db44162-9a80-47a9-bce1-78b2504c37f1', 'txm2', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('83af95b7-6b49-4593-b5ac-678211152b66', '6', '3', '13b16a57-4e2e-4a4a-97b7-159e1ba5f79f', 'txm6', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('8d149e64-d3ae-4fdd-bb0e-61dc001bece0', '3', null, '4c0c9743-506b-4db3-890e-fbdc2634c8b4', 'txm3', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('8dc10062-863b-4a9e-8e3a-3dfc7db3ac17', '2', null, 'e719683e-79c0-4e3b-94a4-cb320ba23de9', 'txm2', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('8e84926e-0bd3-4bd7-8d8f-949547e4a234', '4', null, '1a652fc6-2c71-4eac-8b29-ff9f0d426ea0', 'txm4', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('8fbb010c-17c5-43c7-97d2-2f61a48fc225', '4', null, 'a2cbe340-20d4-4e33-9e6e-cd93ed5690ea', 'txm4', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('90fa958c-0906-40b2-ae84-9b6ac35cc4f5', '1', null, 'c248ea79-eedf-499b-94e5-869fc5acd64f', 'txm1', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('93521558-1509-4b40-8991-5eafa25c14f4', '2', null, 'b183a217-6368-4979-b97e-d99fbaee64c2', 'txm2', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('9386dee3-8ee7-454a-a62a-a6fc55bf450f', '3', null, '41e9cf99-27eb-4612-a3e5-0bf5656d3914', 'txm3', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('93a0a258-8a66-42d7-af53-df6e36526b56', '1', null, '5d4a8565-b8c7-458a-8973-dc569c874f6f', 'txm1', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('942ba4a9-4c49-4b96-acc5-443d7b54baf4', '1', null, 'a76ef432-ff83-4f3f-98fc-ab1bc0886319', 'txm1', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('9475510a-a6fc-4091-967f-ca7d7b562762', '4', null, '834853e7-2baa-4569-8676-f9cf345811ed', 'txm4', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('956f8b54-b7fa-49d4-9c76-9a41ca76fe71', '1', '3', '4b8cac2c-cd2a-48f1-944f-4ce1ee63bea2', 'txm1', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('95b21aeb-a08c-4043-8f77-5ae9d90ae195', '3', null, '1a877460-c9c8-4282-bc2a-2b241425d529', 'txm3', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('95cadef0-53ed-43a5-931c-ac519290b6a2', '1', null, '83213bcb-61c2-4f52-8851-4b8774cfa1d5', 'txm1', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('9aec3196-73e4-4352-9869-d0a5042f22b4', '3', null, '660d5cca-8868-4983-a374-eabe92044239', 'txm3', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('9b1997b3-060b-46f4-9414-1f42c7c80b3b', '2', null, '7f1766d8-723e-40c9-855f-5d4d40006156', 'txm2', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('9b8e03c4-efe2-4ace-af27-7b51f49d3bc7', '1', null, '57299bf1-682c-4635-8ee6-e858883a0cb2', 'txm1', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('9c3b322b-93a6-43a7-8282-551847c57578', '2', null, '4ef33a8c-5907-4186-ac9b-07a4036b9a3f', 'txm2', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('9c99be4e-a35f-4a81-97fe-4bff9b0e8837', '1', '3', '13b16a57-4e2e-4a4a-97b7-159e1ba5f79f', 'txm1', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('9d9b615c-e1fa-49aa-bbe6-953cfedb89a3', '3', null, '7b55260c-2544-42bd-bd1a-1b00e45a597f', 'txm3', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('9dafbfa1-3c94-4f26-8bb6-ff62bfa4edf9', '1', null, 'c4ad2aaa-0971-4231-9872-09f3c01ab519', 'txm1', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('9e3b7367-d068-4544-8b3b-13483004b222', '2', null, '90971498-2d09-4447-8eb6-b78c45d547c0', 'txm2', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('a1749741-e27d-4f94-ad60-b2c46dd96fef', '1', null, 'b918f431-039b-4891-88fe-1a1eb43a1c2e', 'txm1', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('a29e4bc8-db60-45c0-be69-f5c720bed8de', '2', null, 'fe9eb176-b42b-4f03-bfd0-f66620f7d3b9', 'txm2', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('a4dbc822-30e3-493a-bd82-1151275222a6', '2', null, '86bcf065-d961-46c8-8c54-c69176596e21', 'txm2', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('a9f6483c-0151-42e2-9214-4d04384ca783', '3', null, 'fda3f675-15c7-4836-a092-775ca98e5c9c', 'txm3', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('aa5704c2-c285-4fdc-b7b0-073bf86a8403', '1', null, '2ea761a3-bf35-4aac-8d2b-5428e48100ac', 'txm1', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('aab1e35a-4e8c-42e6-a7ba-f634442d77fb', '5', '3', '13b16a57-4e2e-4a4a-97b7-159e1ba5f79f', 'txm5', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('ac88f99a-4501-431e-8de5-e8b96dadeac0', '3', null, '7f1766d8-723e-40c9-855f-5d4d40006156', 'txm3', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('af9531e7-ec58-4602-90d9-28da444ffd32', '1', null, '4c0c9743-506b-4db3-890e-fbdc2634c8b4', 'txm1', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('b1348fef-9097-4bc7-a9c7-6c641e53a3d1', '2', null, '57299bf1-682c-4635-8ee6-e858883a0cb2', 'txm2', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('b196a11f-177c-4b52-88b3-b84e748535f5', '3', null, '6e871712-94c1-469a-9c8b-8f7ade9725ff', 'txm3', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('b25d171c-8e7f-4b4b-9fac-13ce0e4ee059', '3', null, '4be03e73-70f8-4aff-9598-87a14c7f9dee', 'txm3', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('b26bddb3-8030-45fb-b90b-b2e2cac88de6', '1', null, 'b94a72bb-cfad-4ada-a620-57b29849252b', 'txm1', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('b45fab5a-7cc3-4876-b55d-1e2248bf94b4', '4', null, '0c87b0b0-8b60-4463-ba20-2e1970c7a9f5', 'txm4', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('b4a0079e-47c1-4aa5-b150-52460727b298', '2', null, '008da101-b4b7-4189-942d-df5ca5fa549d', 'txm2', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('b5513d84-2056-4935-a997-f48552467061', '4', null, '9f65881f-3867-45fe-aafa-b1dcaa18e05c', 'txm4', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('b5bfe544-3fea-48ac-8650-e4fd8cbbabdc', '2', null, '2ea761a3-bf35-4aac-8d2b-5428e48100ac', 'txm2', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('b6284e07-572b-469d-a648-1116d12d0e5e', '3', null, '4884a692-4023-4c7b-b654-f9a1e1ef1b79', 'txm3', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('b82205b8-34b4-4379-973c-81ce7bdf9f6c', '2', null, '5b2b1955-ab3f-43bc-9ff8-35d825c34307', 'txm2', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('babfcd49-49a5-43a4-839d-64842e3d9c14', '3', null, '5b2b1955-ab3f-43bc-9ff8-35d825c34307', 'txm3', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('bb21a294-7e58-4ead-aea5-7469a91bc5a3', '1', null, '9f65881f-3867-45fe-aafa-b1dcaa18e05c', 'txm1', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('bbd54950-472a-47c2-aa80-784dcc4bf7a3', '3', null, '4ef33a8c-5907-4186-ac9b-07a4036b9a3f', 'txm3', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('be162095-bbbf-4b79-891b-c9ab3acf4cc7', '2', null, '4884a692-4023-4c7b-b654-f9a1e1ef1b79', 'txm2', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('bf7e77a2-7e54-46c4-9fd2-46a664154252', '4', null, '57299bf1-682c-4635-8ee6-e858883a0cb2', 'txm4', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('bfdc1540-7aaf-4953-b608-44005e52d09c', '1', null, '834853e7-2baa-4569-8676-f9cf345811ed', 'txm1', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('c0ef0685-fcef-4f07-950e-4d81e18c9a1d', '4', null, '1dcc35f8-5033-4b76-ad77-ed8834e1be3d', 'txm4', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('c2659cce-e403-49eb-83e8-080dd270eb8e', '2', null, '7b55260c-2544-42bd-bd1a-1b00e45a597f', 'txm2', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('c2cae382-db8a-4c25-aa9b-7fe1e5510006', '4', null, '008da101-b4b7-4189-942d-df5ca5fa549d', 'txm4', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('c368a734-9dd9-4748-97d7-261bb15e5dc3', '1', null, '02ebdf24-aa93-4186-bae6-95aafb8f8b26', 'txm1', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('c42bb7a1-cb36-47f2-87cf-15ffebd54914', '2', null, '660d5cca-8868-4983-a374-eabe92044239', 'txm2', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('c507874d-13e5-40c3-bd3b-f20f3050499c', '1', null, '5b2b1955-ab3f-43bc-9ff8-35d825c34307', 'txm1', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('c635d114-cdc8-4322-a02f-46acaf75bd95', '2', null, 'b94a72bb-cfad-4ada-a620-57b29849252b', 'txm2', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('c9445754-57b3-4d45-af00-a1dc3fb29e96', '4', null, 'a199bb97-1604-4048-a4e7-5b693fe80a11', 'txm4', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('cb9bc29a-3ea5-4991-a666-ca728eac1903', '2', null, 'c248ea79-eedf-499b-94e5-869fc5acd64f', 'txm2', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('ceda7267-c9d3-4d28-99e0-271aa5cf376f', '3', null, '39377fe5-2fd8-43db-8387-14744f89cb30', 'txm3', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('ceec0202-6aad-4677-9d21-fa30ef347935', '4', null, 'a3cf8ca6-9468-4c67-8645-5297226103d7', 'txm4', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('cf15d66a-68e0-4b66-ba5e-49f8a6cdfcd2', '4', null, 'b94a72bb-cfad-4ada-a620-57b29849252b', 'txm4', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('d04b7e06-32ac-4865-a41b-764507f7baeb', '1', null, '1a877460-c9c8-4282-bc2a-2b241425d529', 'txm1', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('d550db29-307c-43c8-9d21-a9a919c484ac', '1', null, '4884a692-4023-4c7b-b654-f9a1e1ef1b79', 'txm1', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('d5eb477e-9ce4-49b3-91f0-f73aa62fa9de', '1', null, 'a2cbe340-20d4-4e33-9e6e-cd93ed5690ea', 'txm1', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('d629def4-903d-4a0f-be0d-be2bda30dada', '3', null, '9f65881f-3867-45fe-aafa-b1dcaa18e05c', 'txm3', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('d62c00d1-d73b-4841-8c0f-8eb511ead74b', '4', null, '283cd4b8-c931-4866-8e45-edeb682d9a6a', 'txm4', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('d778f160-e6cf-47f6-a31f-e7027aa379c1', '2', null, '39377fe5-2fd8-43db-8387-14744f89cb30', 'txm2', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('d77a97df-6a7f-4b9a-a5a6-cf74fcd0036f', '2', null, 'a3cf8ca6-9468-4c67-8645-5297226103d7', 'txm2', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('db016388-2671-4a00-8482-5fb84bf03b4a', '2', null, '1a877460-c9c8-4282-bc2a-2b241425d529', 'txm2', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('dce2cd69-0a5e-49ad-b3cb-ea2971de2d6d', '4', null, '7f1766d8-723e-40c9-855f-5d4d40006156', 'txm4', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('df3b01d8-f4a5-4a28-8a1a-e7263d1f6fde', '1', null, '90300c21-51c4-40c3-b382-2df1bd40863b', 'txm1', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('e0fd4304-f4f5-4f86-8f56-a4d222549ca9', '3', null, 'a76ef432-ff83-4f3f-98fc-ab1bc0886319', 'txm3', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('e123d692-badf-4a85-bbbd-2f2366890837', '3', null, '86bcf065-d961-46c8-8c54-c69176596e21', 'txm3', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('e18da627-9d87-4735-b2cc-5d5572f79be9', '3', null, 'e29e92ec-76ec-4fb6-b6b1-cf7a479c0765', 'txm3', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('e2216459-25a1-4123-9bb4-29321df6676d', '1', null, 'a3cf8ca6-9468-4c67-8645-5297226103d7', 'txm1', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('e252ca37-36c8-4ca8-a674-dd05475c8337', '3', null, 'fdb2a87a-146b-43c9-8ad7-681145713afa', 'txm3', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('e25f28d5-3137-45ed-ad8b-674c3fef853f', '4', null, '39377fe5-2fd8-43db-8387-14744f89cb30', 'txm4', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('e27a2b59-5b6c-43dc-88f2-739225d611e6', '3', null, 'c248ea79-eedf-499b-94e5-869fc5acd64f', 'txm3', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('e2f2baf1-578c-440c-8e33-069aa3b11d13', '2', null, '1dcc35f8-5033-4b76-ad77-ed8834e1be3d', 'txm2', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('e59d9232-d950-4a13-95aa-b182954d206d', '2', null, '960483b8-4bbf-48bd-8a7b-aa0d85e518f5', 'txm2', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('e6f4027d-2436-4429-8079-80cf9370fd26', '4', null, '7b55260c-2544-42bd-bd1a-1b00e45a597f', 'txm4', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('e79bf551-f1bf-42e6-99fc-e46327fd58d9', '3', null, '19e3ec4a-697b-47a5-b9b2-c1a9efe97105', 'txm3', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('eb3ce3ae-10d4-4160-839f-33343892c96f', '4', null, 'db019779-6c2f-4d35-9ec9-9211ee825a30', 'txm4', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('ebb4762f-84a2-4bab-a44d-8674924be02a', '1', null, '188629fb-3b6e-4adc-aa60-e3b8516509df', 'txm1', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('ed30d124-12ab-422b-ac02-afcb8e9c7acd', '4', null, '19e3ec4a-697b-47a5-b9b2-c1a9efe97105', 'txm4', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('ed756aa2-929c-4e56-86f6-56f0199e3ff2', '3', null, '0c87b0b0-8b60-4463-ba20-2e1970c7a9f5', 'txm3', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('ee2674c9-b39c-4375-a5a5-87849e20ecec', '2', null, '83213bcb-61c2-4f52-8851-4b8774cfa1d5', 'txm2', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('ef1e17ab-3ae9-4115-8f5b-c2b7d40c0217', '1', null, '6e871712-94c1-469a-9c8b-8f7ade9725ff', 'txm1', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('ef48d8bb-8f7d-47e1-8952-890cc828bf05', '4', null, '41e9cf99-27eb-4612-a3e5-0bf5656d3914', 'txm4', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('ef521578-b63d-457f-82fd-ed9dcd007dc7', '3', null, 'b918f431-039b-4891-88fe-1a1eb43a1c2e', 'txm3', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('efdcd098-5351-4339-ba24-bc140a72d985', '2', null, 'fdb2a87a-146b-43c9-8ad7-681145713afa', 'txm2', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('f0ebf0be-1d94-46e4-8697-3ef52b813dc3', '2', null, '5d4a8565-b8c7-458a-8973-dc569c874f6f', 'txm2', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('f12e2bb0-6f44-421d-8cc0-d82c369c475d', '3', null, '960483b8-4bbf-48bd-8a7b-aa0d85e518f5', 'txm3', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('f2855139-606b-4585-8af6-2e74e0e94fda', '4', null, 'c4ad2aaa-0971-4231-9872-09f3c01ab519', 'txm4', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('f2ac1136-f75f-4e1e-97ce-73c4fccbc866', '1', null, '39377fe5-2fd8-43db-8387-14744f89cb30', 'txm1', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('f300ce0e-6678-4cea-82d6-3bf009873a54', '2', null, '283cd4b8-c931-4866-8e45-edeb682d9a6a', 'txm2', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('f404bf53-e297-4be7-8777-a12f639884cc', '2', null, 'a76ef432-ff83-4f3f-98fc-ab1bc0886319', 'txm2', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('f589ba45-2899-425e-8a70-11a51a662fb5', '1', null, '008da101-b4b7-4189-942d-df5ca5fa549d', 'txm1', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('f5f7f9da-6ffa-4bcc-aec1-6ae3ce8ab3aa', '1', null, '86bcf065-d961-46c8-8c54-c69176596e21', 'txm1', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('fb9dbe47-04d4-475b-b7c8-fc27db87f534', '4', null, '6e871712-94c1-469a-9c8b-8f7ade9725ff', 'txm4', null, '个', '3', '1.00', '3.00', null);
INSERT INTO `jxc_xsck_son` VALUES ('fc70d8bd-437e-4f17-8de1-2bf833620898', '2', null, 'db019779-6c2f-4d35-9ec9-9211ee825a30', 'txm2', null, '个', '3', '1.00', '3.00', null);

-- ----------------------------
-- Table structure for jxc_xsth_father
-- ----------------------------
DROP TABLE IF EXISTS `jxc_xsth_father`;
CREATE TABLE `jxc_xsth_father` (
  `id` varchar(36) NOT NULL COMMENT 'id',
  `kdsj` datetime DEFAULT NULL COMMENT '开单时间',
  `dh` varchar(50) DEFAULT NULL COMMENT '单号',
  `khmc` varchar(50) NOT NULL DEFAULT '散户' COMMENT '客户名称',
  `lxfs` varchar(50) DEFAULT NULL COMMENT '联系方式',
  `dz` varchar(50) DEFAULT NULL COMMENT '地址',
  `mdid` varchar(50) DEFAULT NULL COMMENT '出库仓库',
  `khr` varchar(50) DEFAULT NULL COMMENT '开单人',
  `cky` varchar(50) DEFAULT NULL COMMENT '出库员',
  `rmbdx` varchar(100) DEFAULT NULL,
  `rmbxx` float DEFAULT NULL,
  `bz` varchar(50) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='销售退货';

-- ----------------------------
-- Records of jxc_xsth_father
-- ----------------------------

-- ----------------------------
-- Table structure for jxc_xsth_son
-- ----------------------------
DROP TABLE IF EXISTS `jxc_xsth_son`;
CREATE TABLE `jxc_xsth_son` (
  `id` varchar(36) NOT NULL COMMENT 'id',
  `bh` int(4) DEFAULT NULL COMMENT '编号',
  `spmc` varchar(100) DEFAULT NULL,
  `zbid` varchar(36) DEFAULT NULL COMMENT '主表id',
  `txm` varchar(50) DEFAULT NULL COMMENT '条形码',
  `gg` varchar(50) DEFAULT NULL COMMENT '规格',
  `dw` varchar(50) DEFAULT NULL COMMENT '单位',
  `sl` int(4) DEFAULT NULL COMMENT '数量',
  `dj` decimal(18,2) DEFAULT NULL COMMENT '单价',
  `xj` decimal(18,2) DEFAULT NULL COMMENT '小计',
  `bz` varchar(100) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='销售退货子表';

-- ----------------------------
-- Records of jxc_xsth_son
-- ----------------------------

-- ----------------------------
-- Table structure for st_user
-- ----------------------------
DROP TABLE IF EXISTS `st_user`;
CREATE TABLE `st_user` (
  `id` varchar(36) NOT NULL COMMENT 'id',
  `username` varchar(50) NOT NULL COMMENT '用户名',
  `password` varchar(50) NOT NULL COMMENT '密码',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户表';

-- ----------------------------
-- Records of st_user
-- ----------------------------
INSERT INTO `st_user` VALUES ('ef6cf8e5-474f-41b0-9c9f-6b4a40af4e78', 'admin', '123');
