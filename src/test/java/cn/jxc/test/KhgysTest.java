package cn.jxc.test;

import java.util.UUID;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import cn.jxc.model.Khgys;
import cn.jxc.service.KhgysService;

public class KhgysTest {
	protected static final Logger logger = Logger.getLogger(UserTest.class);
	private KhgysService khgysService;
	
	@Before
	public void before(){                                     
		@SuppressWarnings("resource")
		ApplicationContext context = new ClassPathXmlApplicationContext(new String[]{"classpath:conf/spring.xml"
				,"classpath:conf/spring-mybatis.xml"});
		khgysService = (KhgysService) context.getBean("khgysServiceImpl");
	}
	
	
	@Test
	public void addMore(){
		for (int i = 0; i < 5; i++) {
			add();
		}
	}
	
	
	public void add(){
		int total = khgysService.findAllCountByMc("");
		Khgys khgys = new Khgys();
		khgys.setId(UUID.randomUUID().toString());
		khgys.setMc("名称"+total);
		khgys.setSfgys("0");//是否供应商
		khgys.setSfkh("1");//是否客户
		khgys.setLxfs("联系方式");
		khgys.setBh(total);
		khgys.setDz("地址");
		khgys.setLxr("联系人");
		khgysService.insert(khgys);
		logger.debug(khgys.getId());
	}


}
