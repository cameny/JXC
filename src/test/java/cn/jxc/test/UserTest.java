package cn.jxc.test;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import cn.jxc.model.User;
import cn.jxc.service.UserService;

public class UserTest {
	protected static final Logger logger = Logger.getLogger(UserTest.class);
	private UserService userService;
	
	@Before
	public void before(){                                     
		@SuppressWarnings("resource")
		ApplicationContext context = new ClassPathXmlApplicationContext(new String[]{"classpath:conf/spring.xml"
				,"classpath:conf/spring-mybatis.xml"});
		userService = (UserService) context.getBean("userServiceImpl");
	}
	
	
	@Test
	public void add(){
		if (userService.selectByPrimaryKey("ef6cf8e5-474f-41b0-9c9f-6b4a40af4e78") == null) {
			User user = new User();
			//user.setId(UUID.randomUUID().toString());
			user.setId("ef6cf8e5-474f-41b0-9c9f-6b4a40af4e78");
			user.setUsername("admin");
			user.setPassword("123");
			userService.insert(user);
			logger.info(user.getId()+"添加成功");
		}
		else {
			logger.info("管理员存在");
		}
		
	}

}
