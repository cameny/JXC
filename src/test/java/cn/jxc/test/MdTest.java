package cn.jxc.test;

import java.util.UUID;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import cn.jxc.model.Md;
import cn.jxc.service.MdService;

public class MdTest {
	protected static final Logger logger = Logger.getLogger(UserTest.class);
	private MdService mdService;
	
	@Before
	public void before(){                                     
		@SuppressWarnings("resource")
		ApplicationContext context = new ClassPathXmlApplicationContext(new String[]{"classpath:conf/spring.xml"
				,"classpath:conf/spring-mybatis.xml"});
		mdService = (MdService) context.getBean("mdServiceImpl");
	}
	
	
	@Test
	public void add(){
		if (mdService.selectByPrimaryKey("3e0d01e7-832b-4087-b7d0-ee0a31b30519") != null ) {
			Md md = new Md();
			md.setId(UUID.randomUUID().toString());
			md.setDh("门店1");
			md.setGly("一号管理员");
			md.setMdmc("门店名称");
			mdService.insert(md);
			logger.info(md.getId()+"添加成功");
		}
	}

}
