package cn.jxc.test;

import java.math.BigDecimal;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import cn.jxc.model.Sp;
import cn.jxc.service.SpService;

public class SpTest {

	protected static final Logger logger = Logger.getLogger(UserTest.class);
	private SpService spService;
	
	@Before
	public void before(){                                     
		@SuppressWarnings("resource")
		ApplicationContext context = new ClassPathXmlApplicationContext(new String[]{"classpath:conf/spring.xml"
				,"classpath:conf/spring-mybatis.xml"});
		spService = (SpService) context.getBean("spServiceImpl");
	}
	
	@Test
	public void addMore(){
		for (int i = 0; i < 10; i++) {
			add();
		}
	}
	
	
	public void add(){
		int total = spService.findAllCountBySpmcAndZt("", "");
		Sp sp = new Sp();
		sp.setId(UUID.randomUUID().toString());
		sp.setSpmc("商品名称"+total);
		sp.setXh("商品型号"+total);
		sp.setGg("规格"+total);
		sp.setDw("个");
		sp.setSph("商品号"+total);
		sp.setTxm("条形码");
		sp.setSpbz("备注");
		sp.setCscbj(new BigDecimal(5.00));//初始成本价
		sp.setBj(new BigDecimal(3.00));//标价
		sp.setCskc(500);//初始库存
		sp.setKczdz(500);//库存最大值
		sp.setKczxz(0);//库存最小值
		sp.setZt("0");
		spService.insert(sp);
		
	}
	
}
