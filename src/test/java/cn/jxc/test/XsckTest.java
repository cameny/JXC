package cn.jxc.test;

import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import cn.jxc.model.XsckFather;
import cn.jxc.model.XsckSon;
import cn.jxc.service.XsckFatherService;
import cn.jxc.service.XsckSonService;

public class XsckTest {
	protected static final Logger logger = Logger.getLogger(UserTest.class);
	private XsckFatherService xsckFatherService;
	private XsckSonService xsckSonService;
	
	@Before
	public void before(){                                     
		@SuppressWarnings("resource")
		ApplicationContext context = new ClassPathXmlApplicationContext(new String[]{"classpath:conf/spring.xml"
				,"classpath:conf/spring-mybatis.xml"});
		xsckFatherService = (XsckFatherService) context.getBean("xsckFatherServiceImpl");
	}
	@Before
	public void before1(){                                     
		@SuppressWarnings("resource")
		ApplicationContext context = new ClassPathXmlApplicationContext(new String[]{"classpath:conf/spring.xml"
				,"classpath:conf/spring-mybatis.xml"});
		xsckSonService = (XsckSonService) context.getBean("xsckSonServiceImpl");
	}
	
	@Test
	public void addMore(){
		for (int i = 0; i < 50; i++) {
			addFather();
		}
	}
	
	
	public void addFather(){
		int xsckNum = xsckFatherService.findAllCountByTime("1990-01-01 00:00:00.000", new Date().toString());
		xsckNum=xsckNum+1;
		XsckFather xsckFather=new XsckFather();
		xsckFather.setId(UUID.randomUUID().toString());
		xsckFather.setKdsj(new Date());
		xsckFather.setDh("XS160806000"+xsckNum);//生成流水方法
		xsckFather.setKhmc("散户");
		xsckFather.setMdid("门店1");
		xsckFather.setKhr("系统管理员");
		xsckFather.setCky("张三");
		xsckFather.setCky("系统管理员");
		xsckFather.setRmbxx((float) 21);
		xsckFather.setRmbdx("贰拾壹元整");
		xsckFather.setBz("换货"+xsckNum);
		xsckFatherService.insert(xsckFather);
		XsckSon xsckSon = new XsckSon();
		for (int i = 1; i < 5; i++) {
			xsckSon.setId(UUID.randomUUID().toString());
			xsckSon.setZbid(xsckFather.getId());
			xsckSon.setBh(i);
			xsckSon.setDj(new BigDecimal("1.00"));
			xsckSon.setSl(3);
			xsckSon.setXj(new BigDecimal("3.00"));
			xsckSon.setDw("个");
			xsckSon.setTxm("txm"+i);
			xsckSonService.insert(xsckSon);
			logger.info("子表写入"+i);
		}
		logger.info(xsckFather.getId()+"添加成功");
	}

}
