package cn.jxc.util;

/***
 * @功能：	常量类
 * @author AfenG
 *
 */
public class Constant {
	/** 处理成功 **/
	public static final String DEAL_SUCCESS = "DEAL_SUCCESS";
	
	/** 处理失败 **/
	public static final String DEAL_FAIL = "DEAL_FAIL";
	
	/** 处理异常 **/
	public static final String DEAL_EXCEPTION = "DEAL_EXCEPTION";
	
	/** session中存放用户信息的key **/
	public static final String USERINFO = "USERINFO";

	/** 登录成功 **/ 
	public static final String LOGIN_SUCCESS = "LOGIN_SUCCESS";
	
	/** 登录失败 **/ 
	public static final String LOGIN_FAIL = "LOGIN_FAIL";
	
	/** 无权限登录  **/
	public static final String LOGIN_NOPOWER="LOGIN_NOPOWER";
	
	/** 管理员账号已存在 **/
	public static final String MANAGER_LOGINID_EXISTS = "MANAGER_LOGINID_EXISTS";
	
	/** 用户重名 **/
	public static final String MENU_NAME_EXISTS = "USER_NAME_EXISTS";
	
	/** session失效时间 **/
	public static final String SESSION_INVALID_TIME = "SESSION_INVALID_TIME";
	
	

}
