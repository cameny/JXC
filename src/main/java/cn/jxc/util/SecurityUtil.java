package cn.jxc.util;

import java.security.Key;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

public class SecurityUtil {
	
	public static String DES = "AES"; // optional value AES/DES/DESede  
    
    public static String CIPHER_ALGORITHM = "AES"; // optional value AES/DES/DESede  
    
    private static String Key = "love.520";
    
    private static String input = "ying1022";
      
  
    public static Key getSecretKey(String key) throws Exception{  
        SecretKey securekey = null;  
        if(key == null){  
            key = "";  
        }  
        KeyGenerator keyGenerator = KeyGenerator.getInstance(DES);  
        keyGenerator.init(new SecureRandom(key.getBytes()));  
        securekey = keyGenerator.generateKey();  
        return securekey;  
    }  
      
    public static String encrypt(String data) throws Exception {  
        SecureRandom sr = new SecureRandom();  
        Key securekey = getSecretKey(Key);  
        Cipher cipher = Cipher.getInstance(CIPHER_ALGORITHM);  
        cipher.init(Cipher.ENCRYPT_MODE, securekey, sr);  
        byte[] bt = cipher.doFinal(data.getBytes());  
        String strs = new BASE64Encoder().encode(bt);  
        return strs;  
    }  
    
    /**将二进制转换成16进制 
     * @param buf 
     * @return 
     */  
    public static String parseByte2HexStr(byte buf[]) {  
            StringBuffer sb = new StringBuffer();  
            for (int i = 0; i < buf.length; i++) {  
                    String hex = Integer.toHexString(buf[i] & 0xFF);  
                    if (hex.length() == 1) {  
                            hex = '0' + hex;  
                    }  
                    sb.append(hex.toUpperCase());  
            }  
            return sb.toString();  
    }  
    
    /**将16进制转换为二进制 
     * @param hexStr 
     * @return 
     */  
    public static byte[] parseHexStr2Byte(String hexStr) {  
            if (hexStr.length() < 1)  
                    return null;  
            byte[] result = new byte[hexStr.length()/2];  
            for (int i = 0;i< hexStr.length()/2; i++) {  
                    int high = Integer.parseInt(hexStr.substring(i*2, i*2+1), 16);  
                    int low = Integer.parseInt(hexStr.substring(i*2+1, i*2+2), 16);  
                    result[i] = (byte) (high * 16 + low);  
            }  
            return result;  
    }  
      
      
    public static String detrypt(String message) throws Exception{  
        SecureRandom sr = new SecureRandom();  
        Cipher cipher = Cipher.getInstance(CIPHER_ALGORITHM);  
        Key securekey = getSecretKey(Key);  
        cipher.init(Cipher.DECRYPT_MODE, securekey,sr);  
        byte[] res = new BASE64Decoder().decodeBuffer(message);  
        res = cipher.doFinal(res);  
        return new String(res);  
    }
    
    /***
     * 进行8位数字的混淆
     * @param message
     * @return
     */
    public static String confuseEntrypt(String message){
    	StringBuilder sb = new StringBuilder(message);
    	for (int i = 0; i < input.length(); i++) {    		
    		sb.insert(3*i, input.charAt(i));
		}
    	String outSb=sb.toString();
    	return outSb.substring(0, outSb.length()-2);
    }
    
    /***
     * 解除8位数字的混淆
     * @param message
     * @return
     */
    public static String unConfuseEntrypt(String message) {
		message = message +"==";
		StringBuffer sb = new StringBuffer(message);
		sb.deleteCharAt(21);
		sb.deleteCharAt(18);
		sb.deleteCharAt(15);
		sb.deleteCharAt(12);
		sb.deleteCharAt(9);
		sb.deleteCharAt(6);
		sb.deleteCharAt(3);
		sb.deleteCharAt(0);
    	return sb.toString();
	}
    
    /***
     * 加密和混淆
     * @return
     * @throws Exception 
     */
    public static String getLast(String message) throws Exception {
    	String entryptedMsg = null;
		entryptedMsg = encrypt(message);
		return confuseEntrypt(entryptedMsg);
	}
    
    /***
     * 解混和解密
     * @return
     * @throws Exception 
     */
    public static String putLast(String message) throws Exception {
		String entryptedMsg = unConfuseEntrypt(message);
		entryptedMsg=detrypt(entryptedMsg);
		return entryptedMsg;
	}
    

    
    
      
    public static void main(String[] args)throws Exception{  
        /*String message = "password";  
        String key = Key;
        String entryptedMsg = encrypt(message,key);  
        System.out.println("加密后:");  
        System.out.println(entryptedMsg);  
        System.out.println(entryptedMsg.length());  
        String decryptedMsg = detrypt(entryptedMsg,key);  
        System.out.println("解密后:");  
        System.out.println(decryptedMsg);  */
    	
    	String msString="123";
    	
    	String hunxiao = SecurityUtil.getLast(msString);
    	System.out.println(hunxiao);
    	String jiehun = SecurityUtil.putLast(hunxiao);
    	System.out.println(jiehun);
    }  

}
