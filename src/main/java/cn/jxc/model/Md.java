package cn.jxc.model;

public class Md {
    private String id;

    private String mdmc;

    private String gly;

    private String dh;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getMdmc() {
        return mdmc;
    }

    public void setMdmc(String mdmc) {
        this.mdmc = mdmc == null ? null : mdmc.trim();
    }

    public String getGly() {
        return gly;
    }

    public void setGly(String gly) {
        this.gly = gly == null ? null : gly.trim();
    }

    public String getDh() {
        return dh;
    }

    public void setDh(String dh) {
        this.dh = dh == null ? null : dh.trim();
    }
}