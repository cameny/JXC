package cn.jxc.model;

public class Khgys {
    private String id;

    private String mc;

    private Integer bh;

    private String lxr;

    private String lxfs;

    private String dz;

    private String sfkh;

    private String sfgys;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getMc() {
        return mc;
    }

    public void setMc(String mc) {
        this.mc = mc == null ? null : mc.trim();
    }

    public Integer getBh() {
        return bh;
    }

    public void setBh(Integer bh) {
        this.bh = bh;
    }

    public String getLxr() {
        return lxr;
    }

    public void setLxr(String lxr) {
        this.lxr = lxr == null ? null : lxr.trim();
    }

    public String getLxfs() {
        return lxfs;
    }

    public void setLxfs(String lxfs) {
        this.lxfs = lxfs == null ? null : lxfs.trim();
    }

    public String getDz() {
        return dz;
    }

    public void setDz(String dz) {
        this.dz = dz == null ? null : dz.trim();
    }

    public String getSfkh() {
        return sfkh;
    }

    public void setSfkh(String sfkh) {
        this.sfkh = sfkh == null ? null : sfkh.trim();
    }

    public String getSfgys() {
        return sfgys;
    }

    public void setSfgys(String sfgys) {
        this.sfgys = sfgys == null ? null : sfgys.trim();
    }
}