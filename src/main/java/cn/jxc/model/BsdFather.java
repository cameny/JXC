package cn.jxc.model;

public class BsdFather {
    private String id;

    private String kdsj;

    private String dh;

    private String mdid;

    private String cky;

    private String bz;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getKdsj() {
        return kdsj;
    }

    public void setKdsj(String kdsj) {
        this.kdsj = kdsj == null ? null : kdsj.trim();
    }

    public String getDh() {
        return dh;
    }

    public void setDh(String dh) {
        this.dh = dh == null ? null : dh.trim();
    }

    public String getMdid() {
        return mdid;
    }

    public void setMdid(String mdid) {
        this.mdid = mdid == null ? null : mdid.trim();
    }

    public String getCky() {
        return cky;
    }

    public void setCky(String cky) {
        this.cky = cky == null ? null : cky.trim();
    }

    public String getBz() {
        return bz;
    }

    public void setBz(String bz) {
        this.bz = bz == null ? null : bz.trim();
    }
}