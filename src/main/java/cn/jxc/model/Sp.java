package cn.jxc.model;

import java.math.BigDecimal;

public class Sp {
    private String id;

    private String spmc;

    private String xh;

    private String gg;

    private String dw;

    private String sph;

    private String txm;

    private String spbz;

    private BigDecimal cscbj;

    private BigDecimal bj;

    private Integer cskc;

    private Integer kczdz;

    private Integer kczxz;

    private String zt;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getSpmc() {
        return spmc;
    }

    public void setSpmc(String spmc) {
        this.spmc = spmc == null ? null : spmc.trim();
    }

    public String getXh() {
        return xh;
    }

    public void setXh(String xh) {
        this.xh = xh == null ? null : xh.trim();
    }

    public String getGg() {
        return gg;
    }

    public void setGg(String gg) {
        this.gg = gg == null ? null : gg.trim();
    }

    public String getDw() {
        return dw;
    }

    public void setDw(String dw) {
        this.dw = dw == null ? null : dw.trim();
    }

    public String getSph() {
        return sph;
    }

    public void setSph(String sph) {
        this.sph = sph == null ? null : sph.trim();
    }

    public String getTxm() {
        return txm;
    }

    public void setTxm(String txm) {
        this.txm = txm == null ? null : txm.trim();
    }

    public String getSpbz() {
        return spbz;
    }

    public void setSpbz(String spbz) {
        this.spbz = spbz == null ? null : spbz.trim();
    }

    public BigDecimal getCscbj() {
        return cscbj;
    }

    public void setCscbj(BigDecimal cscbj) {
        this.cscbj = cscbj;
    }

    public BigDecimal getBj() {
        return bj;
    }

    public void setBj(BigDecimal bj) {
        this.bj = bj;
    }

    public Integer getCskc() {
        return cskc;
    }

    public void setCskc(Integer cskc) {
        this.cskc = cskc;
    }

    public Integer getKczdz() {
        return kczdz;
    }

    public void setKczdz(Integer kczdz) {
        this.kczdz = kczdz;
    }

    public Integer getKczxz() {
        return kczxz;
    }

    public void setKczxz(Integer kczxz) {
        this.kczxz = kczxz;
    }

    public String getZt() {
        return zt;
    }

    public void setZt(String zt) {
        this.zt = zt == null ? null : zt.trim();
    }
}