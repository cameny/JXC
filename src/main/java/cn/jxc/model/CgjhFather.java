package cn.jxc.model;

import java.util.Date;

public class CgjhFather {
    private String id;

    private Date kdsj;

    private String dh;

    private String khmc;

    private String lxfs;

    private String dz;

    private String mdid;

    private String khr;

    private String cky;

    private String rmbdx;

    private Float rmbxx;

    private String bz;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public Date getKdsj() {
        return kdsj;
    }

    public void setKdsj(Date kdsj) {
        this.kdsj = kdsj;
    }

    public String getDh() {
        return dh;
    }

    public void setDh(String dh) {
        this.dh = dh == null ? null : dh.trim();
    }

    public String getKhmc() {
        return khmc;
    }

    public void setKhmc(String khmc) {
        this.khmc = khmc == null ? null : khmc.trim();
    }

    public String getLxfs() {
        return lxfs;
    }

    public void setLxfs(String lxfs) {
        this.lxfs = lxfs == null ? null : lxfs.trim();
    }

    public String getDz() {
        return dz;
    }

    public void setDz(String dz) {
        this.dz = dz == null ? null : dz.trim();
    }

    public String getMdid() {
        return mdid;
    }

    public void setMdid(String mdid) {
        this.mdid = mdid == null ? null : mdid.trim();
    }

    public String getKhr() {
        return khr;
    }

    public void setKhr(String khr) {
        this.khr = khr == null ? null : khr.trim();
    }

    public String getCky() {
        return cky;
    }

    public void setCky(String cky) {
        this.cky = cky == null ? null : cky.trim();
    }

    public String getRmbdx() {
        return rmbdx;
    }

    public void setRmbdx(String rmbdx) {
        this.rmbdx = rmbdx == null ? null : rmbdx.trim();
    }

    public Float getRmbxx() {
        return rmbxx;
    }

    public void setRmbxx(Float rmbxx) {
        this.rmbxx = rmbxx;
    }

    public String getBz() {
        return bz;
    }

    public void setBz(String bz) {
        this.bz = bz == null ? null : bz.trim();
    }
}