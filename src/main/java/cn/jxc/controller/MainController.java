package cn.jxc.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cn.jxc.model.User;
import cn.jxc.service.UserService;
import cn.jxc.util.Constant;

@Controller

public class MainController extends BaseController {

	@Autowired
	private UserService userService;
	
	/*@RequestMapping("index")
	public String index(){
		//System.out.println(111);
		return "index";
	}*/
	
	@RequestMapping("/")
	public String index(HttpSession session){
		User user = (User)session.getAttribute(Constant.USERINFO);
		if (null != user) {
			return "/admin/homeFtl";
		}		
		return "/index";
	}
	
	@RequestMapping(value ="/admin/login",method =RequestMethod.POST)
	public String login(User user,HttpSession session,HttpServletRequest request,Model model){
		user = userService.findByNameAndPassword(user.getUsername(),user.getPassword());
		if (user !=null) {
			request.getSession().setAttribute(Constant.USERINFO, user);
			model.addAttribute("msg", Constant.LOGIN_SUCCESS);
			return "/admin/homeFtl";
		}
		else{
			model.addAttribute("msg", Constant.LOGIN_FAIL);
			return "/index";
		}
	}
	
	@RequestMapping(value = "/admin/logout")
	public String loginout(HttpSession session){
		session.removeAttribute(Constant.USERINFO);
		session.invalidate();
		// 跳转登录
		return "/index";
	}
	
}
