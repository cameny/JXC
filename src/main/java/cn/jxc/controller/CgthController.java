package cn.jxc.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.jxc.model.CgjhFather;
import cn.jxc.model.CgjhSon;
import cn.jxc.model.CgthFather;
import cn.jxc.model.CgthSon;
import cn.jxc.model.User;
import cn.jxc.service.CgthFatherService;
import cn.jxc.service.CgthSonService;
import cn.jxc.util.Constant;
import cn.jxc.util.Page;

@Controller
public class CgthController extends BaseController {

	@Autowired
	private CgthFatherService cgthFatherService;
	@Autowired
	private CgthSonService cgthSonService;
	
	/***
	 * 采购退货页面加载
	 * @param session
	 * @param model
	 * @param beginDate
	 * @param endDate
	 * @return
	 */
	@RequestMapping(value="/cgth/list")
	public String list(HttpSession session,Model model,@RequestParam(value="begindate",defaultValue="1990-01-01 00:00:00.000")String beginDate,@RequestParam(value="enddate",defaultValue="2018-12-31 23:59:59.000")String endDate){
		User user = (User)session.getAttribute(Constant.USERINFO);
		//登录用户信息
		model.addAttribute("user", user);
		return "/admin/cgth/cgth_list";		
	}
	
	/***
	 * 采购退货分页列表
	 * @param session
	 * @param model
	 * @param beginDate
	 * @param endDate
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/cgth/load" , method = RequestMethod.POST)
	public String load(HttpSession session,Model model,@RequestParam(value="begindate",defaultValue="1990-01-01 00:00:00.000")String beginDate,@RequestParam(value="enddate",defaultValue="2018-12-31 23:59:59.000")String endDate,HttpServletRequest request){
		User user = (User)session.getAttribute(Constant.USERINFO);
		List<CgthFather> cgthFatherList = new ArrayList<CgthFather>();

		//登录用户信息
		model.addAttribute("user", user);
		String pageNow = request.getParameter("pageNow");
		Page page = null;
		int totalCount = cgthFatherService.findAllCountByTime(beginDate, endDate);
		
		if (pageNow !=null) {
			page = new Page(totalCount, Integer.parseInt(pageNow));
			cgthFatherList = cgthFatherService.findAllByTime(beginDate, endDate, page.getStartPos(), page.getPageSize());
		}else{
			page = new Page(totalCount, 1);
			cgthFatherList = cgthFatherService.findAllByTime(beginDate, endDate, page.getStartPos(), page.getPageSize());
		}
		//内容列表
		model.addAttribute("cgthFatherList", cgthFatherList);
		model.addAttribute("pager", page);
		return "/admin/cgth/cgth_pager_list";		
	}
	
	/****
	 * 删除采购退货单，并删除子表
	 * @param id
	 * @param model
	 */
	@RequestMapping(value ="/cgth/del/{id}",method=RequestMethod.DELETE)
	@ResponseBody
	public void del(@PathVariable("id")String id,Model model){
		//删除主表和子表的数据
		cgthFatherService.deleteByPrimaryKey(id);
		cgthSonService.deleteByZbid(id);
		model.addAttribute("resultCode", Constant.DEAL_SUCCESS);
		model.addAttribute("errorInfo", "删除成功");
	}
	
	/***
	 * 显示采购退货详情
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/cgth/show/{id}" ,method = RequestMethod.POST)
	public String show(@PathVariable("id")String id,Model model){
		try {
			//主表和子表
			CgthFather cgthFather = cgthFatherService.selectByPrimaryKey(id);
			//子表为多项，所以为列表
			List<CgthSon> cgthSonList = cgthSonService.selectByZbid(id);
			model.addAttribute("cgthFather", cgthFather);
			model.addAttribute("cgthSon", cgthSonList);
			model.addAttribute("msg", Constant.DEAL_SUCCESS);
			logger.info("显示详情:"+id);
		} catch (Exception e) {
			model.addAttribute("errorInfo", "显示商户失败");
			logger.info("显示失败");
			e.getStackTrace();
		}		
		return "/admin/cgth/cgth_show";
	}
}
