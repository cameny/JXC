package cn.jxc.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.jxc.model.CgjhFather;
import cn.jxc.model.CgjhSon;
import cn.jxc.model.User;
import cn.jxc.service.CgjhFatherService;
import cn.jxc.service.CgjhSonService;
import cn.jxc.util.Constant;
import cn.jxc.util.Page;

@Controller
public class CgjhController extends BaseController {

	@Autowired
	private CgjhFatherService cgjhFatherService;
	@Autowired
	private CgjhSonService cgjhSonService;
	
	/***
	 * 采购交货页面加载
	 * @param session
	 * @param model
	 * @param beginDate
	 * @param endDate
	 * @return
	 */
	@RequestMapping(value="/cgjh/list")
	public String list(HttpSession session,Model model,@RequestParam(value="begindate",defaultValue="1990-01-01 00:00:00.000")String beginDate,@RequestParam(value="enddate",defaultValue="2018-12-31 23:59:59.000")String endDate){
		User user = (User)session.getAttribute(Constant.USERINFO);
		//登录用户信息
		model.addAttribute("user", user);
		return "/admin/cgjh/cgjh_list";		
	}
	
	/***
	 * 采购交货分页列表
	 * @param session
	 * @param model
	 * @param beginDate
	 * @param endDate
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/cgjh/load" , method = RequestMethod.POST)
	public String load(HttpSession session,Model model,@RequestParam(value="begindate",defaultValue="1990-01-01 00:00:00.000")String beginDate,@RequestParam(value="enddate",defaultValue="2018-12-31 23:59:59.000")String endDate,HttpServletRequest request){
		User user = (User)session.getAttribute(Constant.USERINFO);
		List<CgjhFather> cgjhFatherList = new ArrayList<CgjhFather>();

		//登录用户信息
		model.addAttribute("user", user);
		String pageNow = request.getParameter("pageNow");
		Page page = null;
		int totalCount = cgjhFatherService.findAllCountByTime(beginDate, endDate);
		
		if (pageNow !=null) {
			page = new Page(totalCount, Integer.parseInt(pageNow));
			cgjhFatherList = cgjhFatherService.findAllByTime(beginDate, endDate, page.getStartPos(), page.getPageSize());
		}else{
			page = new Page(totalCount, 1);
			cgjhFatherList = cgjhFatherService.findAllByTime(beginDate, endDate, page.getStartPos(), page.getPageSize());
		}
		//内容列表
		model.addAttribute("cgjhFatherList", cgjhFatherList);
		model.addAttribute("pager", page);
		return "/admin/cgjh/cgjh_pager_list";		
	}
	
	/****
	 * 删除采购交货单，并删除子表
	 * @param id
	 * @param model
	 */
	@RequestMapping(value ="/cgjh/del/{id}",method=RequestMethod.DELETE)
	@ResponseBody
	public void del(@PathVariable("id")String id,Model model){
		//删除主表和子表的数据
		cgjhFatherService.deleteByPrimaryKey(id);
		cgjhSonService.deleteByZbid(id);
		model.addAttribute("resultCode", Constant.DEAL_SUCCESS);
		model.addAttribute("errorInfo", "删除成功");
	}
	
	/***
	 * 显示采购交货详情
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/cgjh/show/{id}" ,method = RequestMethod.POST)
	public String show(@PathVariable("id")String id,Model model){
		try {
			//主表和子表
			CgjhFather cgjhFather = cgjhFatherService.selectByPrimaryKey(id);
			//子表为多项，所以为列表
			List<CgjhSon> cgjhSonList = cgjhSonService.selectByZbid(id);
			model.addAttribute("cgjhFather", cgjhFather);
			model.addAttribute("cgjhSon", cgjhSonList);
			model.addAttribute("msg", Constant.DEAL_SUCCESS);
			logger.info("显示详情:"+id);
		} catch (Exception e) {
			model.addAttribute("errorInfo", "显示商户失败");
			logger.info("显示失败");
			e.getStackTrace();
		}		
		return "/admin/cgjh/cgjh_show";
	}
}
