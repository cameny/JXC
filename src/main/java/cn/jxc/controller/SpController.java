package cn.jxc.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sun.org.apache.xpath.internal.operations.Mod;

import cn.jxc.model.Sp;
import cn.jxc.model.User;
import cn.jxc.service.SpService;
import cn.jxc.util.Constant;
import cn.jxc.util.Page;

@Controller
public class SpController extends BaseController {

	@Autowired
	private SpService spService;
	
	/***
	 * 商品加载
	 * @param session
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/sp/list")
	public String list(HttpSession session,Model model){
		User user = (User)session.getAttribute(Constant.USERINFO);
		//登录用户信息
		model.addAttribute("user", user);
		return "/admin/sp/sp_list";		
	}
	
	/***
	 * 商品列表
	 * @param session
	 * @param model
	 * @param request
	 * @param spmc
	 * @param zt
	 * @return
	 */
	@RequestMapping(value="/sp/load" , method = RequestMethod.POST)
	public String load(HttpSession session,Model model,HttpServletRequest request,String spmc,String zt){
		User user = (User)session.getAttribute(Constant.USERINFO);
		//登录用户信息
		model.addAttribute("user", user);
		List<Sp> spList = new ArrayList<Sp>();
				//spService.findAllBySpmcAndZt(spmc,zt);
		model.addAttribute("sp", spList);
		
		String pageNow = request.getParameter("pageNow");
		Page page = null;
		int totalCount = spService.findAllCountBySpmcAndZt(spmc, zt);
		
		if (pageNow !=null) {
			page = new Page(totalCount, Integer.parseInt(pageNow));
			spList = spService.findAllBySpmcAndZt(spmc, zt, page.getStartPos(), page.getPageSize());
		}else{
			page = new Page(totalCount, 1);
			spList = spService.findAllBySpmcAndZt(spmc, zt, page.getStartPos(), page.getPageSize());
		}
		//内容列表
		model.addAttribute("spList", spList);
		model.addAttribute("pager", page);
		return "/admin/sp/sp_pager_list";
	}
	
	/****
	 * 删除商品
	 * @param id
	 * @param model
	 */
	@RequestMapping(value ="/sp/del/{id}",method=RequestMethod.DELETE)
	@ResponseBody
	public void del(@PathVariable("id")String id,Model model){
		//删除主表和子表的数据
		spService.deleteByPrimaryKey(id);
		model.addAttribute("resultCode", Constant.DEAL_SUCCESS);
		model.addAttribute("errorInfo", "删除成功");
	}
	
	
	@RequestMapping(value="/sp/insert")
	public String insert(Model model,HttpSession session){
		User user = (User)session.getAttribute(Constant.USERINFO);
		//登录用户信息
		model.addAttribute("user", user);
		return "/admin/sp/sp_add";
	}
	
	/***
	 * 添加商品
	 * @param sp
	 * @param model
	 */
	@RequestMapping(value="/sp/add",method=RequestMethod.POST)
	@ResponseBody
	public void add(Sp sp,Model model){
		sp.setId(UUID.randomUUID().toString());
		sp.setZt("0");
		spService.insert(sp);
		model.addAttribute("resultCode", Constant.DEAL_SUCCESS);
		model.addAttribute("errorInfo", "添加成功");
		//return "redirect:/admin/sp/list";	
	}
	
	/***
	 * 页面跳转
	 * @param model
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/sp/editJump/{id}")
	public String editJump(Model model,@PathVariable String id){
		Sp sp = spService.selectByPrimaryKey(id);
		model.addAttribute("sp",sp);
		return "/admin/sp/sp_edit";
	}
	
	/***
	 * 修改商品
	 * @param md
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/sp/edit",method=RequestMethod.POST)
	public String edit(Sp sp,Model model){
		spService.updateByPrimaryKey(sp);
		model.addAttribute("resultCode", Constant.DEAL_SUCCESS);
		model.addAttribute("errorInfo", "编辑成功");
		return "/admin/sp/sp_list";
	}
	
	
	/***
	 * 显示商品详情
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/sp/show/{id}" ,method = RequestMethod.POST)
	public String show(@PathVariable("id")String id,Model model){
		try {
			Sp sp = spService.selectByPrimaryKey(id);
			model.addAttribute("sp", sp);
			model.addAttribute("msg", Constant.DEAL_SUCCESS);
			logger.info("显示详情:"+id);
		} catch (Exception e) {
			model.addAttribute("errorInfo", "显示商品失败");
			logger.info("显示失败");
			e.getStackTrace();
		}
		return "/admin/sp/sp_show";
	}
	
	/***
	 * 手机检索列表
	 * @param spmc
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/app",method = RequestMethod.GET)
	public String showApp(@RequestParam(value="spmc", required =false)String spmc,Model model){
		List<Sp> spList = spService.findAllBySpmc(spmc);
		model.addAttribute("sp", spList);
		return "/admin/sp/app";
	}
	
	
}
