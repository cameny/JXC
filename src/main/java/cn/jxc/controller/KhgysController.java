package cn.jxc.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.jxc.model.Khgys;
import cn.jxc.model.User;
import cn.jxc.service.KhgysService;
import cn.jxc.util.Constant;
import cn.jxc.util.Page;

@Controller
public class KhgysController extends BaseController{

	@Autowired
	private KhgysService khgysService;
	
	
	/***
	 * 客户供应商加载
	 * @param session
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/khgys/list")
	public String list(HttpSession session,Model model){
		User user = (User)session.getAttribute(Constant.USERINFO);
		//登录用户信息
		model.addAttribute("user", user);
		return "/admin/khgys/khgys_list";		
	}
	
	/***
	 * 客户供应商列表
	 * @param session
	 * @param model
	 * @param request
	 * @param spmc
	 * @param zt
	 * @return
	 */
	@RequestMapping(value="/khgys/load" , method = RequestMethod.POST)
	public String load(HttpSession session,Model model,HttpServletRequest request,String mc){
		User user = (User)session.getAttribute(Constant.USERINFO);
		//登录用户信息
		model.addAttribute("user", user);
		List<Khgys> khgyList = new ArrayList<Khgys>();
				//spService.findAllBySpmcAndZt(spmc,zt);
		model.addAttribute("khgys", khgyList);
		
		String pageNow = request.getParameter("pageNow");
		Page page = null;
		int totalCount = khgysService.findAllCountByMc(mc);
		
		if (pageNow !=null) {
			page = new Page(totalCount, Integer.parseInt(pageNow));
			khgyList = khgysService.findAllByMc(mc, page.getStartPos(), page.getPageSize());
		}else{
			page = new Page(totalCount, 1);
			khgyList = khgysService.findAllByMc(mc, page.getStartPos(), page.getPageSize());
		}
		//内容列表
		model.addAttribute("khgysList", khgyList);
		model.addAttribute("pager", page);
		return "/admin/khgys/khgys_pager_list";
	}
	
	/****
	 * 删除客户供应商
	 * @param id
	 * @param model
	 */
	@RequestMapping(value ="/khgys/del/{id}",method=RequestMethod.DELETE)
	@ResponseBody
	public void del(@PathVariable("id")String id,Model model){
		//删除主表和子表的数据
		khgysService.deleteByPrimaryKey(id);
		model.addAttribute("resultCode", Constant.DEAL_SUCCESS);
		model.addAttribute("errorInfo", "删除成功");
	}
	
	
	@RequestMapping(value="/khgys/insert")
	public String insert(Model model,HttpSession session){
		User user = (User)session.getAttribute(Constant.USERINFO);
		//登录用户信息
		model.addAttribute("user", user);
		return "/admin/khgys/khgys_add";
	}
	
	/***
	 * 添加客户供应商
	 * @param sp
	 * @param model
	 */
	@RequestMapping(value="/khgys/add",method=RequestMethod.POST)
	@ResponseBody
	public void add(Khgys khgys,Model model){
		khgys.setId(UUID.randomUUID().toString());
		khgysService.insert(khgys);
		model.addAttribute("resultCode", Constant.DEAL_SUCCESS);
		model.addAttribute("errorInfo", "添加成功");
		//return "/admin/khgys/list";	
	}
	
	/***
	 * 页面跳转
	 * @param model
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/khgys/editJump/{id}")
	public String editJump(Model model,@PathVariable String id){
		Khgys khgys = khgysService.selectByPrimaryKey(id);
		model.addAttribute("khgys",khgys);
		return "/admin/khgys/khgys_edit";
	}
	
	/***
	 * 修改客户供应商
	 * @param md
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/khgys/edit",method=RequestMethod.POST)
	public String edit(Khgys khgys,Model model){
		khgysService.updateByPrimaryKey(khgys);
		model.addAttribute("resultCode", Constant.DEAL_SUCCESS);
		model.addAttribute("errorInfo", "编辑成功");
		return "/admin/khgys/khgys_list";
	}
	
	
	/***
	 * 显示客户供应商详情
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/khgys/show/{id}" ,method = RequestMethod.POST)
	public String show(@PathVariable("id")String id,Model model){
		try {
			Khgys khgys = khgysService.selectByPrimaryKey(id);
			model.addAttribute("khgys", khgys);
			model.addAttribute("msg", Constant.DEAL_SUCCESS);
			logger.info("显示详情:"+id);
		} catch (Exception e) {
			model.addAttribute("errorInfo", "显示客户供应商失败");
			logger.info("显示失败");
			e.getStackTrace();
		}
		return "/admin/khgys/khgys_show";
	}
}
