package cn.jxc.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.jxc.model.User;
import cn.jxc.model.XsckSon;
import cn.jxc.model.XsthFather;
import cn.jxc.service.XsthFatherService;
import cn.jxc.service.XsthSonService;
import cn.jxc.util.Constant;
import cn.jxc.util.Page;

@Controller
public class XsthController extends BaseController {

	@Autowired
	private XsthFatherService xsthFatherService;
	@Autowired
	private XsthSonService xsthSonService;
	
	/***
	 * 销售退货页面加载
	 * @param session
	 * @param model
	 * @param beginDate
	 * @param endDate
	 * @return
	 */
	@RequestMapping(value="/xsth/list")
	public String list(HttpSession session,Model model,@RequestParam(value="begindate",defaultValue="1990-01-01 00:00:00.000")String beginDate,@RequestParam(value="enddate",defaultValue="2018-12-31 23:59:59.000")String endDate){
		User user = (User)session.getAttribute(Constant.USERINFO);
		//登录用户信息
		model.addAttribute("user", user);
		return "/admin/xsth/xsth_list";		
	}
	
	
	/***
	 * 销售退货分页列表
	 * @param session
	 * @param model
	 * @param beginDate
	 * @param endDate
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/xsth/load" , method = RequestMethod.POST)
	public String load(HttpSession session,Model model,@RequestParam(value="begindate",defaultValue="1990-01-01 00:00:00.000")String beginDate,@RequestParam(value="enddate",defaultValue="2018-12-31 23:59:59.000")String endDate,HttpServletRequest request){
		User user = (User)session.getAttribute(Constant.USERINFO);
		List<XsthFather> XsthFatherList = new ArrayList<XsthFather>();

		//登录用户信息
		model.addAttribute("user", user);
		String pageNow = request.getParameter("pageNow");
		Page page = null;
		int totalCount = xsthFatherService.findAllCountByTime(beginDate, endDate);
		
		if (pageNow !=null) {
			page = new Page(totalCount, Integer.parseInt(pageNow));
			XsthFatherList = xsthFatherService.findAllByTime(beginDate, endDate, page.getStartPos(), page.getPageSize());
		}else{
			page = new Page(totalCount, 1);
			XsthFatherList = xsthFatherService.findAllByTime(beginDate, endDate, page.getStartPos(), page.getPageSize());
		}
		//内容列表
		model.addAttribute("XsthFatherList", XsthFatherList);
		model.addAttribute("pager", page);
		return "/admin/xsth/xsth_pager_list";		
	}
	
	
	/****
	 * 删除出库单，并删除子表
	 * @param id
	 * @param model
	 */
	@RequestMapping(value ="/xsth/del/{id}",method=RequestMethod.DELETE)
	@ResponseBody
	public void del(@PathVariable("id")String id,Model model){
		//删除主表和子表的数据
		xsthFatherService.deleteByPrimaryKey(id);
		xsthSonService.deleteByZbid(id);
		model.addAttribute("resultCode", Constant.DEAL_SUCCESS);
		model.addAttribute("errorInfo", "删除成功");
	}
	
	/***
	 * 显示销售出库详情
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/xsth/show/{id}" ,method = RequestMethod.POST)
	public String show(@PathVariable("id")String id,Model model){
		try {
			//主表和子表
			XsthFather XsthFather = xsthFatherService.selectByPrimaryKey(id);
			//子表为多项，所以为列表
			List<XsckSon> xsthSonList = xsthSonService.selectByZbid(id);
			model.addAttribute("XsthFather", XsthFather);
			model.addAttribute("xsthSon", xsthSonList);
			model.addAttribute("msg", Constant.DEAL_SUCCESS);
			logger.info("显示详情:"+id);
		} catch (Exception e) {
			model.addAttribute("errorInfo", "显示商户失败");
			logger.info("显示失败");
			e.getStackTrace();
		}		
		return "/admin/xsth/xsth_show";
	}
	
	
	
}
