package cn.jxc.controller;

import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.jxc.model.Md;
import cn.jxc.model.User;
import cn.jxc.service.MdService;
import cn.jxc.util.Constant;

/***
 * 门店
 * @author AfenG
 *
 */
@Controller
public class MdController extends BaseController {

	@Autowired
	private MdService mdService;
	
	
	/***
	 * 门店页面加载
	 * @param session
	 * @param model
	 * @param beginDate
	 * @param endDate
	 * @return
	 */
	@RequestMapping(value="/md/list")
	public String list(HttpSession session,Model model,@RequestParam(value="begindate",defaultValue="1990-01-01 00:00:00.000")String beginDate,@RequestParam(value="enddate",defaultValue="2018-12-31 23:59:59.000")String endDate){
		User user = (User)session.getAttribute(Constant.USERINFO);
		//登录用户信息
		model.addAttribute("user", user);
		return "/admin/md/md_list";		
	}
	
	/***
	 * 门店列表
	 * @param session
	 * @param model
	 * @param beginDate
	 * @param endDate
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/md/load" , method = RequestMethod.POST)
	public String load(HttpSession session,Model model){
		User user = (User)session.getAttribute(Constant.USERINFO);
		//登录用户信息
		model.addAttribute("user", user);
		List<Md> mdList = mdService.findAll();
		model.addAttribute("md", mdList);
		return "/admin/md/md_pager_list";		
	}
	
	/****
	 * 删除出库单，并删除子表
	 * @param id
	 * @param model
	 */
	@RequestMapping(value ="/md/del/{id}",method=RequestMethod.DELETE)
	@ResponseBody
	public void del(@PathVariable("id")String id,Model model){
		//删除主表和子表的数据
		mdService.deleteByPrimaryKey(id);
		model.addAttribute("resultCode", Constant.DEAL_SUCCESS);
		model.addAttribute("errorInfo", "删除成功");
	}
	
	
	@RequestMapping(value="/md/insert")
	public String insert(Model model,HttpSession session){
		User user = (User)session.getAttribute(Constant.USERINFO);
		//登录用户信息
		model.addAttribute("user", user);
		return "/admin/md/md_add";
	}
	
	/***
	 * 添加门店
	 * @param md
	 * @param model
	 */
	@RequestMapping(value="/md/add",method=RequestMethod.POST)
	public String add(Md md,Model model){
		md.setId(UUID.randomUUID().toString());
		mdService.insert(md);
		model.addAttribute("resultCode", Constant.DEAL_SUCCESS);
		model.addAttribute("errorInfo", "添加成功");
		return "/admin/md/md_list";	
	}
	
	/***
	 * 页面跳转
	 * @param model
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/md/editJump/{id}")
	public String editJump(Model model,@PathVariable String id){
		Md md = mdService.selectByPrimaryKey(id);
		model.addAttribute("md",md);
		return "/admin/md/md_edit";
	}
	
	/***
	 * 修改门店
	 * @param md
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/md/edit",method=RequestMethod.POST)
	public String edit(Md md,Model model){
		mdService.updateByPrimaryKey(md);
		model.addAttribute("resultCode", Constant.DEAL_SUCCESS);
		model.addAttribute("errorInfo", "编辑成功");
		return "/admin/md/md_list";
	}
}
