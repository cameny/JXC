package cn.jxc.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.jxc.model.User;
import cn.jxc.model.XsckFather;
import cn.jxc.model.XsckSon;
import cn.jxc.service.XsckFatherService;
import cn.jxc.service.XsckSonService;
import cn.jxc.util.Constant;
import cn.jxc.util.Page;

@Controller
public class XsckController extends BaseController{
	
	@Autowired
	private XsckFatherService xsckFatherService;
	
	@Autowired
	private XsckSonService xsckSonService;
	
	/***
	 * 销售出库页面加载
	 * @param session
	 * @param model
	 * @param beginDate
	 * @param endDate
	 * @return
	 */
	@RequestMapping(value="/xsck/list")
	public String list(HttpSession session,Model model,@RequestParam(value="begindate",defaultValue="1990-01-01 00:00:00.000")String beginDate,@RequestParam(value="enddate",defaultValue="2018-12-31 23:59:59.000")String endDate){
		User user = (User)session.getAttribute(Constant.USERINFO);
		//登录用户信息
		model.addAttribute("user", user);
		return "/admin/xsck/xsck_list";		
	}
	
	/***
	 * 销售出库分页列表
	 * @param session
	 * @param model
	 * @param beginDate
	 * @param endDate
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/xsck/load" , method = RequestMethod.POST)
	public String load(HttpSession session,Model model,@RequestParam(value="begindate",defaultValue="1990-01-01 00:00:00.000")String beginDate,@RequestParam(value="enddate",defaultValue="2018-12-31 23:59:59.000")String endDate,HttpServletRequest request){
		User user = (User)session.getAttribute(Constant.USERINFO);
		List<XsckFather> xsckFatherList = new ArrayList<XsckFather>();

		//登录用户信息
		model.addAttribute("user", user);
		String pageNow = request.getParameter("pageNow");
		Page page = null;
		int totalCount = xsckFatherService.findAllCountByTime(beginDate, endDate);
		
		if (pageNow !=null) {
			page = new Page(totalCount, Integer.parseInt(pageNow));
			xsckFatherList = xsckFatherService.findAllByTime(beginDate, endDate, page.getStartPos(), page.getPageSize());
		}else{
			page = new Page(totalCount, 1);
			xsckFatherList = xsckFatherService.findAllByTime(beginDate, endDate, page.getStartPos(), page.getPageSize());
		}
		//内容列表
		model.addAttribute("xsckFatherList", xsckFatherList);
		model.addAttribute("pager", page);
		return "/admin/xsck/xsck_pager_list";		
	}
	
	/****
	 * 删除出库单，并删除子表
	 * @param id
	 * @param model
	 */
	@RequestMapping(value ="/xsck/del/{id}",method=RequestMethod.DELETE)
	@ResponseBody
	public void del(@PathVariable("id")String id,Model model){
		//删除主表和子表的数据
		xsckFatherService.deleteByPrimaryKey(id);
		xsckSonService.deleteByZbid(id);
		model.addAttribute("resultCode", Constant.DEAL_SUCCESS);
		model.addAttribute("errorInfo", "删除成功");
	}
	
	/***
	 * 显示销售出库详情
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/xsck/show/{id}" ,method = RequestMethod.POST)
	public String show(@PathVariable("id")String id,Model model){
		try {
			//主表和子表
			XsckFather xsckFather = xsckFatherService.selectByPrimaryKey(id);
			//子表为多项，所以为列表
			List<XsckSon> xsckSonList = xsckSonService.selectByZbid(id);
			model.addAttribute("xsckFather", xsckFather);
			model.addAttribute("xsckSon", xsckSonList);
			model.addAttribute("msg", Constant.DEAL_SUCCESS);
			logger.info("显示详情:"+id);
		} catch (Exception e) {
			model.addAttribute("errorInfo", "显示商户失败");
			logger.info("显示失败");
			e.getStackTrace();
		}		
		return "/admin/xsck/xsck_show";
	}
	
	@RequestMapping(value = "/xsck/add")
	public String add(HttpSession session,Model model){
		User user = (User)session.getAttribute(Constant.USERINFO);
		//登录用户信息
		model.addAttribute("user", user);
		model.addAttribute("time", new Date());
		return "/admin/xsck/xsck_add";
	}
}
