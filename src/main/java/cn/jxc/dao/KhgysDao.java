package cn.jxc.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cn.jxc.model.Khgys;

public interface KhgysDao {
    int deleteByPrimaryKey(String id);

    int insert(Khgys record);

    int insertSelective(Khgys record);

    Khgys selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(Khgys record);

    int updateByPrimaryKey(Khgys record);
    
    public int findAllCountByMc(@Param("mc")String mc);
    
    public List<Khgys> findAllByMc(@Param("mc")String mc,@Param("startPos")int startPos,@Param("pageSize")int pageSize);
}