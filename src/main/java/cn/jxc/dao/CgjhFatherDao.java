package cn.jxc.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cn.jxc.model.CgjhFather;

public interface CgjhFatherDao {
    int deleteByPrimaryKey(String id);

    int insert(CgjhFather record);

    int insertSelective(CgjhFather record);

    CgjhFather selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(CgjhFather record);

    int updateByPrimaryKey(CgjhFather record);
    
    public List<CgjhFather> findAllByTime(@Param("begindate")String beginDate,@Param("enddate")String endDate,@Param("startPos")int startPos,@Param("pageSize")int pageSize);
    
    public int findAllCountByTime(@Param("begindate")String beginDate,@Param("enddate")String endDate);
}