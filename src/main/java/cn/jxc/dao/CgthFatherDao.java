package cn.jxc.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cn.jxc.model.CgthFather;

public interface CgthFatherDao {
    int deleteByPrimaryKey(String id);

    int insert(CgthFather record);

    int insertSelective(CgthFather record);

    CgthFather selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(CgthFather record);

    int updateByPrimaryKey(CgthFather record);
    
    public List<CgthFather> findAllByTime(@Param("begindate")String beginDate,@Param("enddate")String endDate,@Param("startPos")int startPos,@Param("pageSize")int pageSize);
    
    public int findAllCountByTime(@Param("begindate")String beginDate,@Param("enddate")String endDate);
}