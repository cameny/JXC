package cn.jxc.dao;

import cn.jxc.model.BsdSon;

public interface BsdSonDao {
    int deleteByPrimaryKey(String id);

    int insert(BsdSon record);

    int insertSelective(BsdSon record);

    BsdSon selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(BsdSon record);

    int updateByPrimaryKey(BsdSon record);
}