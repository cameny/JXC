package cn.jxc.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cn.jxc.model.Sp;

public interface SpDao {
    int deleteByPrimaryKey(String id);

    int insert(Sp record);

    int insertSelective(Sp record);

    Sp selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(Sp record);

    int updateByPrimaryKey(Sp record);
    
    public int findAllCountBySpmcAndZt(@Param("spmc")String spmc,@Param("zt")String zt);
    
    public List<Sp> findAllBySpmcAndZt(@Param("spmc")String spmc,@Param("zt")String zt,@Param("startPos")int startPos,@Param("pageSize")int pageSize);
    
    public List<Sp> findAllBySpmc(@Param("spmc")String spmc);
}