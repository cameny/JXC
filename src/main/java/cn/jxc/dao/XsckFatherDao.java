package cn.jxc.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cn.jxc.model.XsckFather;

public interface XsckFatherDao {
    int deleteByPrimaryKey(String id);

    int insert(XsckFather record);

    int insertSelective(XsckFather record);

    XsckFather selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(XsckFather record);

    int updateByPrimaryKey(XsckFather record);
    
    public List<XsckFather> findAllByTime(@Param("begindate")String beginDate,@Param("enddate")String endDate,@Param("startPos")int startPos,@Param("pageSize")int pageSize);
    
    public int findAllCountByTime(@Param("begindate")String beginDate,@Param("enddate")String endDate);
}