package cn.jxc.dao;

import org.apache.ibatis.annotations.Param;

import cn.jxc.model.User;

public interface UserDao {
    int deleteByPrimaryKey(String id);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);
    
    public User findByNameAndPassword(@Param("username")String name,@Param("password")String password);
}