package cn.jxc.dao;

import java.util.List;

import cn.jxc.model.XsckSon;
import cn.jxc.model.XsthSon;

public interface XsthSonDao {
    int deleteByPrimaryKey(String id);

    int insert(XsthSon record);

    int insertSelective(XsthSon record);

    XsthSon selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(XsthSon record);

    int updateByPrimaryKey(XsthSon record);
    
    public List<XsckSon> selectByZbid(String id);
    
    public int deleteByZbid(String id);
}