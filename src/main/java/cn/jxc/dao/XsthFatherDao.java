package cn.jxc.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cn.jxc.model.XsthFather;

public interface XsthFatherDao {
    int deleteByPrimaryKey(String id);

    int insert(XsthFather record);

    int insertSelective(XsthFather record);

    XsthFather selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(XsthFather record);

    int updateByPrimaryKey(XsthFather record);
    
    public int findAllCountByTime(@Param("begindate")String begindate,@Param("enddate")String enddate);
    
    public List<XsthFather> findAllByTime(@Param("begindate")String begindate,@Param("enddate")String enddate,@Param("startPos")int startPos,@Param("pageSize") int pageSize);
}