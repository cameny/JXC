package cn.jxc.service;

import java.util.List;

import cn.jxc.model.CgjhSon;

public interface CgjhSonService {
	int deleteByPrimaryKey(String id);

    int insert(CgjhSon record);

    int insertSelective(CgjhSon record);

    CgjhSon selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(CgjhSon record);

    int updateByPrimaryKey(CgjhSon record);
    
    public List<CgjhSon> selectByZbid(String id);
    
    public int deleteByZbid(String id);

}
