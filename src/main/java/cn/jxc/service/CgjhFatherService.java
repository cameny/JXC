package cn.jxc.service;

import java.util.List;

import cn.jxc.model.CgjhFather;

public interface CgjhFatherService {
	  int deleteByPrimaryKey(String id);

	    int insert(CgjhFather record);

	    int insertSelective(CgjhFather record);

	    CgjhFather selectByPrimaryKey(String id);

	    int updateByPrimaryKeySelective(CgjhFather record);

	    int updateByPrimaryKey(CgjhFather record);

	    public List<CgjhFather> findAllByTime(String beginDate,String endDate,int startPos,int pageSize);
	    
	    public int findAllCountByTime(String beginDate,String endDate);
}
