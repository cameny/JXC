package cn.jxc.service;

import java.util.List;

import cn.jxc.model.Sp;

public interface SpService {
	int deleteByPrimaryKey(String id);

    int insert(Sp record);

    int insertSelective(Sp record);

    Sp selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(Sp record);

    int updateByPrimaryKey(Sp record);
    
    public int findAllCountBySpmcAndZt(String spmc,String zt);
    
    public List<Sp> findAllBySpmcAndZt(String spmc,String zt,int startPos,int pageSize);
    
    public List<Sp> findAllBySpmc(String spmc);

}
