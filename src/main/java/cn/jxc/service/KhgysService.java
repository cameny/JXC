package cn.jxc.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cn.jxc.model.Khgys;

public interface KhgysService {
	  int deleteByPrimaryKey(String id);

	    int insert(Khgys record);

	    int insertSelective(Khgys record);

	    Khgys selectByPrimaryKey(String id);

	    int updateByPrimaryKeySelective(Khgys record);

	    int updateByPrimaryKey(Khgys record);
	    
	    public int findAllCountByMc(String mc);
	    
	    public List<Khgys> findAllByMc(String mc,int startPos,int pageSize);

}
