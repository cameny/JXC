package cn.jxc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.jxc.dao.XsthSonDao;
import cn.jxc.model.XsckSon;
import cn.jxc.model.XsthSon;
import cn.jxc.service.XsthSonService;

@Service
public class XsthSonServiceImpl implements XsthSonService {

	@Autowired
	private XsthSonDao xsthSonDao;
	
	
	
	public int deleteByPrimaryKey(String id) {
		return xsthSonDao.deleteByPrimaryKey(id);
	}

	
	public int insert(XsthSon record) {
		return xsthSonDao.insert(record);
	}

	
	public int insertSelective(XsthSon record) {
		return xsthSonDao.insertSelective(record);
	}

	
	public XsthSon selectByPrimaryKey(String id) {
		return xsthSonDao.selectByPrimaryKey(id);
	}

	
	public int updateByPrimaryKeySelective(XsthSon record) {
		return xsthSonDao.updateByPrimaryKeySelective(record);
	}

	
	public int updateByPrimaryKey(XsthSon record) {
		return xsthSonDao.updateByPrimaryKey(record);
	}


	@Override
	public List<XsckSon> selectByZbid(String id) {
		return xsthSonDao.selectByZbid(id);
	}


	@Override
	public int deleteByZbid(String id) {
		return xsthSonDao.deleteByZbid(id);
	}

}
