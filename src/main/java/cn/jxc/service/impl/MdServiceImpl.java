package cn.jxc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.jxc.dao.MdDao;
import cn.jxc.model.Md;
import cn.jxc.service.MdService;


@Service
public class MdServiceImpl implements MdService {

	@Autowired
	private MdDao mdDao;
	
	
	public int deleteByPrimaryKey(String id) {
		return mdDao.deleteByPrimaryKey(id);
	}

	
	public int insert(Md record) {
		return mdDao.insert(record);
	}

	
	public int insertSelective(Md record) {
		return mdDao.insertSelective(record);
	}

	
	public Md selectByPrimaryKey(String id) {
		return mdDao.selectByPrimaryKey(id);
	}

	
	public int updateByPrimaryKeySelective(Md record) {
		return mdDao.updateByPrimaryKeySelective(record);
	}

	
	public int updateByPrimaryKey(Md record) {
		return mdDao.updateByPrimaryKey(record);
	}


	@Override
	public List<Md> findAll() {
		return mdDao.findAll();
	}

}
