package cn.jxc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.jxc.dao.XsthFatherDao;
import cn.jxc.model.XsthFather;
import cn.jxc.service.XsthFatherService;


@Service
public class XsthFatherServiceImpl implements XsthFatherService {

	@Autowired
	private XsthFatherDao xsthFatherDao;
	
	
	public int deleteByPrimaryKey(String id) {
		return xsthFatherDao.deleteByPrimaryKey(id);
	}

	
	public int insert(XsthFather record) {
		return xsthFatherDao.insert(record);
	}

	
	public int insertSelective(XsthFather record) {
		return xsthFatherDao.insertSelective(record);
	}

	
	public XsthFather selectByPrimaryKey(String id) {
		return xsthFatherDao.selectByPrimaryKey(id);
	}

	
	public int updateByPrimaryKeySelective(XsthFather record) {
		return xsthFatherDao.updateByPrimaryKeySelective(record);
	}

	
	public int updateByPrimaryKey(XsthFather record) {
		return xsthFatherDao.updateByPrimaryKey(record);
	}


	@Override
	public int findAllCountByTime(String begindate, String enddate) {
		return xsthFatherDao.findAllCountByTime(begindate, enddate);
	}


	@Override
	public List<XsthFather> findAllByTime(String begindate, String enddate, int startPos, int pageSize) {
		return xsthFatherDao.findAllByTime(begindate, enddate, startPos, pageSize);
	}

}
