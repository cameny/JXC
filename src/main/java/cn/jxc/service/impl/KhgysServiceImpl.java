package cn.jxc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.jxc.dao.KhgysDao;
import cn.jxc.model.Khgys;
import cn.jxc.service.KhgysService;


@Service
public class KhgysServiceImpl implements KhgysService {

	@Autowired
	private KhgysDao khgysDao;
	
	
	public int deleteByPrimaryKey(String id) {
		return khgysDao.deleteByPrimaryKey(id);
	}

	
	public int insert(Khgys record) {
		return khgysDao.insert(record);
	}

	
	public int insertSelective(Khgys record) {
		return khgysDao.insertSelective(record);
	}

	
	public Khgys selectByPrimaryKey(String id) {
		return khgysDao.selectByPrimaryKey(id);
	}

	
	public int updateByPrimaryKeySelective(Khgys record) {
		return khgysDao.updateByPrimaryKeySelective(record);
	}

	
	public int updateByPrimaryKey(Khgys record) {
		return khgysDao.updateByPrimaryKey(record);
	}


	@Override
	public int findAllCountByMc(String mc) {
		return khgysDao.findAllCountByMc(mc);
	}


	@Override
	public List<Khgys> findAllByMc(String mc, int startPos, int pageSize) {
		return khgysDao.findAllByMc(mc, startPos, pageSize);
	}

}
