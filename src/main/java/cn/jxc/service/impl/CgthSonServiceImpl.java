package cn.jxc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.jxc.dao.CgthSonDao;
import cn.jxc.model.CgthSon;
import cn.jxc.service.CgthSonService;


@Service
public class CgthSonServiceImpl implements CgthSonService {

	@Autowired
	private CgthSonDao cgthSonDao;
	
	
	public int deleteByPrimaryKey(String id) {
		return cgthSonDao.deleteByPrimaryKey(id);
	}

	
	public int insert(CgthSon record) {
		return cgthSonDao.insert(record);
	}

	
	public int insertSelective(CgthSon record) {
		return cgthSonDao.insertSelective(record);
	}

	
	public CgthSon selectByPrimaryKey(String id) {
		return cgthSonDao.selectByPrimaryKey(id);
	}

	
	public int updateByPrimaryKeySelective(CgthSon record) {
		return cgthSonDao.updateByPrimaryKeySelective(record);
	}

	
	public int updateByPrimaryKey(CgthSon record) {
		return cgthSonDao.updateByPrimaryKey(record);
	}


	@Override
	public List<CgthSon> selectByZbid(String id) {
		return cgthSonDao.selectByZbid(id);
	}


	@Override
	public int deleteByZbid(String id) {
		return cgthSonDao.deleteByZbid(id);
	}

}
