package cn.jxc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.jxc.dao.XsckFatherDao;
import cn.jxc.model.XsckFather;
import cn.jxc.service.XsckFatherService;

@Service
public class XsckFatherServiceImpl implements XsckFatherService {

	@Autowired
	private XsckFatherDao xsckFatherDao;
	
	
	public int deleteByPrimaryKey(String id) {
		return xsckFatherDao.deleteByPrimaryKey(id);
	}

	
	public int insert(XsckFather record) {
		return xsckFatherDao.insert(record);
	}

	
	public int insertSelective(XsckFather record) {
		return xsckFatherDao.insertSelective(record);
	}

	
	public XsckFather selectByPrimaryKey(String id) {
		return xsckFatherDao.selectByPrimaryKey(id);
	}

	
	public int updateByPrimaryKeySelective(XsckFather record) {
		return xsckFatherDao.updateByPrimaryKeySelective(record);
	}

	
	public int updateByPrimaryKey(XsckFather record) {
		return xsckFatherDao.updateByPrimaryKey(record);
	}


	@Override
	public List<XsckFather> findAllByTime(String beginDate, String endDate,int startPos,int pageSize) {
		return xsckFatherDao.findAllByTime(beginDate, endDate,startPos,pageSize);
	}


	@Override
	public int findAllCountByTime(String beginDate, String endDate) {
		return xsckFatherDao.findAllCountByTime(beginDate, endDate);
	}

}
