package cn.jxc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.jxc.dao.SpDao;
import cn.jxc.model.Sp;
import cn.jxc.service.SpService;


@Service
public class SpServiceImpl implements SpService {

	@Autowired
	private SpDao spDao;
	
	
	public int deleteByPrimaryKey(String id) {
		return spDao.deleteByPrimaryKey(id);
	}

	
	public int insert(Sp record) {
		return spDao.insert(record);
	}

	
	public int insertSelective(Sp record) {
		return spDao.insertSelective(record);
	}

	
	public Sp selectByPrimaryKey(String id) {
		return spDao.selectByPrimaryKey(id);
	}

	
	public int updateByPrimaryKeySelective(Sp record) {
		return spDao.updateByPrimaryKeySelective(record);
	}

	
	public int updateByPrimaryKey(Sp record) {
		return spDao.updateByPrimaryKey(record);
	}


	@Override
	public int findAllCountBySpmcAndZt(String spmc, String zt) {
		return spDao.findAllCountBySpmcAndZt(spmc, zt);
	}


	@Override
	public List<Sp> findAllBySpmcAndZt(String spmc, String zt, int startPos, int pageSize) {
		return spDao.findAllBySpmcAndZt(spmc, zt, startPos, pageSize);
	}


	@Override
	public List<Sp> findAllBySpmc(String spmc) {
		return spDao.findAllBySpmc(spmc);
	}

}
