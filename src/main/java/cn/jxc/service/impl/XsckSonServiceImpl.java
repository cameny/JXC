package cn.jxc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.jxc.dao.XsckSonDao;
import cn.jxc.model.XsckSon;
import cn.jxc.service.XsckSonService;


@Service
public class XsckSonServiceImpl implements XsckSonService {

	@Autowired
	private XsckSonDao xsckSonDao;
	
	
	public int deleteByPrimaryKey(String id) {
		return xsckSonDao.deleteByPrimaryKey(id);
	}

	
	public int insert(XsckSon record) {
		return xsckSonDao.insert(record);
	}

	
	public int insertSelective(XsckSon record) {
		return xsckSonDao.insertSelective(record);
	}

	
	public XsckSon selectByPrimaryKey(String id) {
		return xsckSonDao.selectByPrimaryKey(id);
	}

	
	public int updateByPrimaryKeySelective(XsckSon record) {
		return xsckSonDao.updateByPrimaryKeySelective(record);
	}

	
	public int updateByPrimaryKey(XsckSon record) {
		return xsckSonDao.updateByPrimaryKey(record);
	}


	@Override
	public List<XsckSon> selectByZbid(String id) {
		return xsckSonDao.selectByZbid(id);
	}


	@Override
	public int deleteByZbid(String id) {
		return xsckSonDao.deleteByZbid(id);
	}

}
