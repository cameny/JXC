package cn.jxc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.jxc.dao.CgjhSonDao;
import cn.jxc.model.CgjhSon;
import cn.jxc.service.CgjhSonService;

@Service
public class CgjhSonServiceImpl implements CgjhSonService {

	@Autowired
	private CgjhSonDao cgjhSonDao;
	
	
	public int deleteByPrimaryKey(String id) {
		return cgjhSonDao.deleteByPrimaryKey(id);
	}

	
	public int insert(CgjhSon record) {
		return cgjhSonDao.insert(record);
	}

	
	public int insertSelective(CgjhSon record) {
		return cgjhSonDao.insertSelective(record);
	}

	
	public CgjhSon selectByPrimaryKey(String id) {
		return cgjhSonDao.selectByPrimaryKey(id);
	}

	
	public int updateByPrimaryKeySelective(CgjhSon record) {
		return cgjhSonDao.updateByPrimaryKeySelective(record);
	}

	
	public int updateByPrimaryKey(CgjhSon record) {
		return cgjhSonDao.updateByPrimaryKey(record);
	}


	@Override
	public List<CgjhSon> selectByZbid(String id) {
		return cgjhSonDao.selectByZbid(id);
	}


	@Override
	public int deleteByZbid(String id) {
		return cgjhSonDao.deleteByZbid(id);
	}

}
