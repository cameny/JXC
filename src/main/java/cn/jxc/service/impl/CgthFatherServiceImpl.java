package cn.jxc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.jxc.dao.CgthFatherDao;
import cn.jxc.model.CgthFather;
import cn.jxc.service.CgthFatherService;

@Service
public class CgthFatherServiceImpl implements CgthFatherService {

	@Autowired
	private CgthFatherDao cgthFatherDao;
	
	
	public int deleteByPrimaryKey(String id) {
		return cgthFatherDao.deleteByPrimaryKey(id);
	}

	
	public int insert(CgthFather record) {
		return cgthFatherDao.insert(record);
	}

	
	public int insertSelective(CgthFather record) {
		return cgthFatherDao.insertSelective(record);
	}

	
	public CgthFather selectByPrimaryKey(String id) {
		return cgthFatherDao.selectByPrimaryKey(id);
	}

	
	public int updateByPrimaryKeySelective(CgthFather record) {
		return cgthFatherDao.updateByPrimaryKeySelective(record);
	}

	
	public int updateByPrimaryKey(CgthFather record) {
		return cgthFatherDao.updateByPrimaryKey(record);
	}


	@Override
	public List<CgthFather> findAllByTime(String beginDate, String endDate, int startPos, int pageSize) {
		return cgthFatherDao.findAllByTime(beginDate, endDate, startPos, pageSize);
	}


	@Override
	public int findAllCountByTime(String beginDate, String endDate) {
		return cgthFatherDao.findAllCountByTime(beginDate, endDate);
	}



}
