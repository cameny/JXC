package cn.jxc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.jxc.dao.CgjhFatherDao;
import cn.jxc.model.CgjhFather;
import cn.jxc.service.CgjhFatherService;

@Service
public class CgjhFatherServiceImpl implements CgjhFatherService {

	@Autowired
	private CgjhFatherDao cgjhFatherDao;
	
	
	public int deleteByPrimaryKey(String id) {
		return cgjhFatherDao.deleteByPrimaryKey(id);
	}

	
	public int insert(CgjhFather record) {
		return cgjhFatherDao.insert(record);
	}

	
	public int insertSelective(CgjhFather record) {
		return cgjhFatherDao.insertSelective(record);
	}

	
	public CgjhFather selectByPrimaryKey(String id) {
		return cgjhFatherDao.selectByPrimaryKey(id);
	}

	
	public int updateByPrimaryKeySelective(CgjhFather record) {
		return cgjhFatherDao.updateByPrimaryKeySelective(record);
	}

	
	public int updateByPrimaryKey(CgjhFather record) {
		return cgjhFatherDao.updateByPrimaryKey(record);
	}


	@Override
	public List<CgjhFather> findAllByTime(String beginDate, String endDate, int startPos, int pageSize) {
		return cgjhFatherDao.findAllByTime(beginDate, endDate, startPos, pageSize);
	}


	@Override
	public int findAllCountByTime(String beginDate, String endDate) {
		return cgjhFatherDao.findAllCountByTime(beginDate, endDate);
	}

}
