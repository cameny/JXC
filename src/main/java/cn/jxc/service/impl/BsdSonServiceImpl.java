package cn.jxc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.jxc.dao.BsdSonDao;
import cn.jxc.model.BsdSon;
import cn.jxc.service.BsdSonService;

@Service
public class BsdSonServiceImpl implements BsdSonService {

	@Autowired
	private BsdSonDao bsdSonDao;
	
	
	public int deleteByPrimaryKey(String id) {
		return bsdSonDao.deleteByPrimaryKey(id);
	}

	
	public int insert(BsdSon record) {
		return bsdSonDao.insert(record);
	}

	
	public int insertSelective(BsdSon record) {
		return bsdSonDao.insertSelective(record);
	}

	
	public BsdSon selectByPrimaryKey(String id) {
		return bsdSonDao.selectByPrimaryKey(id);
	}

	
	public int updateByPrimaryKeySelective(BsdSon record){
		return bsdSonDao.updateByPrimaryKeySelective(record);
	}

	
	public int updateByPrimaryKey(BsdSon record){
		return bsdSonDao.updateByPrimaryKey(record);
	}

}
