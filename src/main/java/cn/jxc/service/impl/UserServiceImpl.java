package cn.jxc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.jxc.dao.UserDao;
import cn.jxc.model.User;
import cn.jxc.service.UserService;


@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDao;
	
	
	public int deleteByPrimaryKey(String id) {
		return userDao.deleteByPrimaryKey(id);
	}

	
	public int insert(User record) {
		return userDao.insert(record);
	}

	
	public int insertSelective(User record) {
		return userDao.insertSelective(record);
	}

	
	public User selectByPrimaryKey(String id) {
		return userDao.selectByPrimaryKey(id);
	}

	
	public int updateByPrimaryKeySelective(User record) {
		return userDao.updateByPrimaryKeySelective(record);
	}

	
	public int updateByPrimaryKey(User record) {
		return userDao.updateByPrimaryKey(record);
	}


	public User findByNameAndPassword(String name, String password) {
		return userDao.findByNameAndPassword(name, password);
	}

}
