package cn.jxc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.jxc.dao.BsdFatherDao;
import cn.jxc.model.BsdFather;
import cn.jxc.service.BsdFatherService;

@Service
public class BsdFatherServiceImpl implements BsdFatherService {

	@Autowired
	private BsdFatherDao bsdFatherDao;
	
	public int deleteByPrimaryKey(String id) {
		return bsdFatherDao.deleteByPrimaryKey(id);
	}

	
	public int insert(BsdFather record) {
		return bsdFatherDao.insert(record);
	}

	
	public int insertSelective(BsdFather record) {
		return bsdFatherDao.insertSelective(record);
	}

	
	public BsdFather selectByPrimaryKey(String id) {
		return bsdFatherDao.selectByPrimaryKey(id);
	}

	
	public int updateByPrimaryKeySelective(BsdFather record) {
		return bsdFatherDao.updateByPrimaryKeySelective(record);
	}

	
	public int updateByPrimaryKey(BsdFather record) {
		return bsdFatherDao.updateByPrimaryKey(record);
	}

}
