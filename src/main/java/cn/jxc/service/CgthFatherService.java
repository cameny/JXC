package cn.jxc.service;

import java.util.List;

import cn.jxc.model.CgthFather;

public interface CgthFatherService {
	 int deleteByPrimaryKey(String id);

	    int insert(CgthFather record);

	    int insertSelective(CgthFather record);

	    CgthFather selectByPrimaryKey(String id);

	    int updateByPrimaryKeySelective(CgthFather record);

	    int updateByPrimaryKey(CgthFather record);
	    
	    public List<CgthFather> findAllByTime(String beginDate,String endDate,int startPos,int pageSize);
	    
	    public int findAllCountByTime(String beginDate,String endDate);
}
