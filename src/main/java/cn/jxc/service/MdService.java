package cn.jxc.service;

import java.util.List;

import cn.jxc.model.Md;

public interface MdService {
	int deleteByPrimaryKey(String id);

    int insert(Md record);

    int insertSelective(Md record);

    Md selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(Md record);

    int updateByPrimaryKey(Md record);
    
    public List<Md> findAll();

}
