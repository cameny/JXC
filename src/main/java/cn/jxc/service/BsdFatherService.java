package cn.jxc.service;

import cn.jxc.model.BsdFather;

public interface BsdFatherService {

    int deleteByPrimaryKey(String id);

    int insert(BsdFather record);

    int insertSelective(BsdFather record);

    BsdFather selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(BsdFather record);

    int updateByPrimaryKey(BsdFather record);
}
