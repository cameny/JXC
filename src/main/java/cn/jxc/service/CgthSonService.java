package cn.jxc.service;

import java.util.List;

import cn.jxc.model.CgthSon;

public interface CgthSonService {
	int deleteByPrimaryKey(String id);

    int insert(CgthSon record);

    int insertSelective(CgthSon record);

    CgthSon selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(CgthSon record);

    int updateByPrimaryKey(CgthSon record);
    
    public List<CgthSon> selectByZbid(String id);
    
    public int deleteByZbid(String id);

}
