package cn.jxc.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cn.jxc.model.XsckFather;

public interface XsckFatherService {
	 int deleteByPrimaryKey(String id);

	    int insert(XsckFather record);

	    int insertSelective(XsckFather record);

	    XsckFather selectByPrimaryKey(String id);

	    int updateByPrimaryKeySelective(XsckFather record);

	    int updateByPrimaryKey(XsckFather record);

	    public List<XsckFather> findAllByTime(String beginDate,String endDate,int startPos,int pageSize);
	    
	    public int findAllCountByTime(String beginDate,String endDate);
}
