package cn.jxc.service;

import cn.jxc.model.User;

public interface UserService {
	int deleteByPrimaryKey(String id);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    public User findByNameAndPassword(String name,String password);
}
