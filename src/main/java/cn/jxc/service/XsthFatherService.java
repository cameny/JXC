package cn.jxc.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cn.jxc.model.XsthFather;

public interface XsthFatherService {
	 int deleteByPrimaryKey(String id);

	    int insert(XsthFather record);

	    int insertSelective(XsthFather record);

	    XsthFather selectByPrimaryKey(String id);

	    int updateByPrimaryKeySelective(XsthFather record);

	    int updateByPrimaryKey(XsthFather record);
	    
	    public int findAllCountByTime(String begindate,String enddate);

	    public List<XsthFather> findAllByTime(String begindate,String enddate,int startPos,int pageSize);
}
