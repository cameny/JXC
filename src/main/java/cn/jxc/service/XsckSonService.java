package cn.jxc.service;

import java.util.List;

import cn.jxc.model.XsckSon;

public interface XsckSonService {
    int deleteByPrimaryKey(String id);

    int insert(XsckSon record);

    int insertSelective(XsckSon record);

    XsckSon selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(XsckSon record);

    int updateByPrimaryKey(XsckSon record);

    public List<XsckSon> selectByZbid(String id);
    
    public int deleteByZbid(String id);
}
