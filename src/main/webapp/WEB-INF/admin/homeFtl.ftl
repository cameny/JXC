<#assign ctx=request.contextPath>
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="renderer" content="webkit">
    <link href="${ctx}/static/pc/css/bootstrap.min.css" rel="stylesheet">
    <link href="${ctx}/static/pc/font-awesome/css/font-awesome.css"  rel="stylesheet">
    <!-- Morris -->
    <link href="${ctx}/static/pc/css/plugins/morris/morris-0.4.3.min.css"  rel="stylesheet">
    <!-- Gritter -->
    <link href="${ctx}/static/pc/js/plugins/gritter/jquery.gritter.css"  rel="stylesheet">
    <link href="${ctx}/static/pc/css/animate.css"  rel="stylesheet">
    <link href="${ctx}/static/pc/css/style.css"  rel="stylesheet">
    <link href="${ctx}/static/pc/css/plugins/toastr/toastr.min.css" rel="stylesheet">
	
	<style>
		.wel-text{height:813px;background:#FFF url(${ctx}/static/pc/img/home.jpg) center 100px no-repeat;}
	</style>
</head>

<body>
    <div id="wrapper">
        <#include "/admin/menuFtl.ftl">
        <div id="page-wrapper" class="gray-bg dashbard-1">           
            <#include "/admin/leftTopFtl.ftl">
            <#include "/admin/footerFtl.ftl">            
        </div>
    </div>
    <!-- Mainly scripts -->
    <script src="${ctx}/static/pc/js/jquery-2.1.1.min.js" ></script>
    <script src="${ctx}/static/pc/js/bootstrap.min.js" ></script>
    <script src="${ctx}/static/pc/js/plugins/metisMenu/jquery.metisMenu.js" ></script>
    <script src="${ctx}/static/pc/js/plugins/slimscroll/jquery.slimscroll.min.js" ></script>

    <!-- Custom and plugin javascript -->
    <script src="${ctx}/static/pc/js/hplus.js" ></script>
    <script src="${ctx}/static/pc/js/plugins/pace/pace.min.js" ></script>
    <script src="${ctx}/static/pc/js/plugins/toastr/toastr.min.js"></script>
	
	<script>
		toastr.options = {
					  "closeButton": true,
					  "debug": false,
					  "progressBar": true,
					  "positionClass": "toast-bottom-right",
					  "onclick": null,
					  "showDuration": "50",
					  "hideDuration": "100",
					  "timeOut": "1200",
					  "extendedTimeOut": "100",
					  "showEasing": "swing",
					  "hideEasing": "linear",
					  "showMethod": "fadeIn",
					  "hideMethod": "fadeOut"
		}
		
		/* $().ready(function(){
			toastr.info("登录成功");
		}) */
	</script>
</body>

</html>