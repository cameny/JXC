<#assign ctx=request.contextPath>
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"><meta name="renderer" content="webkit">
    <link href="${ctx}/static/pc/css/bootstrap.min.css"  rel="stylesheet">
    <link href="${ctx}/static/pc/font-awesome/css/font-awesome.css"  rel="stylesheet">
    <!-- Data Tables -->
    <link href="${ctx}/static/pc/css/plugins/dataTables/dataTables.bootstrap.css"  rel="stylesheet">
    <link href="${ctx}/static/pc/css/animate.css" rel="stylesheet">
    <link href="${ctx}/static/pc/css/style.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="${ctx}/static/pc/js/flavr/flavr/css/animate.css" />
    <link rel="stylesheet" type="text/css" href="${ctx}/static/pc/js/flavr/flavr/css/flavr.css" />
    <link rel="stylesheet" href="${ctx}/static/pc//js/zTree_v3/css/zTreeStyle/zTreeStyle.css" type="text/css">
	<style>
		.btn-circle {
		  width: 20px;
		  height: 20px;
		  border-radius: 10px;
		}
		.fa{		
		  position: relative;
		  top: -3px;
		}
	</style>
</head>

<body>
    <div id="wrapper">
    	<#include "/admin/menuFtl.ftl">
    	<div id="page-wrapper" class="gray-bg dashbard-1">
	    	<#include "/admin/leftTopFtl.ftl">
	    	
	    	<div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-content">
								<div class="row">
	                                    <div class="col-sm-3">
		                                        <div class="input-group">
		                                            <input type="text" id="mc" placeholder="名称" class="input-sm form-control" style="font-size: 12px;">
		                                        </div>
	                                    </div> 
	                                    <div class="col-sm-3">
		                                        <div class="input-group">
		                                            <!--<input type="text" id="keyword" placeholder="请输入商户名称" class="input-sm form-control" style="font-size: 12px;">-->
		                                            <span class="input-group-btn">
		                                        		<button type="button" class="btn btn-sm btn-primary" onclick="search()">搜索</button>
		                                        	</span>
		                                        </div>
	                                    </div>
	                                    
	                                    <button type="button" class="btn btn-sm btn-danger" onclick="addKhgys()">新增客户供应商</button>
                                </div>
                                
                                <!-- 表格数据 -->
                                <div id="dataList">	                                
	                                
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>    	
	    	<#include "/admin/footerFtl.ftl">

        </div>
    </div>

    </div>
    
    <!-- 编辑页面 -->
    <div class="modal inmodal" id="editKhgysModal" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div id="editKhgysContent" class="modal-body">
            
            </div>                  
        </div>											                                    
    </div>
    
    <!-- 显示页面 -->
    <div class="modal inmodal" id="showKhgysModal" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div id="showKhgysContent" class="modal-body" style="width:860px;">
            	
            </div>                  
        </div>												                                    
    </div>
    
    
    <!-- 新增页面 -->
    <div class="modal inmodal" id="addKhgysModal" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div id="addKhgysContent" class="modal-body" style="width:800px;">
            	
            </div>                  
        </div>												                                    
    </div>
    

    <!-- Mainly scripts -->
    <script src="${ctx}/static/pc/js/jquery-2.1.1.min.js"></script>
    <script src="${ctx}/static/pc/js/bootstrap.min.js"></script>
    <script src="${ctx}/static/pc/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="${ctx}/static/pc/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="${ctx}/static/pc/js/hplus.js"></script>
    <script src="${ctx}/static/pc/js/plugins/pace/pace.min.js"></script>
    <script src="${ctx}/static/pc/js/validation.js"></script>
    <script type="text/javascript" src="${ctx}/static/pc/js/flavr/flavr/js/flavr.min.js"></script>
	<script type="text/javascript" src="${ctx}/static/pc/js/flavr/base.js"></script>
	<script type="text/javascript" src="${ctx}/static/pc/js/zTree_v3/js/jquery.ztree.all-3.5.js"></script>
	<script src="${ctx}/static/pc/js/khgys/khgys.js"></script>
</body>

</html>