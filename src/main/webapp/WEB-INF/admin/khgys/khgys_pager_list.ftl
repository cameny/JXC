<div class="row">
	<div class="col-sm-12">
		<table id="example" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%" role="grid" aria-describedby="example_info" style="width: 100%;">
			<thead>
				<tr role="row">
					<th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 248px;">名称</th>
					<th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 248px;">编号</th>
					<th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 248px;">联系人</th>
					<th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 248px;">联系方式</th>
					<th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 248px;">关系</th>
					<th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 248px;">操作</th>
					<!-- 操作   商品名称    型号规格   条形码     标价    总库存    状态操作   -->
        		</tr>      	
        	</thead>
        <tbody>
        <#list khgysList as list>
        	<tr role="row" class="odd">
        		<td>${list.mc}</td>
        		<td>${list.bh}</td>
        		<td>${list.lxr}</td>
        		<td>${list.lxfs}</td>
        		<td>${list.sfgys},${list.sfkh}</td>
        		<td><button class="btn btn-primary btn-xs btn-info" type="button" title="查看详情" onclick=showKhgys('${list.id}')>详情</button>
        		<button class="btn btn-primary btn-xs  btn-warning" type="button" title="查看详情" onclick=editKhgys('${list.id}')>编辑</button>
        		<button class="btn btn-primary btn-xs btn-danger" type="button" title="删除" onclick=delKhgys('${list.id}')>删除</button></td>
        	</tr>
        </#list>
        </tbody>

        </table>
                 当前${pager.pageNow}页，共${pager.totalPageCount}页，总数据量${pager.totalCount}条
     </div>

<#if pager.totalCount??>
	<#import "/admin/pager.ftl" as q>
	<@q.pager pageNo=pager.pageNow pageSize=pager.totalPageCount recordCount=pager.totalPageCount toURL="#"/>
</#if>
</div>
