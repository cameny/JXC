<form method="post" class="form-horizontal" id="addForm">
 <fieldset>
    <legend>客户供应商添加</legend>
        <div class="form-group">
	        <label class="col-sm-2 control-label">名称</label>
	            <div class="col-sm-4">
	                <input type="text" class="form-control" id="mc" name="mc" >
	            </div>
	            <label class="col-sm-2 control-label">编号</label>
	            <div class="col-sm-4">
	                <input type="text" class="form-control" id="bh" name="bh">
	            </div>
	    </div>
	    <div class="form-group">
	        <label class="col-sm-2 control-label">联系人</label>
	            <div class="col-sm-4">
	                <input type="text" class="form-control" id="lxr" name="lxr" >
	            </div>
            <label class="col-sm-2 control-label">联系方式</label>
	            <div class="col-sm-4">
	                <input type="text" class="form-control" id="lxfs" name="lxfs" >
	            </div>
	    </div>
	    <div class="form-group">
            <label class="col-sm-2 control-label">是否客户</label>
	            <div class="col-sm-4">
	                <input type="text" class="form-control" id="sfkh" name="sfkh" >
	            </div>
			<label class="col-sm-2 control-label">是否供应商</label>
	            <div class="col-sm-4">
	                <input type="text" class="form-control" id="sfgys" name="sfgys" >
	            </div>
	    </div>
	    <div class="form-group">
	        <label class="col-sm-2 control-label">地址</label>
	            <div class="col-sm-4">
	                <input type="text" class="form-control" id="dz" name="dz" >
	            </div>
	    </div>
	    <div class="form-group">
			<div class="col-sm-4 col-sm-offset-2">
                <button class="btn btn-primary" type="button" onclick="saveKhgys()">保存</button>
                <button class="btn btn-white" type="button" onclick="cancelSaveShop()">取消</button>
        </div>
	</div>
 </fieldset>
</form>
