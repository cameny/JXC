<nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="side-menu">
                    <#include "/admin/headerFtl.ftl">
                   <!-- <jsp:include page="header.jsp" flush="true"></jsp:include> -->
                    
                    <li>
                        <a href="javascript:void(0)"><i class="fa fa fa-volume-up"></i>
                        	<span class="nav-label">销售管理</span>
                        	<span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level">
                            <li>
                            	<a href="${ctx}/xsck/list">销售出库</a>
                            </li>
                            <li>
                            	<a href="${ctx}/static/pc/admin/article/list">销售退货</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:void(0)"><i class="fa fa fa-qrcode"></i>
                        	<span class="nav-label">采购管理</span>
                        	<span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level">
                            <li>
                            	<a href="${ctx}/static/pc/admin/tag/list">采购进货</a>
                            </li>
                            <li>
                            	<a href="${ctx}/static/pc/admin/tag/list">采购退货</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="${ctx}/khgys/list"><i class="fa fa fa-qrcode"></i>
                        	<span class="nav-label">客户供应商</span>
                        </a>
                    </li>
                    <li>
                        <a href="${ctx}/sp/list"><i class="fa fa fa-qrcode"></i>
                        	<span class="nav-label">商品管理</span>
                        </a>
                    </li>
                    <li>
                        <a href="${ctx}/md/list"><i class="fa fa fa-qrcode"></i>
                        	<span class="nav-label">门店管理</span>
                        </a>
                    </li>
                    <li>
                        <a href="${ctx}/static/pc/admin/partner/list"><i class="fa fa fa-qrcode"></i>
                        	<span class="nav-label">报表中心</span>
                        </a>
                        <!--
                        <ul class="nav nav-second-level">

                        </ul> -->
                    </li>
                </ul>

            </div>
        </nav>