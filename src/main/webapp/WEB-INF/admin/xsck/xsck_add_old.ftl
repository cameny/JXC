<#assign ctx=request.contextPath>
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"><meta name="renderer" content="webkit">
    <link href="${ctx}/static/pc/css/bootstrap.min.css"  rel="stylesheet">
    <link href="${ctx}/static/pc/font-awesome/css/font-awesome.css"  rel="stylesheet">
    <!-- Data Tables -->
    <link href="${ctx}/static/pc/css/plugins/dataTables/dataTables.bootstrap.css"  rel="stylesheet">
    <link href="${ctx}/static/pc/css/animate.css" rel="stylesheet">
    <link href="${ctx}/static/pc/css/style.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="${ctx}/static/pc/js/flavr/flavr/css/animate.css" />
    <link rel="stylesheet" type="text/css" href="${ctx}/static/pc/js/flavr/flavr/css/flavr.css" />
    <link rel="stylesheet" href="${ctx}/static/pc/js/zTree_v3/css/zTreeStyle/zTreeStyle.css" type="text/css">
    <link rel="stylesheet" href="${ctx}/static/pc/bootstrap-table/bootstrap-table.css"  />
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="${ctx}/static/pc/plugins/html5shiv/html5shiv.min.js"></script>
    <![endif]-->

    <!-- grid.simple.min.css, grid.simple.min.js -->
    <link rel="stylesheet" href="${ctx}/static/pc/builds/merged/bsgrid.all.min.css"/>
    <link rel="stylesheet" href="${ctx}/static/pc/builds/css/skins/grid_bootstrap.min.css"/>

    <script type="text/javascript" src="${ctx}/static/pc/plugins/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="${ctx}/static/pc/plugins/bootstrap/2.3.2/js/bootstrap.min.js"></script>

    <script type="text/javascript" src="${ctx}/static/pc/builds/js/lang/grid.zh-CN.min.js"></script>
    <script type="text/javascript" src="${ctx}/static/pc/builds/merged/bsgrid.all.min.js"></script>
    
    
	<style>
		.btn-circle {
		  width: 20px;
		  height: 20px;
		  border-radius: 10px;
		}
		.fa{		
		  position: relative;
		  top: -3px;
		}
	</style>
	<style>	
	fieldset{border:1px solid #1ab394; padding:5px;}
	legend{display: block;
	  width: auto;
	  padding: 0;
	  margin-bottom: 0;
	  font-size: 14px;
	  color: #333;
	  border: none;
	}
	
	</style>
</head>

<body>
    <div id="wrapper">
    	<#include "/admin/menuFtl.ftl">
    	<div id="page-wrapper" class="gray-bg dashbard-1">
	    	<#include "/admin/leftTopFtl.ftl">
    	

<form id="form1" method="post" class="form-horizontal">
 <fieldset>
    <legend>销售订单详情</legend>
		<div class="form-group">
                <input type="hidden" class="form-control" id="id" name="id" maxlength="20" value="${xsckFather.id}" >            
        </div>
        <div class="form-group">
	        <label class="col-sm-2 control-label">客户名称</label>
	            <div class="col-sm-4">
	                <input type="text" class="form-control" id="khmc" name="khmc" maxlength="20" value="${xsckFather.khmc}" >
	            </div>
	            <label class="col-sm-2 control-label">单号</label>
	            <div class="col-sm-4">
	                <input type="text" class="form-control"  maxlength="20" readOnly="true" value="系统会自动生成单号">
	            </div>
	    </div>
	    <div class="form-group">
	        <label class="col-sm-2 control-label">开单时间</label>
	            <div class="col-sm-4">
	                <input type="text" class="form-control" id="kdsj" name="kdsj" maxlength="20" value="${time?datetime}" readonly="true">
	            </div>
             <label class="col-sm-2 control-label">联系方式</label>
            <div class="col-sm-4">
                <input type="text" class="form-control" id="lxfs" name="lxfs" maxlength="20" value="${xsckFather.lxfs}" >
            </div>
	    </div>
	    <div class="form-group">
	        <label class="col-sm-2 control-label">地址</label>
	            <div class="col-sm-4">
	                <input type="text" class="form-control" id="dz" name="dz" maxlength="20" value="${xsckFather.dz}" >
	            </div>
	        <label class="col-sm-2 control-label">门店</label>
	            <div class="col-sm-4">
	                <input type="text" class="form-control" id="mdid" name="mdid" maxlength="20" value="${xsckFather.mdid}" >
	            </div>
	    </div>
	    <div class="form-group">
	        <label class="col-sm-2 control-label">开单人</label>
	            <div class="col-sm-4">
	                <input type="text" class="form-control" id="khr" name="khr" maxlength="20" value="${user.username}" readOnly="true" >
	            </div>
	        <label class="col-sm-2 control-label">出库员</label>
	            <div class="col-sm-4">
	                <input type="text" class="form-control" id="cky" name="cky" maxlength="20" value="${user.username}" >
	            </div>
	    </div>
</form>
    
	    <!-- 子表-->
	    <div class="row">
			<div class="col-sm-12">
				<div class="container">
					<form id="form2" name="form2" method="post" class="form-horizontal">	
						
					<table id="zb"></table>
					</form>
					<button class="btn btn-primary" type="button" id="btn_add" onclick="#">添加一行</button>
					<button class="btn btn-warning" type="button" id="btn_del" onclick="#">删除一行</button>
				</div>
				
		     </div>
		</div>
		<form id="form3" method="post" class="form-horizontal">	
	    <div class="form-group">
	        <label class="col-sm-2 control-label">人民币大写</label>
	            <div class="col-sm-4">
	                <input type="text" class="form-control" id="rmbdx" name="rmbdx" maxlength="20">
	            </div>
	        <label class="col-sm-2 control-label">人民币小写</label>
	            <div class="col-sm-4">
	                <input type="text" class="form-control" id="rmbxx" name="rmbxx" maxlength="20" >
	            </div>
	    </div>
	    <div class="form-group">
	        <label class="col-sm-2 control-label">备注</label>
	            <div class="col-sm-4">
	                <input type="text" class="form-control" id="bz" name="bz" maxlength="20" >
	            </div>
	    </div>
	    <div class="form-group">
                                        <div class="col-sm-4 col-sm-offset-2">
                                            <button class="btn btn-primary" type="button" onclick="saveShop()">保存内容</button>
                                            <button class="btn btn-white" type="button" onclick="cancelSaveShop()">取消</button>
                                        </div>
                                    </div>
 </fieldset>
</form> 
	<input type="button" onclick="getData()" value="测试" /> 
	<script>
		
	</script>

	<script type="text/javascript" src="${ctx}/static/pc/bootstrap-table/bootstrap-table.js"></script>
	<script type="text/javascript" src="${ctx}/static/pc/bootstrap-table/mindmup-editabletable.js"></script>
	<script type="text/javascript" src="${ctx}/static/pc/bootstrap-table/edit.js"></script>