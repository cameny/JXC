<#assign ctx=request.contextPath>
<style>

fieldset{border:1px solid #1ab394; padding:5px;}
legend{display: block;
  width: auto;
  padding: 0;
  margin-bottom: 0;
  font-size: 14px;
  color: #333;
  border: none;
}

</style>
<form id="" method="post" class="form-horizontal">
 <fieldset>
    <legend>销售订单详情</legend>
		<div class="form-group">
                <input type="hidden" class="form-control" id="id" name="id" maxlength="20" value="${xsckFather.id}" readOnly="true">            
        </div>
        <div class="form-group">
	        <label class="col-sm-2 control-label">客户名称</label>
	            <div class="col-sm-4">
	                <input type="text" class="form-control" id="khmc" name="khmc" maxlength="20" value="${xsckFather.khmc}" readOnly="true">
	            </div>
	            <label class="col-sm-2 control-label">单号</label>
	            <div class="col-sm-4">
	                <input type="text" class="form-control" id="dh" name="dh" maxlength="20" value="${xsckFather.dh}" readOnly="true">
	            </div>
	    </div>
	    <div class="form-group">
	        <label class="col-sm-2 control-label">开单时间</label>
	            <div class="col-sm-4">
	                <input type="text" class="form-control" id="kdsj" name="kdsj" maxlength="20" value="${xsckFather.kdsj?datetime}" readOnly="true">
	            </div>
             <label class="col-sm-2 control-label">联系方式</label>
            <div class="col-sm-4">
                <input type="text" class="form-control" id="lxfs" name="lxfs" maxlength="20" value="${xsckFather.lxfs}" readOnly="true">
            </div>
	    </div>
	    <div class="form-group">
	        <label class="col-sm-2 control-label">地址</label>
	            <div class="col-sm-4">
	                <input type="text" class="form-control" id="dz" name="dz" maxlength="20" value="${xsckFather.dz}" readOnly="true">
	            </div>
	        <label class="col-sm-2 control-label">门店</label>
	            <div class="col-sm-4">
	                <input type="text" class="form-control" id="mdid" name="mdid" maxlength="20" value="${xsckFather.mdid}" readOnly="true">
	            </div>
	    </div>
	    <div class="form-group">
	        <label class="col-sm-2 control-label">开单人</label>
	            <div class="col-sm-4">
	                <input type="text" class="form-control" id="khr" name="khr" maxlength="20" value="${xsckFather.khr}" readOnly="true">
	            </div>
	        <label class="col-sm-2 control-label">出库员</label>
	            <div class="col-sm-4">
	                <input type="text" class="form-control" id="cky" name="cky" maxlength="20" value="${xsckFather.cky}" readOnly="true">
	            </div>
	    </div>
	    	    
	    <!-- 子表-->
	    <div class="row">
			<div class="col-sm-12">
				<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%" role="grid" aria-describedby="example_info" style="width: 100%;">
					<thead>
						<tr role="row">
							<th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 248px;">编号</th>
							<th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 248px;">商品名称</th>
							<th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 248px;">条形码</th>
							<th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 248px;">规格</th>
							<th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 248px;">单位</th>
							<th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 248px;">单价</th>
							<th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 248px;">数量</th>
							<th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 248px;">小计</th>
							<th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 248px;">备注</th>
							<!-- "交易方","总金额","开单日期","开单人","库房","备注" -->
		        		</tr>      	
		        	</thead>
		        <tbody>
		        <#list xsckSon as list>
		        	<tr role="row" class="odd">
		        		<td>${list.bh}</td>
		        		<td>${list.spmc}</td>
		        		<td>${list.txm}</td>
		        		<td>${list.gg}</td>
		        		<td>${list.dw}</td>
		        		<td>${list.dj}</td>
		        		<td>${list.sl}</td>
		        		<td><strong><em>¥</em></strong>${list.xj}</td>
		        		<td>${list.bz}</td>
		        	</tr>
		        </#list>
		        </tbody>
		        </table>
		     </div>
		</div>
	    <div class="form-group">
	        <label class="col-sm-2 control-label">人民币大写</label>
	            <div class="col-sm-4">
	                <input type="text" class="form-control" id="rmbdx" name="rmbdx" maxlength="20" value="${xsckFather.rmbdx}" readOnly="true">
	            </div>
	        <label class="col-sm-2 control-label">人民币小写</label>
	            <div class="col-sm-4">
	                <input type="text" class="form-control" id="rmbxx" name="rmbxx" maxlength="20" value="¥${xsckFather.rmbxx}" readOnly="true">
	            </div>
	    </div>
	    <div class="form-group">
	        <label class="col-sm-2 control-label">备注</label>
	            <div class="col-sm-4">
	                <input type="text" class="form-control" id="bz" name="bz" maxlength="20" value="${xsckFather.bz}" readOnly="true">
	            </div>
	    </div>
	    
 </fieldset>
</form> 

