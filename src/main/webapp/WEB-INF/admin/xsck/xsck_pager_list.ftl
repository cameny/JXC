<div class="row">
	<div class="col-sm-12">
		<table id="example" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%" role="grid" aria-describedby="example_info" style="width: 100%;">
			<thead>
				<tr role="row">
					<th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 248px;">单号</th>
					<th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 248px;">交易方</th>
					<th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 248px;">总金额</th>
					<th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 248px;">开单日期</th>
					<th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 248px;">开单人</th>
					<th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 248px;">库房</th>
					<th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 248px;">备注</th>
					<th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 248px;">操作</th>
					<!-- "交易方","总金额","开单日期","开单人","库房","备注" -->
        		</tr>      	
        	</thead>
        <tbody>
        <#list xsckFatherList as list>
        	<tr role="row" class="odd">
        		<td>${list.dh}</td>
        		<td>${list.khmc}</td>
        		<td><strong><em>¥</em></strong>${list.rmbxx}</td>
        		<td>${list.kdsj?datetime}</td>
        		<td>${list.khr}</td>
        		<td>${list.mdid}</td>
        		<td>${list.bz}</td>        		
        		<td><button class="btn btn-primary btn-xs btn-info" type="button" title="查看详情" onclick=showXsck('${list.id}')>	查看详情</button>
        		<button class="btn btn-primary btn-xs btn-warning" type="button" title="编辑" onclick=editXsck('${list.id}')>编辑</button>
        		<button class="btn btn-primary btn-xs btn-danger" type="button" title="删除" onclick=delXsck('${list.id}')>删除</button></td>
        	</tr>
        </#list>
        </tbody>

        </table>
                 当前${pager.pageNow}页，共${pager.totalPageCount}页，总数据量${pager.totalCount}条
     </div>

<#if pager.totalCount??>
	<#import "/admin/pager.ftl" as q>
	<@q.pager pageNo=pager.pageNow pageSize=pager.totalPageCount recordCount=pager.totalPageCount toURL="#"/>
</#if>
    
</div>
