<#assign ctx=request.contextPath>
<!DOCTYPE html>
	<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"><meta name="renderer" content="webkit">
    <title>后台  - 登录</title>
    <link href="${ctx}/static/pc/css/bootstrap.min.css"  rel="stylesheet">
    <link href="${ctx}/static/pc/font-awesome/css/font-awesome.css"  rel="stylesheet">
    <link href="${ctx}/static/pc/css/animate.css"  rel="stylesheet">
    <link href="${ctx}/static/pc/css/style.css"  rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="${ctx}/static/pc/js/flavr/flavr/css/animate.css" />
    <link rel="stylesheet" type="text/css" href="${ctx}/static/pc/js/flavr/flavr/css/flavr.css" />
    <link href="${ctx}/static/pc/css/plugins/toastr/toastr.min.css" rel="stylesheet">
	<style>
	.loginscreen.middle-box{padding-top:260px;}
	</style>
</head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen  animated fadeInDown">
        <div>
            <div>
                <h1 class="logo-name"></h1>
            </div>

            <form id="form" class="m-t" role="form" method="post" action="${ctx}/admin/login">
                <div class="form-group">
                    <input type="text" name="username" id="name" class="form-control" placeholder="用户名">
                </div>
                <div class="form-group">
                    <input type="password" name="password" id="password" class="form-control" placeholder="密码">
                </div>
                <#if msg== "LOGIN_FAIL">
                	登录失败：用户名或密码错误
                <#else>
                	
				</#if>
                <button type="button" class="btn btn-primary block full-width m-b" id="sub" >登 录</button>

            </form>
        </div>
    </div>
	
    <script src="${ctx}/static/pc/js/jquery-2.1.1.min.js" ></script>
    <script src="${ctx}/static/pc/js/bootstrap.min.js" ></script>
    <script type="text/javascript" src="${ctx}/static/pc/js/flavr/flavr/js/flavr.min.js"></script>
	<script type="text/javascript" src="${ctx}/static/pc/js/flavr/base.js"></script>
	<script src="${ctx}/static/pc/js/validation.js"></script>
	<script src="${ctx}/static/pc/js/plugins/toastr/toastr.min.js"></script>
	<script type="text/javascript">
		//登录
		$('#sub').click(function(){
			var name = $("#name").val();
			var password = $("#password").val();
			if(isEmpty(name)){
				toastr.error("账户不能为空");
				return false;
			};
			
			if(isEmpty(password)){
				toastr.error("密码不能为空");
				return false;
			};
			$("form").submit();
		});
		
	</script>
</body>
</html>
