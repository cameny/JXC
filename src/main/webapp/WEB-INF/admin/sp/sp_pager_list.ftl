<div class="row">
	<div class="col-sm-12">
		<table id="example" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%" role="grid" aria-describedby="example_info" style="width: 100%;">
			<thead>
				<tr role="row">
					<th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 248px;">操作</th>
					<th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 248px;">商品名称</th>
					<th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 248px;">型号</th>
					<th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 248px;">规格</th>
					<th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 248px;">条形码</th>
					<th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 248px;">标价</th>
					<th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 248px;">总库存</th>
					<th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 248px;">状态操作</th>
					<!-- 操作   商品名称    型号规格   条形码     标价    总库存    状态操作   -->
        		</tr>      	
        	</thead>
        <tbody>
        <#list spList as list>
        	<tr role="row" class="odd">
        		<td><button class="btn btn-primary btn-xs btn-info" type="button" title="查看详情" onclick=showSp('${list.id}')>详情</button>
        		<button class="btn btn-primary btn-xs  btn-warning" type="button" title="查看详情" onclick=editSp('${list.id}')>编辑</button>
        		<button class="btn btn-primary btn-xs btn-danger" type="button" title="删除" onclick=delSp('${list.id}')>删除</button></td>
        		<td>${list.spmc}</td>
        		<td>${list.xh}</td>
        		<td>${list.gg}</td>
        		<td>${list.txm}</td>
        		<td><strong><em>¥</em></strong>${list.bj}</td>
        		<td>库存</td>        		  		
        		<td><button class="btn btn-primary btn-xs btn-info" type="button" title="查看详情" onclick=showXsck('${list.id}')>禁用</button></td>
        	</tr>
        </#list>
        </tbody>

        </table>
                 当前${pager.pageNow}页，共${pager.totalPageCount}页，总数据量${pager.totalCount}条
     </div>

<#if pager.totalCount??>
	<#import "/admin/pager.ftl" as q>
	<@q.pager pageNo=pager.pageNow pageSize=pager.totalPageCount recordCount=pager.totalPageCount toURL="#"/>
</#if>
</div>
