<form method="post" class="form-horizontal" id="addForm">
 <fieldset>
    <legend>商品详情</legend>
        <div class="form-group">
	        <label class="col-sm-2 control-label">商品名称</label>
	            <div class="col-sm-4">
	                <input type="text" class="form-control" id="spmc" name="spmc" value="${sp.spmc}" readonly="true">
	            </div>
	            <label class="col-sm-2 control-label">型号</label>
	            <div class="col-sm-4">
	                <input type="text" class="form-control" id="xh" name="xh" value="${sp.xh}" readonly="true">
	            </div>
	    </div>
	    <div class="form-group">
	        <label class="col-sm-2 control-label">规格</label>
	            <div class="col-sm-4">
	                <input type="text" class="form-control" id="gg" name="gg" value="${sp.gg}" readonly="true">
	            </div>
            <label class="col-sm-2 control-label">单位</label>
	            <div class="col-sm-4">
	                <input type="text" class="form-control" id="dw" name="dw" value="${sp.dw}" readonly="true">
	            </div>
	    </div>
	    <div class="form-group">
	        <label class="col-sm-2 control-label">商品号</label>
	            <div class="col-sm-4">
	                <input type="text" class="form-control" id="sph" name="sph" value="${sp.sph}" readonly="true">
	            </div>
            <label class="col-sm-2 control-label">条形码</label>
	            <div class="col-sm-4">
	                <input type="text" class="form-control" id="txm" name="txm" value="${sp.txm}" readonly="true">
	            </div>
	    </div>
	    <div class="form-group">
	        <label class="col-sm-2 control-label">初始成本价</label>
	            <div class="col-sm-4">
	                <input type="text" class="form-control" id="cscbj" name="cscbj" value="${sp.cscbj}" readonly="true">
	            </div>
            <label class="col-sm-2 control-label">标价</label>
	            <div class="col-sm-4">
	                <input type="text" class="form-control" id="bj" name="bj" value="${sp.bj}" readonly="true">
	            </div>
	    </div>
	    <div class="form-group">
	        <label class="col-sm-2 control-label">初始库存</label>
	            <div class="col-sm-4">
	                <input type="text" class="form-control" id="cskc" name="cskc" value="${sp.cskc}" readonly="true">
	            </div>
	    </div>
	    <div class="form-group">
	        <label class="col-sm-2 control-label">库存最大值</label>
	            <div class="col-sm-4">
	                <input type="text" class="form-control" id="kczdz" name="kczdz" value="${sp.kczdz}" readonly="true">
	            </div>
            <label class="col-sm-2 control-label">库存最小值</label>
	            <div class="col-sm-4">
	                <input type="text" class="form-control" id="kczxz" name="kczxz" value="${sp.kczxz}" readonly="true">
	            </div>
	    </div>
	    <div class="form-group">
	        <label class="col-sm-2 control-label">商品备注</label>
	            <div class="col-sm-4">
	                <input type="text" class="form-control" id="spbz" name="spbz" value="${sp.spbz}" readonly="true">
	            </div>
	    </div>
	    <div class="form-group">
			<div class="col-sm-4 col-sm-offset-2">
                <button class="btn btn-white" type="button" onclick="cancelSaveShop()">返回</button>
        </div>
	</div>
 </fieldset>
</form>
