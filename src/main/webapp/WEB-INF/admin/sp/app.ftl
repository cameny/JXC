<#assign ctx=request.contextPath>
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"><meta name="renderer" content="webkit">
    <link href="${ctx}/static/pc/css/bootstrap.min.css"  rel="stylesheet">
    <link href="${ctx}/static/pc/font-awesome/css/font-awesome.css"  rel="stylesheet">
    <!-- Data Tables -->
    <link href="${ctx}/static/pc/css/plugins/dataTables/dataTables.bootstrap.css"  rel="stylesheet">
    <link href="${ctx}/static/pc/css/animate.css" rel="stylesheet">
    <link href="${ctx}/static/pc/css/style.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="${ctx}/static/pc/js/flavr/flavr/css/animate.css" />
    <link rel="stylesheet" type="text/css" href="${ctx}/static/pc/js/flavr/flavr/css/flavr.css" />
    <link rel="stylesheet" href="${ctx}/static/pc//js/zTree_v3/css/zTreeStyle/zTreeStyle.css" type="text/css">
    
	<style>
		.btn-circle {
		  width: 20px;
		  height: 20px;
		  border-radius: 10px;
		}
		.fa{		
		  position: relative;
		  top: -3px;
		}
	</style>
</head>

<body>
	<div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
						<div class="row">
                            <div class="col-sm-3">
                                    <div class="input-group">
                                    	<form action="/app" method="GET">
                                        <input type="text" id="spmc" name="spmc" placeholder="请输入关键字" style="width: auto;" class="input-sm form-control"><button type="submit" class="btn btn-sm btn-primary">搜索</button>
                                        <span class="input-group-btn">
                                    	</span>
                                    	</form>
                                    </div>
                            </div>
                        </div>
                        <!-- 表格数据 -->
                        <table data-toggle="table">
                        <#list sp as list>
                    		<tr>
                    			<td>名称：</td><td>${list.spmc}</td>
                    		</tr>
                    		<tr>
                    			<td>型号：</td><td>${list.xh}</td>
                    		</tr>
                    		<tr>
                    			<td>规格：</td><td>${list.gg}</td>
                    		</tr>
                    		<tr>
                    			<td>名称：</td><td>${list.spmc}</td>
                    		</tr>
                    		<tr>
                    			<td>单价:</td><td><strong><em>¥</em></strong>${list.bj}</td>
                    		</tr>
                    		<tr><td></td></tr>
                        </#list>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    

    <!-- Mainly scripts -->
    <script src="${ctx}/static/pc/js/jquery-2.1.1.min.js"></script>
    <script src="${ctx}/static/pc/js/bootstrap.min.js"></script>
    <script src="${ctx}/static/pc/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="${ctx}/static/pc/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="${ctx}/static/pc/js/hplus.js"></script>
    <script src="${ctx}/static/pc/js/plugins/pace/pace.min.js"></script>
    <script src="${ctx}/static/pc/js/validation.js"></script>
    <script type="text/javascript" src="${ctx}/static/pc/js/flavr/flavr/js/flavr.min.js"></script>
	<script type="text/javascript" src="${ctx}/static/pc/js/flavr/base.js"></script>
	<script src="${ctx}/static/pc/js/bootstrap-table.min.js"></script>
	<script src="${ctx}/static/pc/js/bootstrap-table-zh-CN.js"></script>
	<script>
		$(function(){
			// 加载商品列表
			loadSpList();
		})
		
		function loadSpList(){
			$.ajax({
	        url : '../app',
	        data : $("#spmc").val(),
	        type: 'POST',
	        success  : function(data) {
	        	alert(data);
	        	$("#dataList").html(data);
			}
	    });
		}
	<script>
</body>

</html>