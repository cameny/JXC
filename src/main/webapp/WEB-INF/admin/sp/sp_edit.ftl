<form method="post" class="form-horizontal" id="editForm">
 <fieldset>
    <legend>商品详情</legend>
    	<div class="form-group">
	        <label class="col-sm-2 control-label">商品ID</label>
	            <div class="col-sm-4">
	                <input type="text" class="form-control" id="spmc" name="spmc" value="${sp.id}" >
	            </div>
	    </div>
        <div class="form-group">
	        <label class="col-sm-2 control-label">商品名称</label>
	            <div class="col-sm-4">
	                <input type="text" class="form-control" id="spmc" name="spmc" value="${sp.spmc}" >
	            </div>
	            <label class="col-sm-2 control-label">型号</label>
	            <div class="col-sm-4">
	                <input type="text" class="form-control" id="xh" name="xh" value="${sp.xh}" >
	            </div>
	    </div>
	    <div class="form-group">
	        <label class="col-sm-2 control-label">规格</label>
	            <div class="col-sm-4">
	                <input type="text" class="form-control" id="gg" name="gg" value="${sp.gg}" >
	            </div>
            <label class="col-sm-2 control-label">单位</label>
	            <div class="col-sm-4">
	                <input type="text" class="form-control" id="dw" name="dw" value="${sp.dw}" >
	            </div>
	    </div>
	    <div class="form-group">
	        <label class="col-sm-2 control-label">商品号</label>
	            <div class="col-sm-4">
	                <input type="text" class="form-control" id="sph" name="sph" value="${sp.sph}" >
	            </div>
            <label class="col-sm-2 control-label">条形码</label>
	            <div class="col-sm-4">
	                <input type="text" class="form-control" id="txm" name="txm" value="${sp.txm}" >
	            </div>
	    </div>
	    <div class="form-group">
	        <label class="col-sm-2 control-label">初始成本价</label>
	            <div class="col-sm-4">
	                <input type="text" class="form-control" id="cscbj" name="cscbj" value="${sp.cscbj}" >
	            </div>
            <label class="col-sm-2 control-label">标价</label>
	            <div class="col-sm-4">
	                <input type="text" class="form-control" id="bj" name="bj" value="${sp.bj}" >
	            </div>
	    </div>
	    <div class="form-group">
	        <label class="col-sm-2 control-label">初始库存</label>
	            <div class="col-sm-4">
	                <input type="text" class="form-control" id="cskc" name="cskc" value="${sp.cskc}" >
	            </div>
	    </div>
	    <div class="form-group">
	        <label class="col-sm-2 control-label">库存最大值</label>
	            <div class="col-sm-4">
	                <input type="text" class="form-control" id="kczdz" name="kczdz" value="${sp.kczdz}" >
	            </div>
            <label class="col-sm-2 control-label">库存最小值</label>
	            <div class="col-sm-4">
	                <input type="text" class="form-control" id="kczxz" name="kczxz" value="${sp.kczxz}" >
	            </div>
	    </div>
	    <div class="form-group">
	        <label class="col-sm-2 control-label">商品备注</label>
	            <div class="col-sm-4">
	                <input type="text" class="form-control" id="spbz" name="spbz" value="${sp.spbz}" >
	            </div>
	    </div>
	    <div class="form-group">
			<div class="col-sm-4 col-sm-offset-2">
				<button class="btn btn-primary" type="button" onclick="saveEditSp()">保存</button>
                <button class="btn btn-white" type="button" onclick="cancelSaveShop()">返回</button>
        </div>
	</div>
 </fieldset>
</form>
