<#assign ctx=request.contextPath>
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"><meta name="renderer" content="webkit">
    <link href="${ctx}/static/pc/css/bootstrap.min.css"  rel="stylesheet">
    <link href="${ctx}/static/pc/font-awesome/css/font-awesome.css"  rel="stylesheet">
    <!-- Data Tables -->
    <link href="${ctx}/static/pc/css/plugins/dataTables/dataTables.bootstrap.css"  rel="stylesheet">
    <link href="${ctx}/static/pc/css/animate.css" rel="stylesheet">
    <link href="${ctx}/static/pc/css/style.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="${ctx}/static/pc/js/flavr/flavr/css/animate.css" />
    <link rel="stylesheet" type="text/css" href="${ctx}/static/pc/js/flavr/flavr/css/flavr.css" />
    <link rel="stylesheet" href="${ctx}/static/pc/js/zTree_v3/css/zTreeStyle/zTreeStyle.css" type="text/css">
    
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="${ctx}/static/pc/plugins/html5shiv/html5shiv.min.js"></script>
    <![endif]-->

    <!-- grid.simple.min.css, grid.simple.min.js -->
    <link rel="stylesheet" href="${ctx}/static/pc/builds/merged/bsgrid.all.min.css"/>
    <link rel="stylesheet" href="${ctx}/static/pc/builds/css/skins/grid_bootstrap.min.css"/>

    <script type="text/javascript" src="${ctx}/static/pc/plugins/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="${ctx}/static/pc/plugins/bootstrap/2.3.2/js/bootstrap.min.js"></script>

    <script type="text/javascript" src="${ctx}/static/pc/builds/js/lang/grid.zh-CN.min.js"></script>
    <script type="text/javascript" src="${ctx}/static/pc/builds/merged/bsgrid.all.min.js"></script>
    <script src="${ctx}/static/pc/js/md/md.js"></script>
	<style>
		.btn-circle {
		  width: 20px;
		  height: 20px;
		  border-radius: 10px;
		}
		.fa{		
		  position: relative;
		  top: -3px;
		}
	</style>
	<style>	
	fieldset{border:1px solid #1ab394; padding:5px;}
	legend{display: block;
	  width: auto;
	  padding: 0;
	  margin-bottom: 0;
	  font-size: 14px;
	  color: #333;
	  border: none;
	}
	
	</style>
</head>

<body>
    <div id="wrapper">
    	<#include "/admin/menuFtl.ftl">
    	<div id="page-wrapper" class="gray-bg dashbard-1">
	    	<#include "/admin/leftTopFtl.ftl">
    	

<form method="post" class="form-horizontal" id="addForm">
 <fieldset>
    <legend>门店添加</legend>
        <div class="form-group">
	        <label class="col-sm-2 control-label">门店名称</label>
	            <div class="col-sm-4">
	                <input type="text" class="form-control" id="mdmc" name="mdmc" maxlength="20">
	            </div>
	            <label class="col-sm-2 control-label">管理员</label>
	            <div class="col-sm-4">
	                <input type="text" class="form-control" id="gly" name="gly" value="${user.username}" readOnly="true"">
	            </div>
	    </div>
	    <div class="form-group">
	        <label class="col-sm-2 control-label">电话</label>
	            <div class="col-sm-4">
	                <input type="text" class="form-control" id="dh" name="dh" maxlength="20">
	            </div>
	    </div>
	    <div class="form-group">
			<div class="col-sm-4 col-sm-offset-2">
                <button class="btn btn-primary" type="button" onclick="saveMd()">保存</button>
                <button class="btn btn-white" type="button" onclick="cancelSaveShop()">取消</button>
        </div>
	</div>
 </fieldset>
</form>


<!-- Mainly scripts -->
    <script src="${ctx}/static/pc/js/jquery-2.1.1.min.js"></script>
    <script src="${ctx}/static/pc/js/bootstrap.min.js"></script>
    <script src="${ctx}/static/pc/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="${ctx}/static/pc/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="${ctx}/static/pc/js/hplus.js"></script>
    <script src="${ctx}/static/pc/js/plugins/pace/pace.min.js"></script>
    <script src="${ctx}/static/pc/js/validation.js"></script>
    <script type="text/javascript" src="${ctx}/static/pc/js/flavr/flavr/js/flavr.min.js"></script>
	<script type="text/javascript" src="${ctx}/static/pc/js/flavr/base.js"></script>
	<script type="text/javascript" src="${ctx}/static/pc/js/zTree_v3/js/jquery.ztree.all-3.5.js"></script>

