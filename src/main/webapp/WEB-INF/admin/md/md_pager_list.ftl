<div class="row">
	<div class="col-sm-12">
		<table id="example" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%" role="grid" aria-describedby="example_info" style="width: 100%;">
			<thead>
				<tr role="row">
					
					<th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 248px;">门店名称</th>
					<th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 248px;">管理员</th>
					<th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 248px;">电话</th>
					<th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 248px;">操作</th>

        		</tr>      	
        	</thead>
        <tbody>
        <#list md as list>
        	<tr role="row" class="odd">
        		<td>${list.mdmc}</td>
        		<td>${list.gly}</td>
        		<td>${list.dh}</td>
        		<td>
        		<button class="btn btn-primary btn-xs btn-warning" type="button" title="编辑" onclick=editMd('${list.id}')>编辑</button>
        		<button class="btn btn-primary btn-xs btn-danger" type="button" title="删除" onclick=delMd('${list.id}')>删除</button></td>
        	</tr>
        </#list>
        </tbody>
        </table>
     </div>
    
</div>
