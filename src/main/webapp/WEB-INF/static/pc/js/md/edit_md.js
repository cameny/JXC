function getRootPath() {
	//获取当前网址，如： http://localhost:8080/GameFngine/share/meun.jsp
	var curWwwPath = window.document.location.href;
	//获取主机地址之后的目录，如： GameFngine/meun.jsp
	var pathName = window.document.location.pathname;
	var pos = curWwwPath.indexOf(pathName);
	//获取主机地址，如： http://localhost:8080
	var localhostPaht = curWwwPath.substring(0, pos);
	//获取带"/"的项目名，如：/GameFngine
	var projectName = pathName.substring(0,
			pathName.substr(1).indexOf('/') + 1);
	return (localhostPaht + projectName + "/");
}

// 保存门店
function editMd(){
    // 保存门店
    $.ajax({
        type : "POST",
        url :  getRootPath()+'md/edit',
        data : $("#editForm").serialize(),
        success  : function(data) {
        	if(data.resultCode != 'DEAL_SUCCESS'){
        		autoCloseAlert(data.errorInfo,1000);
        		window.location.href = getRootPath()+ "md/list";
			}else{
				// 调到列表页
				window.location.href = getRootPath()+ "md/list";
			}
		}
    });
}

function cancelEditMd(){
	window.location.href = getRootPath()+ "md/list";
}