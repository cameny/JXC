$(function(){
	// 加载门店列表
	loadMdList();
})

// 加载门店列表
function loadMdList(){	
	// 查询列表
	$.ajax({
        url : '../md/load',
        type: 'POST',
        success  : function(data) {
        	$("#dataList").html(data);
		}
    });
	
}

// 新增门店  跳转新页
function addMd(){
	window.location.href = "../md/insert";
}

// 删除门店
function delMd(id){
	$.ajax({
        url : '../md/del/'+id,
        type : 'DELETE',
        success  : function(data) {
        	if(data.resultCode == 'DEAL_SUCCESS'){
        		autoCloseAlert(data.errorInfo,1000);
        	}else{
        		autoCloseAlert(data.errorInfo,1000);
        	}
        	loadMdList();
		}
    });	
}
//保存门店
function saveMd(){
	$.ajax({
		url : '../md/add',
		data : $("#addForm").serialize(),
		type : 'POST',
		success : function(data){
        	autoCloseAlert(data.errorInfo,2000);
			window.location.href="../md/list";
		}
	});
}
//返回上一页
function cancelSaveShop(){
	window.location.href="../md/list";
}

// 编辑门店
function editMd(id){
	window.location.href = "../md/editJump/"+id;
}