$(function(){
	// 加载文章列表
	loadXsckList();
})


// 跳转分页
function toPage(page){
	$("#page").val(page);
	loadXsckList();		
}

// 加载文章列表
function loadXsckList(){
	// 收集参数
	var param = buildParam();
	
	var page = $("#page").val();
	if(isEmpty(page) || page == 0){
		page = 1;
	}
	
	// 查询列表
	$.ajax({
        url : '../xsck/load',
        type: 'POST',
        //data : 'page='+page+"&param="+param,
        success  : function(data) {
        	$("#dataList").html(data);
		}
    });
	
}

// 收集参数
function buildParam(){
	var param = {};
	var keyword = $("#keyword").val();
	if(!isEmpty(keyword)){
		param["title"] = encodeURI(encodeURI(keyword));
	}
	
	var categoryId = $("#categoryId").val();
	if(!isEmpty(categoryId) && categoryId != '-1'){
		param["categoryId"] = categoryId;
	}
	
	var tagId = $("#tagId").val();
	if(!isEmpty(tagId) && tagId != '-1'){
		param["tagId"] = tagId;
	}
	
	return JSON.stringify(param);
}

// 搜索
function search(){
	loadXsckList();
}

//显示详情
function showXsck(id){
	$.ajax({
        url : '../xsck/show/'+id,
        type : 'POST',
        success  : function(data) {
        	$('#showXsckContent').html(data);
        	$('#showXsckModal').modal('show');
        	$('#showXsckModal').addClass('animated');
        	$('#showXsckModal').addClass('bounceInLeft');
		}
    });
}


// 新增文章  跳转新页
function addXsck(){
	window.location.href = "../xsck/add";
}

// 删除文章
function delXsck(id){
	$.ajax({
        url : '../xsck/del/'+id,
        type : 'DELETE',
        success  : function(data) {
        	if(data.resultCode == 'DEAL_SUCCESS'){
        		autoCloseAlert(data.errorInfo,3000);
        	}else{
        		autoCloseAlert(data.errorInfo,3000);
        	}
        	loadXsckList();
		}
    });
	
}


// 编辑文章
function editXsck(id){
	window.location.href = "../xsck/editJump/"+id;
}