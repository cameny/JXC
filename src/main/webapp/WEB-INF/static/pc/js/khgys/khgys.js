$(function(){
	// 加载商品列表
	loadKhgysList();
})

// 加载商品列表
function loadKhgysList(){	
	// 查询列表
	$.ajax({
        url : '../khgys/load',
        type: 'POST',
        success  : function(data) {
        	$("#dataList").html(data);
		}
    });
	
}

// 新增商品  跳转新页
function addKhgys(){
	$.ajax({
        url : '../khgys/insert/',
        type : 'POST',
        success  : function(data) {
        	$('#addKhgysContent').html(data);
        	$('#addKhgysModal').modal('show');
        	$('#addKhgysModal').addClass('animated');
        	$('#addKhgysModal').addClass('bounceInLeft');
		}
    });
	//window.location.href = "../sp/insert";
}

// 删除商品
function delKhgys(id){
	$.ajax({
        url : '../khgys/del/'+id,
        type : 'DELETE',
        success  : function(data) {
        	if(data.resultCode == 'DEAL_SUCCESS'){
        		autoCloseAlert(data.errorInfo,1000);
        	}else{
        		autoCloseAlert(data.errorInfo,1000);
        	}
        	loadKhgysList();
		}
    });	
}
//保存商品
function saveKhgys(){
	$.ajax({
		url : '../khgys/add',
		data : $("#addForm").serialize(),
		type : 'POST',
		success : function(data){
        	autoCloseAlert(data.errorInfo,2000);
        	$('#addKhgysModal').modal('hide');
        	loadKhgysList();
			//window.location.href="../sp/list";
		}
	});
}
//返回上一页
function cancelSaveShop(){
	window.location.href="../khgys/list";
}


//显示详情
function showKhgys(id){
	$.ajax({
        url : '../khgys/show/'+id,
        type : 'POST',
        success  : function(data) {
        	$('#showKhgysContent').html(data);
        	$('#showKhgysModal').modal('show');
        	$('#showKhgysModal').addClass('animated');
        	$('#showKhgysModal').addClass('bounceInLeft');
		}
    });
}


// 编辑商品
function editKhgys(id){
	$.ajax({
        url : '../khgys/editJump/'+id,
        type : 'POST',
        success  : function(data) {
        	$('#editKhgysContent').html(data);
        	$('#editKhgysModal').modal('show');
        	$('#editKhgysModal').addClass('animated');
        	$('#editKhgysModal').addClass('bounceInLeft');
		}
    });
	//window.location.href = "../sp/editJump/"+id;
}

//编辑商品保存
function saveEditKhgys(){
	$.ajax({
		url : '../khgys/edit',
		data : $("#editForm").serialize(),
		type : 'POST',
		success : function(data){
        	autoCloseAlert(data.errorInfo,1000);
        	$('#editKhgysModal').modal('hide');
        	loadKhgysList();
			//window.location.href="../sp/list";
		}
	});
}