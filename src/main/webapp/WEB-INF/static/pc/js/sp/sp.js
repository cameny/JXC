$(function(){
	// 加载商品列表
	loadSpList();
})

// 加载商品列表
function loadSpList(){	
	// 查询列表
	$.ajax({
        url : '../sp/load',
        type: 'POST',
        success  : function(data) {
        	$("#dataList").html(data);
		}
    });
	
}

// 新增商品  跳转新页
function addSp(){
	$.ajax({
        url : '../sp/insert/',
        type : 'POST',
        success  : function(data) {
        	$('#addSpContent').html(data);
        	$('#addSpModal').modal('show');
        	$('#addSpModal').addClass('animated');
        	$('#addSpModal').addClass('bounceInLeft');
		}
    });
	//window.location.href = "../sp/insert";
}

// 删除商品
function delSp(id){
	$.ajax({
        url : '../sp/del/'+id,
        type : 'DELETE',
        success  : function(data) {
        	if(data.resultCode == 'DEAL_SUCCESS'){
        		autoCloseAlert(data.errorInfo,1000);
        	}else{
        		autoCloseAlert(data.errorInfo,1000);
        	}
        	loadSpList();
		}
    });	
}
//保存商品
function saveSp(){
	$.ajax({
		url : '../sp/add',
		data : $("#addForm").serialize(),
		type : 'POST',
		success : function(data){
        	autoCloseAlert(data.errorInfo,1000);
        	$('#addSpModal').modal('hide');
        	loadSpList();
			//window.location.href="../sp/list";
		}
	});
}
//返回上一页
function cancelSaveShop(){
	window.location.href="../sp/list";
}


//显示详情
function showSp(id){
	$.ajax({
        url : '../sp/show/'+id,
        type : 'POST',
        success  : function(data) {
        	$('#showSpContent').html(data);
        	$('#showSpModal').modal('show');
        	$('#showSpModal').addClass('animated');
        	$('#showSpModal').addClass('bounceInLeft');
		}
    });
}


// 编辑商品
function editSp(id){
	$.ajax({
        url : '../sp/editJump/'+id,
        type : 'POST',
        success  : function(data) {
        	$('#editSpContent').html(data);
        	$('#editSpModal').modal('show');
        	$('#editSpModal').addClass('animated');
        	$('#editSpModal').addClass('bounceInLeft');
		}
    });
	//window.location.href = "../sp/editJump/"+id;
}

//编辑商品保存
function saveEditSp(){
	$.ajax({
		url : '../sp/edit',
		data : $("#editForm").serialize(),
		type : 'POST',
		success : function(data){
        	autoCloseAlert(data.errorInfo,1000);
        	$('#editSpModal').modal('hide');
        	loadSpList();
			//window.location.href="../sp/list";
		}
	});
}