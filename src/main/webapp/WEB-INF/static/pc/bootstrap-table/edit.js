$(function () {
	var i = 1;

    $('#zb').bootstrapTable({
        idField: 'id',
      	pagination: true,
      	search: false,
      	striped: true,
      	uniqueId: "id", 
      	editable: true,
        columns: [{
        	fild:  'id',
        	checkbox: true,
        	edit:false,
        	align:'center'
        },{
          	field: 'id',
            title: '编号',
            edit:false,
            align:'center'
        },{
        	field: 'spid',
        	title: '商品id',
        	align: 'center'
        },{
            field: 'spmc',
            title: '商品名称',
            align:'center'
        }, {
            field: 'txm',
            title: '条形码',
            align:'center'
        }, {
            field: 'gg',
            title: '规格',
            align:'center'
        }, {
            field: 'dw',
            title: '单位',
            align:'center'
        }, {
            field: 'sl',
            title: '数量',
            align:'center'
        }, {
            field: 'dj',
            title: '单价',
            align:'center'
        }, {
            field: 'xj',
            title: '小计',
            align:'center'
        }, {
            field: 'bz',
            title: '备注',
            align:'center'
        }],
        data :[{
        	id:  1 ,
        	spid: '1',
        	spmc: '123213',
        	txm : '',
        	gg  : '',
        	dw  : '',
        	sl  : '',
        	dj  : '',
        	xj  : '',
        	bz  : ''
        }],
      	onPostBody: function () {
      		 $('#zb').editableTableWidget({editor: $('<textarea>')});
        }
    });
		    
   
		    
		    
    $("#btn_add").click(function(){
    	$('#zb').bootstrapTable('insertRow',{
    		index: i+1,
    		row:{
    			id: i+1,
    			spid:i+1,
    			spmc:'',
    			txm:'',
    			gg:'',
    			dw:'',
    			sl:'',
    			dj:'',
    			xj:'',
    			bz:''
    		}
    	});
    	i++;
    });
    
    $("#btn_del").click(function(){
    	var ids = $.map($('#zb').bootstrapTable('getSelections'),function(row){
    		return row.id;
    	});
    	$('#zb').bootstrapTable('remove',{field: 'id', values: ids
    	});
    });
		    
});

function  getData(){
	alert(JSON.stringify($('#zb').bootstrapTable('getData')));
}