$(function () {
	 //1.初始化Table
   // var oTable = new TableInit();
   // oTable.Init();
});
    
    
    var TableInit = function () {
        var oTableInit = new Object();
        //初始化Table
        oTableInit.Init = function () {
        	 $('#zb').bootstrapTable({
        	        idField: 'id',
        	      	pagination: true,
        	      	search: false,
        	      	striped: true,
        	      	uniqueId: "id", 
        	      	queryParams:oTableInit.queryParams,
        	      	editable: true,
        	        columns: [{
        	        	fild:  'id',
        	        	checkbox: true,
        	        	edit:false,
        	        	align:'center'
        	        },{
        	          	field: 'id',
        	            title: '编号',
        	            edit:false,
        	            align:'center'
        	        },{
        	            field: 'spmc',
        	            title: '商品名称',
        	            align:'center'
        	        }, {
        	            field: 'txm',
        	            title: '条形码',
        	            align:'center'
        	        }, {
        	            field: 'gg',
        	            title: '规格',
        	            align:'center'
        	        }, {
        	            field: 'dw',
        	            title: '单位',
        	            align:'center'
        	        }, {
        	            field: 'sl',
        	            title: '数量',
        	            align:'center'
        	        }, {
        	            field: 'dj',
        	            title: '单价',
        	            align:'center'
        	        }, {
        	            field: 'xj',
        	            title: '小计',
        	            align:'center'
        	        }, {
        	            field: 'bz',
        	            title: '备注',
        	            align:'center'
        	        }],
        	        data :[{
        	        	id:  1 ,
        	        	spmc: '',
        	        	txm : '',
        	        	gg  : '',
        	        	dw  : '',
        	        	sl  : '',
        	        	dj  : '',
        	        	xj  : '',
        	        	bz  : ''
        	        }],
        	      	onPostBody: function () {
        	          	$('#zb').editableTableWidget({editor: $('<textarea>')});
        	        }
        	    });
        	 
        	 
        	 
        	//得到查询的参数
             oTableInit.queryParams = function (params) {
                   var temp = {   //这里的键的名字和控制器的变量名必须一直，这边改动，控制器也需要改成一样的
                       limit: params.limit,   //页面大小
                       offset: params.offset,  //页码
                       sdate: $("#stratTime").val(),
                       edate: $("#endTime").val(),
                       sellerid: $("#sellerid").val(),
                       orderid: $("#orderid").val(),
                       CardNumber: $("#CardNumber").val(),
                       maxrows: params.limit,
                       pageindex:params.pageNumber,
                       portid: $("#portid").val(),
                       CardNumber: $("#CardNumber").val(),
                       tradetype:$('input:radio[name="tradetype"]:checked').val(),
                       success:$('input:radio[name="success"]:checked').val(),
                   };
                   return temp;
               };
               return oTableInit;
        };
    }	
	
    
	var i = 1;
    $("#btn_add").click(function(){
    	$('#zb').bootstrapTable('insertRow',{
    		index: i+1,
    		row:{
    			id: i+1,
    			spmc:'',
    			txm:'',
    			gg:'',
    			dw:'',
    			sl:'',
    			dj:'',
    			xj:'',
    			bz:''
    		}
    	});
    	i++;
    });
    
    $("#btn_del").click(function(){
    	var ids = $.map($('#zb').bootstrapTable('getSelections'),function(row){
    		return row.id;
    	});
    	$('#zb').bootstrapTable('remove',{field: 'id', values: ids
    	});
    });

function  getData(){
	alert(JSON.stringify($('#zb').bootstrapTable('getData')));
}

